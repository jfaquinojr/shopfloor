﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ShopFloor.Entities;
using ShopFloor.Managers;

namespace ShopFloor.Web.Controllers
{
    [Authorize]
    public class UserClassesController : Controller
    {
        private UserClassManager db = new UserClassManager();

        // GET: UserClasses
        public ActionResult Index()
        {
            return View(db.GetAll());
        }

        // GET: UserClasses/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserClass userClass = db.GetByID(id.Value);
            if (userClass == null)
            {
                return HttpNotFound();
            }
            return View(userClass);
        }

        // GET: UserClasses/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UserClasses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,UClassCode,UClassDesc")] UserClass userClass)
        {
            if (ModelState.IsValid)
            {
                db.Create(userClass);
                return RedirectToAction("Index");
            }

            return View(userClass);
        }

        // GET: UserClasses/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserClass userClass = db.GetByID(id.Value);
            if (userClass == null)
            {
                return HttpNotFound();
            }
            return View(userClass);
        }

        // POST: UserClasses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,UClassCode,UClassDesc")] UserClass userClass)
        {
            if (ModelState.IsValid)
            {
                db.Save(userClass);
                return RedirectToAction("Index");
            }
            return View(userClass);
        }

        // GET: UserClasses/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserClass userClass = db.GetByID(id.Value);
            if (userClass == null)
            {
                return HttpNotFound();
            }
            return View(userClass);
        }

        // POST: UserClasses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            db.Remove(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
