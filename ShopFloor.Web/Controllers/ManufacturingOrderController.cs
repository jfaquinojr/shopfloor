﻿using ShopFloor.Entities;
using ShopFloor.Managers;
using ShopFloor.SAP.Managers;
using ShopFloor.Web.Models;
using ShopFloor.Web.Models.DataTables;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using ShopFloor.Business;

namespace ShopFloor.Web.Controllers
{
	[Authorize]
	public class ManufacturingOrderController : BaseController
	{
		MOHeaderManager db = new MOHeaderManager();

		#region Action Result

		public ActionResult Index()
		{
			return View();
		}

		public ActionResult Create()
		{
			ViewBag.MOTypes = this.GetMOTypes();

			var mo = new EditMOHeaderModel();
			mo.MONumber = db.GetMaxIntValue(m => m.MONumber) + 1;

			return View(mo);
		}


		[HttpPost]
		public ActionResult Create(EditMOHeaderModel model)
		{
			if (ModelState.IsValid)
			{
				if (!db.IsDuplicate(m => m.MONumber == model.MONumber && m.MOTypeID == model.MOTypeID))
				{
					model.CreatedBy = db.GetUserIDByUsername(this.User.Identity.Name);
					model.CreatedOn = DateTime.Now;
					int newid = db.Create(model);

					AddMessageSuccess("Successfully created MOHeader!");
					return RedirectToAction("Edit", new { id = newid });
				}
				ModelState.AddModelError("", "Duplicate Manufcaturing Order (same Number and Type) found.");
			}
			ViewBag.MOTypes = this.GetMOTypes();
			return View(model);
		}

		public ActionResult Edit(int? id)
		{
			if (!id.HasValue)
				return RedirectToAction("Index");

			ViewBag.MOTypes = this.GetMOTypes();
			var model = db.Map(db.GetByID(id.Value), new EditMOHeaderModel());
			model.FinishedGoods = db.GetFinishedGoods(id.Value);
			model.CurrentDateTime = TempData["CurrentDateTime"] as DateTime? ?? DateTime.Now;

			if(model.DownTimeRouteID > 0)
			{
				var route = new RoutesManager().GetByID(model.DownTimeRouteID.Value);
				if(route!= null)
				{
					model.RouteDisplayName = $"{route.RouteCode} - {route.RouteDesc}";
				}
			}

			return View(model);
		}

		[HttpPost]
		public ActionResult Edit(EditMOHeaderModel model)
		{
			if (ModelState.IsValid)
			{
				var entity = db.GetByID(model.ID);
				entity.MONumber = model.MONumber;
				entity.MOTypeID = model.MOTypeID;
				entity.GangRunTag = model.GangRunTag;
				entity.ModifiedBy = db.GetUserIDByUsername(User.Identity.Name);
				entity.ModifiedOn = DateTime.Now;

				db.Save(entity);
				
				AddMessageSuccess("MO successfully saved.");

				return RedirectToAction("Edit", model.ID);
			}

			ViewBag.MOTypes = this.GetMOTypes();
			model.FinishedGoods = db.GetFinishedGoods(model.ID);
			model.CurrentDateTime = model.CurrentDateTime;
			return View(model);
		}

		public ActionResult MOClosing(int? id)
		{
			if (!id.HasValue)
				return RedirectToAction("Index");

			ViewBag.MOTypes = this.GetMOTypes();
			var model = db.Map(db.GetByID(id.Value), new EditMOHeaderModel());
			model.FinishedGoods = db.GetFinishedGoods(id.Value);
			model.CurrentDateTime = TempData["CurrentDateTime"] as DateTime? ?? DateTime.Now;
			model.IsMOClosing = true;
			return View("Edit", model);
		}

		public ActionResult CreateFinishedGood(EditMODetailFggModel model)
		{
			if (ModelState.IsValid)
			{
				// get MO header
				var moHeader = new MOHeaderManager().GetByID(model.MOHeaderID);


				// Create FGG
				int createdBy = db.GetUserIDByUsername(User.Identity.Name);
				model.CreatedBy = createdBy;
				model.CreatedOn = DateTime.Now;
				var item = new SAP.Managers.SAPItemManager().GetByItemCode(model.ItemCode);
				if (item != null)
				{
					model.ItemName = item.ItemName;
				}


				// save to database
				var dbFGG = new MODetailFGGManager();
				model.ID = dbFGG.Create(model);


				// add resources for this FGG
				var dbRSC = new MODetailRSCManager();
				if(moHeader.GangRunTag.GetValueOrDefault(false))
				{
					dbRSC.CreateFromFGGGangRun(model);
				}
				else
				{
					dbRSC.CreateFromFGG(model);
				}


				// add raw materials for this FGG
				var dbITM = new MODetailITMManager();
				dbITM.CreateFromItemCode(model.ItemCode, model.ID, createdBy);


				return MOContent(model.MOHeaderID);
			}

			return null;
		}

		public ActionResult MOContent(int id)
		{
			var mo = db.GetByID(id);
			var model = new EditMOHeaderModel();
			model = db.Map(mo, model);
			model.FinishedGoods = db.GetFinishedGoods(id);

			return PartialView("_MOContent", model);
		}

		public ActionResult DeleteFGG(int id)
		{
			//delete child records - schedule
			var dbSched = new MOResourceScheduleManager();
			dbSched.DeleteSchedByFGGID(id);

			//delete child records - RSC
			var dbFGG = new MODetailFGGManager();
			dbFGG.DeleteRSCByFGGID(id);

			//delete child records - ITM
			dbFGG.DeleteITMByFGGID(id);

			// remember MOHeaderID first
			int moHeaderID = dbFGG.GetByID(id).MOHeaderID;

			//delete FGG record
			dbFGG.Flush(); //<-- calls SaveChanges to delete child records
			dbFGG.Remove(id);

			return MOContent(moHeaderID);
		}

		[HttpPost]
		public ActionResult UpdateEstTime(MODetailRsc model)
		{
			var dbRSC = new MODetailRSCManager();
			var rsc = dbRSC.GetByID(model.ID);
			rsc.ModifiedBy = dbRSC.GetUserIDByUsername(User.Identity.Name);
			rsc.ModifiedOn = DateTime.Now;
			rsc.EstTime = model.EstTime;
			rsc.SetupTime = model.SetupTime;
			rsc.StockTime = model.StockTime;
			dbRSC.Save(rsc);

			dbRSC.UpdateMOSchedule(rsc.MODetailFgg.MOHeaderID);

			return MOContent(rsc.MODetailFgg.MOHeaderID);
		}

		public ActionResult MOByResource()
		{
			var result = db.GetMOByResource();
			var dbResources = new ResourceManager();
			ViewBag.Resources = this.CreateSelectList(dbResources.GetAll(), m => m.ID, m => string.Format("{0} | {1}", m.ResourceCode, m.ResourceDesc));
			return View();
		}

		public ActionResult DowntimeList()
		{
			return View();
		}

		public ActionResult DownTimeCreate()
		{
			var mo = new CreateDownTime();
			mo.MONumber = db.GetMaxIntValue(m => m.MONumber) + 1;
			mo.CurrentDateTime = DateTime.Now;
			mo.MOTypeID = new ShopFloorEntityManager<MOType>(m=>m.ID).QueryAll().FirstOrDefault(m=>m.MOTypeCode == ShopFloor.Constants.CONST_MOTYPES_FILLER)?.ID ?? 0;
			var dbRoutes = new RoutesManager();
			ViewBag.Routes = CreateSelectList(dbRoutes.GetDownTimeRoutes(), m => m.ID, m => $"{m.RouteCode}|{m.RouteDesc}");
			return View(mo);
		}

		[HttpPost]
		public ActionResult DownTimeCreate(CreateDownTime downTime)
		{
			if (ModelState.IsValid)
			{
				int createdBy = db.GetUserIDByUsername(User.Identity.Name);
				var dbFGG = new MODetailFGGManager();
				var dbRSC = new MODetailRSCManager();

				// create an MO
				var mo = new MOHeader();
				mo.CreatedBy = createdBy;
				mo.CreatedOn = DateTime.Now;
				mo.MOTypeID = GetFillerMOTypeID();
				mo.MONumber = downTime.MONumber;
				mo.DownTimeRouteID = downTime.RouteID;
				db.Create(mo);

				// get all item routes
				var dbItemRoutes = new ItemRouteManager();
				var itemRoutes = dbItemRoutes.GetAllByRouteID(downTime.RouteID);
				foreach(var itemRoute in itemRoutes)
				{

					// create new fgg
					var fgg = new MODetailFgg();
					fgg.ItemCode = itemRoute.ItemCode;
					fgg.ItemName = itemRoute.ItemName;
					fgg.MOHeaderID = mo.ID;
					fgg.CreatedBy = createdBy;
					fgg.CreatedOn = DateTime.Now;
					fgg.ID = dbFGG.Create(fgg);

					// add resources for this FGG
					dbRSC.CreateFromFGG(fgg);
				}

				return RedirectToAction("DownTimeEdit", new { id = mo.ID });
			}
			return View(downTime);
		}

		public ActionResult DownTimeEdit(int id)
		{
			return RedirectToAction("Edit", new { id = id });
		}



		#endregion


		#region Json Result

		public JsonResult GetNoOfClosedRSC(int id)
		{
			return Json(1, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public JsonResult CloseMO(int id)
		{
			db.CloseMO(id);
			return Json(true, JsonRequestBehavior.AllowGet);
		}

		public JsonResult Delete(int id)
		{
			var mo = db.GetByID(id);
			db.ArchiveMO(mo);

			db.Remove(id);

			return Json(true, JsonRequestBehavior.AllowGet);
		}


		public JsonResult ValidateFGG(string itemcode, int id)
		{
			//var routeDb = new RoutesManager();
			//routeDb.IsDuplicate(m=>m.ItemCode)

			var dbFGG = new MODetailFGGManager();
			if (dbFGG.HasDuplicateItemCode(itemcode, id))
			{
				return Json("There is already an existing item code for this MO.", JsonRequestBehavior.AllowGet);
			}

			var dbSAP = new SAPItemManager();
			if (!dbSAP.IsDuplicate(m => m.ItemCode == itemcode))
			{
				return Json("This Item Code is invalid or does not exist.", JsonRequestBehavior.AllowGet);
			}

			var dbBOM = new BOMManager();
			if (!dbBOM.IsEffective(itemcode))
			{
				return Json("The BOM Effectivity date has expired.", JsonRequestBehavior.AllowGet);
			}

			return Json(true, JsonRequestBehavior.AllowGet);
		}

		public JsonResult UpdateFGG(MODetailFgg entity)
		{

			// pull values for docEntry and lineNum
			int? docEntry = null;
			int? lineNum = null;
			var dbSOMgr = new SAPOrderManager();
			SAP.Entities.Order order = null;
			if (entity.SONo > 0)
				order = dbSOMgr.GetByDocNum(entity.SONo ?? 0);
			if (order != null && order.OrderEx != null)
			{
				docEntry = order.DocEntry;
				lineNum = order.OrderEx.LineNum;
			}



			var dbRSC = new MODetailFGGManager();
			var e = dbRSC.GetByID(entity.ID);
			e.SAPRDR1DocEntry = docEntry;
			e.SAPRDR1LineNum = lineNum;
			e.OutsTag = entity.OutsTag ?? e.OutsTag;
			e.SONo = entity.SONo;
			e.DeliveryDate = entity.DeliveryDate ?? e.DeliveryDate;
			e.ModifiedBy = db.GetUserIDByUsername(User.Identity.Name);
			e.ModifiedOn = DateTime.Now;
			dbRSC.Save(e);

			return Json(true, JsonRequestBehavior.AllowGet);
		}

		public JsonResult UpdateITM(MODetailItm entity)
		{
			var dbITM = new MODetailITMManager();
			var e = dbITM.GetByID(entity.ID);
			e.ModifiedBy = dbITM.GetUserIDByUsername(User.Identity.Name);
			e.ModifiedOn = DateTime.Now;
			e.MOQty = entity.MOQty;

			e.RMItemCode = entity.RMItemCode;
			e.RMItemName = entity.RMItemName;
			//e.IssuedQty = entity.IssuedQty; changed to readonly
			e.IssuedQty2 = entity.IssuedQty2;
			e.UOMFactor = entity.UOMFactor;
			e.RMFactor = entity.RMFactor;
			e.Allowance = entity.Allowance;
			

			dbITM.Save(e);
			return Json(e, JsonRequestBehavior.AllowGet);
		}

		public JsonResult RecomputeMOQty(int itmId)
		{
			var dbITM = new MODetailITMManager();
			var e = dbITM.GetByID(itmId);

			//re-compute MOQty
			var fgg = new MODetailFGGManager().GetByID(e.MODetailFGGID);
			fgg.MOHeader = new MOHeaderManager().GetByID(fgg.MOHeaderID);
			var calc = CalculatorFactory.Create(e);
			e.MOQty = calc.ComputeMOQty(e.IssuedQty2.Value, e.UOMFactor.Value, e.Allowance.Value, e.RMFactor.Value);

			dbITM.Save(e);

			return Json(true, JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetFGG(int id)
		{
			var fgg = db.GetFinishedGood(id);
			var result = new
			{
				ID = fgg.ID,
				BatchPrcnt = fgg.BatchPrcnt,
				BatchQty = fgg.BatchQty,
				DeliveryDate = fgg.DeliveryDate,
				MOHeaderID = fgg.MOHeaderID,
				OutsTag = fgg.OutsTag,
				SONo = fgg.SONo,
				ItemCode = fgg.ItemCode
			};
			return Json(fgg, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Inserts MODetailRSC into MOResourceSchedule
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public JsonResult ProcessSchedule(int id)
		{
			var mo = db.GetByID(id);
			mo.IsScheduled = true;
			mo.ModifiedBy = db.GetUserIDByUsername(User.Identity.Name);
			mo.ModifiedOn = DateTime.Now;
			db.Save(mo);
			db.ProcessSchedule(id, User.Identity.Name);

			return Json(true, JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetItemRouteByID(int itemRouteId)
		{
			var dbIR = new ItemRouteManager();
			return Json(dbIR.GetByID(itemRouteId), JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public JsonResult SaveTransmittal(int fggID, int transmQty, int transmID)
		{

			// save or create transmittal. if transmID is 0, create else update.
			var dbTrans = new TransmittalManager();
			Transmittal entity = null;
			if(transmID > 0)
			{
				entity = dbTrans.GetByID(transmID);
			}
			else
			{
				entity = new Transmittal();
			}


			entity.MODetailFGGID = fggID;
			entity.TrnsmQty = transmQty;


			if(transmID > 0)
			{
				dbTrans.Save(entity);
			}
			else
			{
				dbTrans.Create(entity);
			}
		   

			return Json(entity, JsonRequestBehavior.AllowGet);
		}


		public JsonResult GetNextSeriesValue(int moTypeId)
		{
			var currentMaxValue = db.GetMaxIntValue(m => m.MONumber, m => m.MOTypeID == moTypeId);

			var dbMOTypes = new ShopFloorEntityManager<MOType>(m => m.ID);
			var motype = dbMOTypes.GetByID(moTypeId);
			//var moSeriesEnd = motype.MOSeriesEnd > motype.MOSeriesStart ? motype.MOSeriesEnd : int.MaxValue;
			var moSeriesEnd = int.MaxValue; // always ignore MOSeriesEnd. But just leave logic here. Just in case.

			//if still within range then increment it
			if (currentMaxValue >= motype.MOSeriesStart && currentMaxValue < moSeriesEnd) 
			{
				currentMaxValue += 1;

				var maxHSMONumber = GetMaxHistoryMONumber(motype.MOTypeCode);
				if(maxHSMONumber >= currentMaxValue)
				{
					currentMaxValue = maxHSMONumber + 1;
				}
			}
			else
			{
				// otherwise, reset
				currentMaxValue = motype.MOSeriesStart;
			}

			return Json(currentMaxValue, JsonRequestBehavior.AllowGet);
		}

		public ActionResult CheckIfMONumberExistsInHistory(EditMOHeaderModel model)
		{
			
			var dbHSMO = new ShopFloorEntityManager<HSMOHeader>(m => m.ID);
			bool exists = false;
			if(model.MOTypeID > 0)
			{
				var dbMOTypeMgr = new ShopFloorEntityManager<MOType>(m => m.ID);
				var moType = dbMOTypeMgr.GetByID(model.MOTypeID);
				exists = dbHSMO.QueryAll().Any(m => m.MONumber == model.MONumber && m.MOType == moType.MOTypeCode);
			}
			else
			{
				exists = dbHSMO.QueryAll().Any(m => m.MONumber == model.MONumber);
			}
			if(exists)
			{
				return Json("MONumber already exists in HSMOHistory.", JsonRequestBehavior.AllowGet);
			}
			return Json(true, JsonRequestBehavior.AllowGet);
		}

		public ActionResult GetSAPSO(int soNumber)
		{
			var dbSAPSO = new SAPOrderManager();
			var entity = dbSAPSO.QueryAll().Where(m => m.DocNum == soNumber).FirstOrDefault();
			return Json(entity, JsonRequestBehavior.AllowGet);
		}

		public ActionResult GetSubstituteRM(int moDetailITMID)
		{
			var itm = new MODetailITMManager().GetByID(moDetailITMID);

			var dbSubs = new ShopFloorEntityManager<BOMDetailSub>(m => m.ID);
			var list = dbSubs.QueryAll().Where(m => m.BOMDetailID == itm.BOMDetailID).ToList();

			return Json(list, JsonRequestBehavior.AllowGet);
		}

		public ActionResult GetDownTimeList(SearchParams args)
		{
			var list = db.QueryAllIncluding(m => m.MOType).Where(m => m.MOType.MOTypeCode == ShopFloor.Constants.CONST_MOTYPES_FILLER).ToList();
			var json = CreateDTReturnData(list, list.Count, list.Count, args.draw);
			return Content(json, "application/json");
		}

		#endregion


		#region Content Result


		public ContentResult GetMOByResource(SearchParams args)
		{
			var result = db.GetMOByResource();
			var data = base.CreateDTReturnData(result, result.Count, result.Count, args.draw);
			return Content(data, "application/json");
		}

		public ContentResult GetSAPSONumbers(SearchParams args, string itemCode)
		{
			int page, row;
			string table = args.Table;
			string filter = args.Filter ?? "";
			string sortcolumn = "";
			string direction = "asc";
			if (args.order?.Length > 0 && args.columns.Length >= args.order.Length)
			{
				sortcolumn = args.columns[args.order[0].column].data;
				direction = args.order[0].dir;
			}
			string searchvalue = "";
			if (args.search != null) searchvalue = args.search.value ?? "";

			var dbSO = new SAPOrderManager();
			dbSO.FilterWithItemCodeAsWell(itemCode);
			var result = dbSO.Search(searchvalue, args.length, args.start, out page, out row, sortcolumn, direction);

			var data = base.CreateDTReturnData(result, page, row, args.draw);
			return Content(data, "application/json");
		}

		#endregion


		#region Private Methods

		private int GetMaxHistoryMONumber(string moTypeCode = "")
		{
			var dbHSMO = new ShopFloorEntityManager<HSMOHeader>(m => m.ID);
			if(string.IsNullOrEmpty(moTypeCode))
			{
				return dbHSMO.GetMaxIntValue(m => m.MONumber);
			}
			return dbHSMO.GetMaxIntValue(m=>m.MONumber, m => m.MOType == moTypeCode);
		}

		private IEnumerable<SelectListItem> GetMOTypes()
		{
			var dbTypes = new ShopFloorEntityManager<MOType>(m => m.ID);
			return this.CreateSelectList(dbTypes.QueryAll().Where(m => m.IsDeleted != true).ToList(), m => m.ID, m => $"{m.MOTypeCode} | {m.MOTypeName}");
		}

		private int GetFillerMOTypeID()
		{
			var dbMOType = new ShopFloorEntityManager<MOType>(m => m.ID);
			var moType = dbMOType.QueryAll().FirstOrDefault(m => m.MOTypeCode == ShopFloor.Constants.CONST_MOTYPES_FILLER);
			return moType.ID;
		}

		#endregion

	}
}
