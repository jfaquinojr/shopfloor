﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ShopFloor.Entities;
using ShopFloor.Managers;
using ShopFloor.Web.Models;


namespace ShopFloor.Web.Controllers
{
	[Authorize]
    public class ResourceCategoriesController : BaseController
    {
		private ShopFloorEntityManager<ResourceCategory> db = new ShopFloorEntityManager<ResourceCategory>(m => m.ID);

        // GET: ResourceCategories
        public ActionResult Index()
        {
            return View();
        }

        // GET: ResourceCategories/Create
        public ActionResult Create()
        {
            return View(new ResourceCategory { Active = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ResourceCategory model)
        {
            if (ModelState.IsValid)
            {
                if (!db.IsDuplicate(m => m.Code == model.Code))
                {
                    model.CreatedBy = db.GetUserIDByUsername(User.Identity.Name);
                    model.CreatedOn = DateTime.Now;
                    db.Create(model);
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("Code", "Another Category with the same Code already exists.");
                }

            }

            return View(model);
        }

        // GET: ResourceCategories/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ResourceCategory resourceCategory = db.GetByID(id.Value);
            if (resourceCategory == null)
            {
                return HttpNotFound();
            }
            return View(resourceCategory);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ResourceCategory model)
        {
            if (ModelState.IsValid)
            {
                if (!db.IsDuplicate(m => m.Code == model.Code && m.ID != model.ID))
                {
                    model.ModifiedBy = db.GetUserIDByUsername(User.Identity.Name);
                    model.ModifiedOn = DateTime.Now;
                    db.Save(model);
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("Code", "Another Category with the same Code already exists.");
                }
            }
            return View(model);
        }

        // GET: ResourceCategories/Delete/5
        public JsonResult Delete(int id)
        {
            db.Remove(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
