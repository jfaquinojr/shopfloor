﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ShopFloor.Web.Models;
using ShopFloor.SAP.Managers;
using ShopFloor.Managers;
using ShopFloor.Web.Models.DataTables;
using System.Collections.Specialized;
using System.Collections;
using Newtonsoft.Json;

namespace ShopFloor.Web.Controllers
{
    [Authorize]
    public class SearchController : BaseController
    {

        public ContentResult Search(SearchParams args)
        {
            //HttpUtility.ParseQueryString(new Uri(fullUrl).Query)
            int page, row;
            string table = args.Table;
            string filter = args.Filter ?? "";
            string sortcolumn = "";
            string direction = "asc";
            if (args.order?.Length > 0 && args.columns.Length >= args.order.Length)
            {
                sortcolumn = args.columns[args.order[0].column].data;
                direction = args.order[0].dir;
            }
            string searchvalue = "";
            if (args.search != null) searchvalue = args.search.value;

            
            IList result = ManagerFactory.Create(table, filter).Search(searchvalue ?? "", args.length, args.start, out page, out row, sortcolumn, direction); // search all

            var data = CreateDTReturnData(result, page, row, args.draw);

            return Content(data, "application/json");
        }

    }
}