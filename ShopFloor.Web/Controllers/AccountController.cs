﻿using ShopFloor.Managers;
using ShopFloor.Web.Models;
using System;
using System.Web.Mvc;
using System.Web.Security;

namespace ShopFloor.Web.Controllers
{
    public class AccountController : BaseController
    {
        UsersManager db = new UsersManager();

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel model, string ReturnUrl)
        {
            
            if (ModelState.IsValid)
            {
                if (db.ValidateLogin(model.Username, model.Password))
                {

                    if(db.IsVerified(model.Username))
                    {
                        FormsAuthentication.SetAuthCookie(model.Username, false);

                        if (db.IsChangePassword(model.Username))
                            return RedirectToAction("ChangePassword");

                        if (!string.IsNullOrEmpty(ReturnUrl))
                            return Redirect(ReturnUrl);

                        return this.RedirectToAction("Index", "Default");
                    }

                    ModelState.AddModelError("", "Account is not yet verified.");
                    return View(model);
                }
                ModelState.AddModelError("", "username or password is incorrect.");
            }
            return View(model);
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(AccountRegisterModel model)
        {
            if (ModelState.IsValid)
            {
                // check if passwords match
                if (model.Password != model.ConfirmPassword)
                {
                    ModelState.AddModelError("", "Passwords do not match.");
                    return View(model);
                }

                // check if username already exists
                if (db.HasDuplicate(model.Username))
                {
                    ModelState.AddModelError("", "Username already exists.");
                    return View(model);
                }

                // create new user
                db.RegisterNewUser(model.Username, model.Password, model.UserID, model.EmployeeID);

                return View("RegistrationComplete", model);
            }

            return View(model);
        }



        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(EditChangePassword model)
        {
            if (ModelState.IsValid)
            {
                if (model.ConfirmPassword == model.NewPassword)
                {
                    db.ChangePassword(User.Identity.Name, model.ConfirmPassword);
                    AddMessageSuccess("Password successfully changed.");
                    return RedirectToAction("Index", "Default");
                }
                else
                {
                    ModelState.AddModelError("", "Passwords do not match.");
                }

            }
            return View(model);
        }


        [HttpPost]
        public JsonResult KeepSessionAlive(string whatever)
        {
            Session.Timeout = 60000;
            Session["KEEPSESSIONALIVE"] = DateTime.Now.ToFileTime();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

    }
}