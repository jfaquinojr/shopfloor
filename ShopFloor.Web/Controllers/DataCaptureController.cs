﻿using ShopFloor.Entities;
using ShopFloor.Managers;
using ShopFloor.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace ShopFloor.Web.Controllers
{
	[Authorize]
	public class DataCaptureController : BaseController
	{
		private DataCaptureManager db = new DataCaptureManager();

		public ActionResult Index(int? id) //id == MOHeaderID
		{
			if (id == null)
			{
				return RedirectToAction("Index", "ManufacturingOrder");
			}

			PopulateDropDowns(id.Value);

			// get list of data capture
			var model = db.Map(new MOHeaderManager().GetByID(id.Value), new EditMOHeaderModel());


			return View(model);
		}

		public ActionResult Edit(int id)
		{
			// get list of data capture
			var model = db.Map(db.GetByID(id), new EditShopFloorDataCaptureModel());

			PopulateDropDowns(id);
			return View(model);
		}

		public JsonResult GetFGGJSONList(int moHeaderId)
		{
			var fggs = new MODetailFGGManager().GetByMOHeaderID(moHeaderId);

			var list = fggs.Select(fgg => new ViewDataCaptureFGG
			{
				ItemCode = fgg.ItemCode,
				ItemDescription = fgg.ItemName,
				DeliveryDate = fgg.DeliveryDate,
				MODetailFGGID = fgg.ID,
				MOQuantity = (int)(fgg.BatchQty ?? 0),
				Process = false
			}).ToList();

			return Json(list, JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetDataCapture(int id)
		{
			var result = db.GetByID(id);

			var addlRM = db.GetAddlRawMaterials(id).FirstOrDefault() ?? new ShopFloorAddl();

            var rsc = db.GetMODetailRSC(result.MODetailRSCID);
			var schedStart = rsc?.FromDate ?? result.RscStartDate; // just in case resource was closed/archived
			var schedEnd = rsc?.ToDate ?? result.RscEndDate;       // get from dbo.ShopFloor.RscDATES values

			var model = new
			{
				ID = result.ID,
				Status = result.Status,
				StartDate = result.DisplayStartDate,
				EndDate = result.DisplayEndDate,
				ElapsedTime = result.DisplayElapsedTime,
				MissingScheduleDates = (result.RscStartDate == null || result.RscEndDate == null),
				ScheduleStartDate = schedStart.GetValueOrDefault(DateTime.MaxValue).ToString(ShopFloor.Constants.CONST_FORMAT_DATETIME),
				ScheduleEndDate = schedEnd.GetValueOrDefault(DateTime.MaxValue).ToString(ShopFloor.Constants.CONST_FORMAT_DATETIME),
				ScheduleElapsedTime = (schedEnd.GetValueOrDefault(DateTime.MaxValue) - schedStart.GetValueOrDefault(DateTime.MinValue)).ToString(@"hh\:mm\:ss"),
				Good = result.Good,
				NoOfOuts = result.NoOfOuts,
				SubResourceID = result.SubResourceID,
				RunRateReasonID = result.RunRateReasonID,
				ActivityType = new ActivityType
				{
					ID = result.ActivityTypeID.Value,
					ActivityCode = result.ActivityType.ActivityCode,
					ActivityName = result.ActivityType.ActivityName,
				},
				ShopFloorAddl = new ShopFloorAddl
				{
					ID = addlRM.ID,
					AdditionalReasonID = addlRM.AdditionalReasonID,
					AddlQty = addlRM.AddlQty,
					ShopFloorID = addlRM.ShopFloorID,
					RMItemCode = addlRM.RMItemCode,
				}

			};

			return Json(model, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public JsonResult GetResourcesForFGGs(int[] fggIDs)
		{
			if (fggIDs == null) return null;

			var resources = new MODetailRSCManager().GetAllByFGGIDs(fggIDs);
            var result = (from r in resources
                          where (r.ClosedTag ?? false) == false
                          select new
                          {
                              ResourceCode = r.ResourceCode,
                              ResourceDesc = r.ResourceDesc,
                              MOHeaderID = r.MODetailFgg.MOHeaderID,
                              Sequence = r.Sequence
                          })
						  .Distinct()
						  .OrderBy(m => m.Sequence);
			return Json(result, JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetMODetailRSC(string resourceCode, int fggId)
		{
			var dbRSC = new MODetailRSCManager();
			var ret = dbRSC.QueryAll()
				.FirstOrDefault(m => m.ResourceCode == resourceCode && m.MODetailFGGID == fggId);
			var json = new
			{
				ScheduleStartDate = ret.StartDate.ToString(ShopFloor.Constants.CONST_FORMAT_DATETIME),
				ScheduleEndDate = ret.EndDate.ToString(ShopFloor.Constants.CONST_FORMAT_DATETIME),
				ScheduleElapsedTime = (ret.EndDate - ret.StartDate).ToString(@"hh\:mm\:ss"),
				ID = ret.ID
			};
			return Json(json, JsonRequestBehavior.AllowGet);
		}

		public JsonResult IsOnStreamIsNull(string resourceCode, int fggID)
		{
			var dbRSC = new MODetailRSCManager();
			var hasNullOnStreamTag = dbRSC.QueryAll()
				.Any(m => 
					m.ResourceCode == resourceCode 
					&& m.MODetailFGGID == fggID 
					&& m.OnStreamTag == null);
			return Json(hasNullOnStreamTag, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public JsonResult StartResource(string resourceCode, int[] fggIDs, ActivityType activity)
		{
			var d = DateTime.Now;
			var userId = db.GetUserIDByUsername(User.Identity.Name);
			var dataRetVal = new List<ShopFloorDataCapture>();
			var dbFGG = new MODetailFGGManager();
			var dbRSC = new MODetailRSCManager();
			var dbQAP = new QAParamResourceManager();
			var resources = dbRSC.GetAllByFGGIDs(fggIDs);
			var qaPars = dbQAP.GetAllByResourceCode(resourceCode);
			var newShopFloorId = 0;
			foreach (var rsc in resources)
			{
				if(rsc.ResourceCode==resourceCode)
				{
					// create ShopFloor record
					var entity = new ShopFloorDataCapture();
					entity.BatchPrcnt = (int)(rsc.BatchPercent ?? 0);
					entity.MODetailRSCID = rsc.ID;
					entity.ActivityTypeID = activity.ID;

					entity.StartDate = d;
					entity.CreatedBy = userId;
					entity.CreatedOn = d;
					entity.UserID = userId;
					entity.RscStartDate = rsc.StartDate;
					entity.RscEndDate = rsc.EndDate;
                    entity.ResourceCode = rsc.ResourceCode;
                    entity.ResourceDesc = rsc.ResourceDesc;
                    entity.MOHeaderID = rsc.MODetailFgg.MOHeaderID;
                    entity.MONumber = rsc.MODetailFgg.MOHeader.MONumber;
                    entity.MODetailFGGID = rsc.MODetailFGGID;
                    entity.ItemCode = rsc.MODetailFgg.ItemCode;
                    entity.ItemName = rsc.MODetailFgg.ItemName;
					newShopFloorId = db.Create(entity);

					// save OnStream
					rsc.OnStreamTag = true;
					rsc.ModifiedOn = d;
					rsc.ModifiedBy = userId;
					dbRSC.Save(rsc);

					dataRetVal.Add(entity);

					// create ShopFloorQAPar records
					foreach(var qaPar in qaPars)
					{
						var sfQAPar = new ShopFloorQAPar();
						sfQAPar.CreatedBy = userId;
						sfQAPar.CreatedOn = d;
						sfQAPar.QAParamID = qaPar.QAParamID;
						sfQAPar.ShopFloorID = newShopFloorId;
						sfQAPar.QAParamActual = qaPar.QAParam.QAParaValueNum;
						db.CreateShopFloorQAParam(sfQAPar);
					}
				}
			}

			return Json(dataRetVal, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public JsonResult StopResource(int[] shopFloorIDs)
		{
			DateTime dateEnd = DateTime.Now;
			DateTime dateStart = dateEnd;
			var userId = db.GetUserIDByUsername(User.Identity.Name);

			var dbRSC = new MODetailRSCManager();
			var dbSched = new MOResourceScheduleManager();

			// get elapsed time.
			TimeSpan ts = new TimeSpan();

			foreach (var id in shopFloorIDs)
			{
				var sf = db.GetByID(id);
				sf.ModifiedBy = userId;
				sf.ModifiedOn = dateEnd;
				sf.EndDate = dateEnd;

				dateStart = sf.StartDate.Value;
				ts = (dateEnd - dateStart);
				sf.Duration = (decimal) ts.TotalHours;

				db.Save(sf);

                // set OnStreamTag to 0
                var rsc = db.GetMODetailRSC(sf.MODetailRSCID);
                if (rsc != null)
                {
                    rsc.OnStreamTag = false;
                    rsc.ModifiedBy = userId;
                    rsc.ModifiedOn = dateEnd;
                    dbRSC.Save(rsc);
                }

				dbSched.DeleteSchedByFGGID(sf.MODetailRSCID);
			}



			

			var jsonData = new
			{
				ElapsedTime = ts.ToString(@"hh\:mm\:ss"),
				EndDate = dateEnd,
				StartDate = dateStart
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		private void ArchiveRSC(MODetailRsc rsc)
		{
            var dbFGG = new MODetailFGGManager();
            var fgg = dbFGG.GetByIDIncluding(rsc.MODetailFGGID, m => m.MOHeader, m1 => m1.MOHeader.MOType);
            var mo = fgg.MOHeader;
			var hsRSC = new HSMODetailRsc();
			hsRSC.MOType = mo.MOType.MOTypeCode;
			hsRSC.MONumber = mo.MONumber;
			hsRSC.FGItemCode = fgg.ItemCode;
			hsRSC.ResourceCode = rsc.ResourceCode;
			hsRSC.SetupTime = rsc.SetupTime;
			hsRSC.StockTime = rsc.StockTime;
			hsRSC.EstTime = rsc.EstTime;
			hsRSC.FromDate = rsc.FromDate;
			hsRSC.ToDate = rsc.ToDate;
			hsRSC.OrigFromDate = rsc.OrigFromDate;
			hsRSC.OrigToDate = rsc.OrigToDate;
			hsRSC.OrigEstTime = rsc.OrigEstTime;
            hsRSC.RSCSeq = rsc.ID; // as per client request. store MODetailRSCID instead of rsc.Sequence; also see ManufacturingOrderController.Archive()
            hsRSC.ClosedTag = (short)(rsc.ClosedTag.GetValueOrDefault(false) ? 1 : 0);
			hsRSC.OnStreamTag = (short)(rsc.OnStreamTag.GetValueOrDefault(false) ? 1 : 0);
			hsRSC.MOStat = (short)(rsc.OnHold.GetValueOrDefault(false) ? 0 : 1);
			hsRSC.MOPriority = mo.MOPriority;
			hsRSC.PriorityTag = (short)(rsc.PriorityTag.GetValueOrDefault(false) ? 1 : 0);
			hsRSC.BatchPrcnt = (short)rsc.BatchPercent.GetValueOrDefault(0);
			//db.HSMODetailRscs.Add(hsRSC);
			var dbHSMO = new ShopFloorEntityManager<HSMODetailRsc>(m => m.ID);
			dbHSMO.Create(hsRSC);
		}

		[HttpPost]
		public JsonResult CloseResource(int[] shopFloorIDs)
		{
			{
				DateTime dateNow = DateTime.Now;
				var userId = db.GetUserIDByUsername(User.Identity.Name);

				var dbRsc = new MODetailRSCManager();

				foreach (var id in shopFloorIDs)
				{
					var sf = db.GetByID(id);
					sf.ModifiedBy = userId;
					sf.ModifiedOn = dateNow;

					db.Save(sf);

                    var rsc = db.GetMODetailRSC(sf.MODetailRSCID);
                    if (rsc != null)
                    {
                        rsc.ClosedTag = true;
                        rsc.ModifiedBy = userId;
                        rsc.ModifiedOn = dateNow;
                        dbRsc.Save(rsc);

                        ArchiveRSC(rsc);

                        dbRsc.Remove(rsc.ID);
                    }

					
				}

				return Json(true, JsonRequestBehavior.AllowGet);
			}
		}

		public ContentResult StopCapture(int id)
		{
			// save startdate
			var entity = db.GetByID(id);
			entity.EndDate = DateTime.Now;
			entity.ModifiedBy = db.GetUserIDByUsername(User.Identity.Name);
			entity.ModifiedOn = DateTime.Now;
			db.Save(entity);

			// save OnStream
			var rscMgr = new MODetailRSCManager();
			var rsc = rscMgr.GetByID(entity.MODetailRSCID.Value);
			rsc.OnStreamTag = false;
			rscMgr.Save(rsc);

			return Content("true");
		}

		public JsonResult Delete(int id)
		{
			return Json(true, JsonRequestBehavior.AllowGet);
		}

		#region JSON Dropdown List

		public JsonResult GetActivities()
		{
			var dbAct = new ActivityTypeManager();
			return Json(dbAct.GetAll(), JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetActiveActivities()
		{
			var dbAct = new ActivityTypeManager();
			return Json(dbAct.QueryAll().Where(m => m.Active == true), JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetRunRateReasons()
		{
			var dbMgr = new RunRateReasonManager();
			return Json(dbMgr.GetAll(), JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetAddlRM(int moHeaderID)
		{
			var dbITM = new MODetailITMManager();
			var query =
				dbITM.QueryAllIncluding(m => m.MODetailFgg, m => m.MODetailFgg.MOHeader, m => m.MODetailFgg.MOHeader.MOType)
					.Where(m => m.MODetailFgg.MOHeaderID == moHeaderID && m.MODetailFgg.MOHeader.MOType.MOTypeCode == ShopFloor.Constants.CONST_MOTYPES_MO)
					.Select(m => new {ItemCode = m.RMItemCode, ItemName = m.RMItemName});

			return Json(query.ToList(), JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetAddlReasons()
		{
			var dbMgr = new AdditionalReasonManager();
			return Json(dbMgr.GetAll(), JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetQACriteria()
		{
			var dbMgr = new QACriteriaManager();
			return Json(dbMgr.GetAll(), JsonRequestBehavior.AllowGet);
		}

		#endregion

		public JsonResult UpdateGeneralForm(int[] shopFloorIDs, ShopFloorDataCapture model)
		{
			int userid = db.GetUserIDByUsername(User.Identity.Name);
			var now = DateTime.Now;
			foreach(int id in shopFloorIDs)
			{
				var entity = db.GetByID(id);
				entity.ActivityTypeID = model.ActivityTypeID;
				entity.Good = model.Good;
				entity.RunRateReasonID = model.RunRateReasonID;
				entity.NoOfOuts = model.NoOfOuts;
				entity.ModifiedBy = userid;
				entity.ModifiedOn = now;
				db.Save(entity);
			}
			return Json(true, JsonRequestBehavior.AllowGet);
		}

		public JsonResult AddRejectEntry(int[] shopFloorIDs, int qaCriteriaId, decimal qty)
		{
			var model = new ShopFloorRej();
			model.CreatedBy = db.GetUserIDByUsername(User.Identity.Name);
			model.CreatedOn = DateTime.Now;
			model.QACriteriaID = qaCriteriaId;
			model.Reject = qty;
			db.CreateReject(model, shopFloorIDs);

			// return just one reject entry for display
			int shopFloorId = shopFloorIDs.First();
			ShopFloorRej reject = db.GetRejectByShopFloorID(shopFloorId, qaCriteriaId);

			return Json(reject, JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetRejectEntries(int id)
		{
			var result = db.GetRejects(id);
			return Json(result, JsonRequestBehavior.AllowGet);
		}


		[HttpPost]
		public JsonResult AddAddlRM(int[] shopFloorIDs, ShopFloorAddl model)
		{
			model.CreatedBy = db.GetUserIDByUsername(User.Identity.Name);
			model.CreatedOn = DateTime.Now;

			var dbAddlRM = new ShopFloorAddlManager();
			foreach (var shopFloorId in shopFloorIDs)
			{
				var newAddlRM = new ShopFloorAddl()
				{
					ShopFloorID = shopFloorId,
					AddlQty = model.AddlQty,
					RMItemCode = model.RMItemCode,
					AdditionalReasonID = model.AdditionalReasonID,
					CreatedBy = model.CreatedBy,
					CreatedOn = model.CreatedOn
				};
				dbAddlRM.Create(newAddlRM);
			}

			return Json(true, JsonRequestBehavior.AllowGet);
		}


		[HttpPost]
		public JsonResult SaveRejectEntry(int[] shopFloorIDs, ShopFloorRej model)
		{
			model.ModifiedBy = db.GetUserIDByUsername(User.Identity.Name);
			model.ModifiedOn = DateTime.Now;
			db.SaveReject(model, shopFloorIDs);

			return Json(true, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public JsonResult DeleteRejectEntry(int rejectId, int[] shopFloorIDs)
		{
			db.DeleteReject(rejectId, shopFloorIDs);
			return Json(true, JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetAllQAParamResourcesByResourceCode(int id, string resourceCode)
		{
			var dbQA = new ShopFloorQAParManager();
			return Json(dbQA.GetAllByShopFloorID(id, resourceCode), JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public JsonResult SaveQAParam(int[] shopFloorIDs, ShopFloorQAPar model)
		{
			var dbQA = new ShopFloorQAParManager();
			model.ModifiedBy = dbQA.GetUserIDByUsername(User.Identity.Name);
			model.ModifiedOn = DateTime.Now;
			dbQA.SaveQAParamValues(shopFloorIDs, model);
			return Json(true, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public JsonResult AssignSubResource(SubsResource model, int[] shopFloorIDs)
		{
			model.ModifiedBy = db.GetUserIDByUsername(User.Identity.Name);
			db.AssignSubResource(model, shopFloorIDs);
			return Json(true, JsonRequestBehavior.AllowGet);
		}

		private void PopulateDropDowns(int id)
		{
			ViewBag.Resources = CreateSelectList(new MODetailRSCManager().GetAllByMOHeaderID(id), m => m.ID, m => $"{m.ResourceCode} - {m.ResourceDesc}");
			ViewBag.Activities = CreateSelectList(new ActivityTypeManager().GetAll(), m => m.ID, m => $"{m.ActivityCode} - {m.ActivityName}");
			ViewBag.RMAddlItems = CreateSelectList(db.GetRMAddlItems(id), m => m.RMItemCode, m => $"{m.RMItemCode} - {m.RMItemName}");
			ViewBag.AddddlReasons = CreateSelectList(new AdditionalReasonManager().GetAll(), m => m.ID, m => $"{m.ReasonCode} - {m.ReasonCode}");
			ViewBag.RunRateReasons = CreateSelectList(new RunRateReasonManager().GetAll(), m => m.ID, m => $"{m.ReasonCode} - {m.ReasonName}");
		}


		public ActionResult List()
		{
			return View();
		}
	}
}