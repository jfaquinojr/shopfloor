﻿using ShopFloor.Entities;
using ShopFloor.Managers;
using System.Net;
using System.Web.Mvc;


namespace ShopFloor.Web.Controllers
{
    [Authorize]
    public class AdditionalReasonsController : BaseController
    {
		private ShopFloorEntityManager<AdditionalReason> db = new ShopFloorEntityManager<AdditionalReason>(m => m.ID);

        // GET: AdditionalReasons
        public ActionResult Index()
        {
            return View();
        }

        // GET: AdditionalReasons/Create
        public ActionResult Create()
        {
            return View(new AdditionalReason { Active = true });
        }

        // POST: AdditionalReasons/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,ReasonCode,ReasonName,Active")] AdditionalReason model)
        {
            if (ModelState.IsValid)
            {
                if (!db.IsDuplicate(m => m.ReasonCode == model.ReasonCode))
                {
                    db.Create(model);
                    this.AddMessageSuccess("Record created.");
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", "The Reason Code entered already exists.");
                }

            }

            return View(model);
        }

        // GET: AdditionalReasons/Edit/5
        public ActionResult Edit(int? id)

        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AdditionalReason additionalReason = db.GetByID(id.Value);
            if (additionalReason == null)
            {
                return HttpNotFound();
            }
            return View(additionalReason);
        }

        // POST: AdditionalReasons/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,ReasonCode,ReasonName,Active")] AdditionalReason model)
        {
            if (ModelState.IsValid)
            {
                if (!db.IsDuplicate(m => m.ReasonCode == model.ReasonCode && m.ID != model.ID))
                {
                    db.Save(model);
                    AddMessageSuccess("Record saved.");
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", "There is already a Reason with the same Reason Code entered.");
                }
            }
            return View(model);
        }

        // GET: AdditionalReasons/Delete/5
        public JsonResult Delete(int id)
        {
            db.Remove(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
