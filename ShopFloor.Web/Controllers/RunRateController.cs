﻿using ShopFloor.Entities;
using ShopFloor.Managers;
using System;
using System.Web.Mvc;

namespace ShopFloor.Web.Controllers
{
    [Authorize]
    public class RunRateController : BaseController
    {
        RunRateManager db = new RunRateManager();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            PopulateDropDown();
            return View();
        }

        [HttpPost]
        public ActionResult Create(RunRate model)
        {
            if (ModelState.IsValid)
            {
                // ensure no duplicates
                if (!db.IsDuplicate(m => m.ResourceID == model.ResourceID && m.ItemRouteID == model.ItemRouteID)) 
                {
                    model.CreatedBy = db.GetUserIDByUsername(User.Identity.Name);
                    model.CreatedOn = DateTime.Now;
                    db.Create(model);
                    AddMessageSuccess("RunRate created.");
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", "A duplicate RunRate already exists.");
                }

            }

            PopulateDropDown();
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var entity = db.GetByID(id);

            PopulateDropDown();
            return View(entity);
        }

        [HttpPost]
        public ActionResult Edit(RunRate model)
        {
            if (ModelState.IsValid)
            {
                // ensure no duplicates
                if (!db.IsDuplicate(m => m.ResourceID == model.ResourceID && m.ItemRouteID == model.ItemRouteID && m.ID != model.ID))
                {
                    model.ModifiedBy = db.GetUserIDByUsername(User.Identity.Name);
                    model.ModifiedOn = DateTime.Now;
                    db.Save(model);
                    AddMessageSuccess("RunRate Saved.");
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", "A duplicate RunRate already exists.");
                }
            }

            PopulateDropDown();
            return View(model);
        }

        private void PopulateDropDown()
        {
            ViewBag.Items = this.CreateSelectList(new RoutesManager().GetItemRoutes(), m => m.ID, m => m.ItemCode + " - " + m.U_ITEMCAT01 );
            ViewBag.Resources = this.CreateSelectList(new ResourceManager().GetAll(), m => m.ID, m => m.ResourceCode + " - " + m.ResourceDesc);
        }

        public JsonResult Delete(int id)
        {
            db.Remove(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetItemCategory(string itemCode)
        {
            var s = "";
            var dbItems = new SAP.Managers.SAPItemManager();
            var found = dbItems.GetByItemCode(itemCode);
            if (found != null)
            {
                s = found.U_ITEMCAT01;
            }
            return Json(s, JsonRequestBehavior.AllowGet);
        }
    }
}