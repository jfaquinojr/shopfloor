﻿using ShopFloor.Entities;
using ShopFloor.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopFloor.Web.Controllers
{
	[Authorize]
    public class QAAppsController : BaseController
    {
        private QAAppManager db = new QAAppManager();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(QAApplication model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedBy = db.GetUserIDByUsername(User.Identity.Name);
                model.CreatedOn = DateTime.Now;
                int id = db.Create(model);

                AddMessageSuccess("Record created.");
                return RedirectToAction("Edit", new { id = id });
            }
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var model = db.GetByID(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(QAApplication model)
        {
            if (ModelState.IsValid)
            {
                model.ModifiedBy = db.GetUserIDByUsername(User.Identity.Name);
                model.ModifiedOn = DateTime.Now;
                db.Save(model);

                AddMessageSuccess("Record saved.");
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public JsonResult Delete(int id)
        {
            db.Remove(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
		
    }
}