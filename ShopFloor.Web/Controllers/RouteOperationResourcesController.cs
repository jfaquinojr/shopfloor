﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ShopFloor.Entities;
using ShopFloor.Managers;
using ShopFloor.Web.Models;
using System.Collections;


namespace ShopFloor.Web.Controllers
{
	[Authorize]
    public class RouteOperationResourcesController : BaseController
    {
        private RouteOperationResourcesManager db = new RouteOperationResourcesManager();


        public ActionResult ListByRoute(int id)
        {
            return View();
        }

        public ActionResult Index()
        {
            return View();
        }

        // GET: RouteOperationResources/Create
        public ActionResult Create()
        {
            var model = new EditRouteOperationResourceModel();
            model.Active = true;
            PopulateDropDowns();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(EditRouteOperationResourceModel model)
        {
            PopulateDropDowns();

            if (ModelState.IsValid)
            {
                // check if duplicate
                if (!db.IsDuplicate(model.RouteID, model.OperationID, model))
                {
                    model.CreatedBy = db.GetUserIDByUsername(User.Identity.Name);
                    model.RouteOperationID = db.GetOrCreateRouteOperation(model.RouteID, model.OperationID, model.CreatedBy.Value);
                    int id = db.Create(model);
                    AddMessageSuccess("Routing Setup successfully saved.");
                    return RedirectToAction("Edit", new { id = id });
                }
                else
                {
                    ModelState.AddModelError("", "There is already a duplicate entry for this Routing Setup");
                }
            }

            return View(model);
        }



        // GET: RouteOperationResources/Edit/5
        public ActionResult Edit(int? id)

        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RouteOperationResource entity = db.GetByID(id.Value);
            if (entity == null)
            {
                return HttpNotFound();
            }

            var model = db.Map(entity, new EditRouteOperationResourceModel());
            model.RouteID = entity.RouteOperation.RouteID;
            model.OperationID = entity.RouteOperation.OperationID;
            
            PopulateDropDowns();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditRouteOperationResourceModel model)
        {
            PopulateDropDowns();

            if (ModelState.IsValid)
            {
                if (!db.IsDuplicate(model.RouteID, model.OperationID, model))
                {
                    model.ModifiedBy = db.GetUserIDByUsername(User.Identity.Name);
                    db.Save(model);
                    AddMessageSuccess("Routing Setup successfully saved.");
                    return View(model);
                }

                ModelState.AddModelError("", "A duplicate Routing Setup found.");
            }

            return View(model);
        }


        // GET: RouteOperationResources/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            db.Remove(id.Value);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [AllowAnonymous]
        public JsonResult GetResourceDesc(int? id)
        {
            if (id == null) return Json("", JsonRequestBehavior.AllowGet);
            return Json(db.GetResourceDesc(id.Value), JsonRequestBehavior.AllowGet);
        }


        private void PopulateDropDowns()
        {
            ViewBag.Operations = CreateSelectList(new ShopFloorEntityManager<Operation>(m => m.ID).GetAll(), m => m.ID, m => m.OperCode);
            ViewBag.Routes = CreateSelectList(new RoutesManager().GetAll(), m => m.ID, m => m.RouteCode);
            ViewBag.ResourceList = CreateSelectList(new ResourceManager().GetAll(), m => m.ID, m => m.ResourceCode);
        }
    }
}
