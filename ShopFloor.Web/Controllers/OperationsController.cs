﻿using ShopFloor.Entities;
using ShopFloor.Managers;
using System;
using System.Net;
using System.Web.Mvc;

namespace ShopFloor.Web.Controllers
{
    [Authorize]
    public class OperationsController : BaseController
    {
        private ShopFloorEntityManager<Operation> db = new ShopFloorEntityManager<Operation>(m => m.ID);

        // GET: Operations
        public ActionResult Index()
        {
            return View(db.GetAll());
        }

        public ActionResult Create()
        {
            return View(new Operation { Active = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Operation operation)
        {
            if (ModelState.IsValid)
            {
                if (!db.IsDuplicate(m => m.OperCode == operation.OperCode))
                {
                    operation.CreatedBy = db.GetUserIDByUsername(User.Identity.Name);
                    operation.CreatedOn = DateTime.Now;
                    operation.Sequence = db.GetNextSequence(m => m.Sequence);
                    db.Create(operation);
                    AddMessageSuccess("Record successfully created.");
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("OperCode", "Duplicate Operation Code found.");
            }
            return View(operation);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Operation operation = db.GetByID(id.Value);
            if (operation == null)
            {
                return HttpNotFound();
            }

            return View(operation);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Operation operation)
        {
            if (ModelState.IsValid)
            {
                if (!db.IsDuplicate(m => m.OperCode == operation.OperCode && m.ID != operation.ID))
                {
                    operation.ModifiedBy = db.GetUserIDByUsername(User.Identity.Name);
                    operation.ModifiedOn = DateTime.Now;
                    db.Save(operation);
                    AddMessageSuccess("Record saved.");
                    return RedirectToAction("Index");
                }

                ModelState.AddModelError("OperCode", "Duplicate Operation Code found.");

            }
            return View(operation);
        }

        public JsonResult Delete(int id)
        {
            db.Remove(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
