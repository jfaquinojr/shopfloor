﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ShopFloor.Entities;
using ShopFloor.Managers;
using ShopFloor.Web.Models;
using ShopFloor.SAP.Managers;
using ShopFloor.Web.Models.DataTables;
using System.Configuration;

namespace ShopFloor.Web.Controllers
{
    [Authorize]
    public class BOMController : BaseController
    {
        private BOMManager db = new BOMManager();

        public ActionResult Index()
        {
            ViewBag.Title = "BOM";
            ViewBag.ActiveMenu = "BOM";
            return View();
        }

        // GET: BOM/Create
        public ActionResult Create()
        {
            var model = new EditBOMModel();
            return View(model);
        }

        public ActionResult EditBOMDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BillOfMaterial entity = db.GetByID(id.Value);
            if (entity == null)
            {
                return HttpNotFound();
            }
            var model = db.Map(entity, new EditBOMModel());
            //model.FGItemCodes = CreateSelectList(new SAP.Managers.SAPEntityManager().GetAllItems(), m => m.ItemCode, m => m.ItemDescription);
            model.BOMDetails = new BOMDetailManager(entity.ID).GetAll();

            return View(model);
        }

		public ActionResult SubstituteRM(int id)
		{
			var dbBOMDetail = new BOMDetailManager(id);
			ViewBag.RMCodePrimes = CreateSelectList(dbBOMDetail.QueryAllIncluding(m => m.BillOfMaterial).Where(t => t.BOMHeaderID == id).ToList(), m => m.ID, o => $"{o.RMCode}|{o.RMName}");

			BillOfMaterial entity = db.GetByID(id);
			if (entity == null)
			{
				return HttpNotFound();
			}
			var model = db.Map(entity, new EditBOMModel());
			model.BOMDetails = new BOMDetailManager(entity.ID).GetAll();

			return View(model);
		}

		[HttpPost]
		public ActionResult Create(EditBOMModel model)
		{
			return Persist(model, true);
		}

		[HttpPost]
		public ActionResult Copy(EditBOMModel model)
		{
			var copied = new EditBOMModel();
			copied.BOMHeadeIDCopy = model.ID;

			var bom = db.GetByID(model.ID);
			copied.BOMCode = bom.BOMCode;
			copied.EffectFrom = bom.EffectFrom;
			copied.EffectTo = bom.EffectTo;
			ModelState.Clear();
			return View("Create", copied);
		}

		[HttpPost]
        public ActionResult EditBOMDetails(EditBOMModel model)
        {
			return SaveBOMDetails(model, "EditBOMDetails");
		}

		[HttpPost]
		public ActionResult ItemAssignment(EditBOMModel model)
		{
			return SaveBOMDetails(model, "ItemAssignment");
		}

		[HttpPost]
		public ActionResult SubstituteRM(EditBOMModel model)
		{
			var dbBOMDetail = new BOMDetailManager(model.ID);
			ViewBag.RMCodePrimes = CreateSelectList(dbBOMDetail.QueryAllIncluding(m => m.BillOfMaterial).Where(t => t.BOMHeaderID == model.ID).ToList(), m => m.ID, o => $"{o.RMCode}|{o.RMName}");
			return SaveBOMDetails(model, "SubstituteRM");
		}

		private ActionResult SaveBOMDetails(EditBOMModel model, string action)
		{
			if (!model.IsFinal.GetValueOrDefault(false))
				return Persist(model, false);

			var entity = db.GetByID(model.ID);
			entity.EffectTo = model.EffectTo;
			db.Save(entity);
			AddMessageSuccess("Record saved.");

			return RedirectToAction(action, new { id = model.ID });
		}


		public ActionResult MarkAsFinal(int id)
        {
            BillOfMaterial entity = db.GetByID(id);
            if (entity == null)
            {
                return HttpNotFound();
            }

            TempData[Constants.CONST_MESSAGE_SUCCESS] = "BOM successfully marked as final.";
            db.MarkAsFinal(id, User.Identity.Name);

            return RedirectToAction("EditBOMDetails", new { id = id });
        }


        private ActionResult Persist(EditBOMModel model, bool isNew)
        {
            if (ModelState.IsValid)
            {
                // check for duplicate BOMs
				if (!db.IsDuplicate(m => m.BOMCode == model.BOMCode && m.BOMType == model.BOMType && m.BOMSeries == model.BOMSeries && m.ID != model.ID))
                {
                    if (isNew)
                    {
                        model.UserID = new UsersManager().GetByUsername(User.Identity.Name).ID;
                        model.CreatedBy = model.UserID;
                        model.CreatedOn = DateTime.Now;
                        int id = db.Create(model);

						db.Copy(model.BOMHeadeIDCopy.GetValueOrDefault(0), id);

                        AddMessageSuccess("BOM " + model.BOMName + " successfully created.");
                        return RedirectToAction("EditBOMDetails", new { id = id });
                    }
                    else
                    {
                        model.ModifiedBy = new UsersManager().GetByUsername(User.Identity.Name).ID;
                        model.ModifiedOn = DateTime.Now;
                        db.Save(model);
                        AddMessageSuccess("BOM " + model.BOMName + " successfully saved.");
                    }

                    //model.FGItemCodes = CreateSelectList(new SAP.Managers.SAPEntityManager().GetAllItems(), m => m.ItemCode, m => m.ItemDescription);
                    model.BOMDetails = new BOMDetailManager(model.ID).GetAll();

                    return View(model);
                }
                else
                {
					string errMsg = $"Please specify a different value. {model.BOMSeries} already exists for {model.BOMTypeName} + {model.BOMCode}.";
					ModelState.AddModelError("BOMSeries", errMsg);
                }
            }

            return View(model);
        }

        // GET: BOM/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            db.Remove(id.Value);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public JsonResult IsItemCodeValid(string code)
        {
            bool ret = new SAPItemManager().IsDuplicate(m => m.ItemCode == code);
            return Json(ret, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ItemAssignment(int id)
        {
            BillOfMaterial entity = db.GetByID(id);
            if (entity == null)
            {
                return HttpNotFound();
            }
            var model = db.Map(entity, new EditBOMModel());
            //model.FGItemCodes = CreateSelectList(new SAP.Managers.SAPEntityManager().GetAllItems(), m => m.ItemCode, m => m.ItemDescription);
            model.BOMDetails = new BOMDetailManager(entity.ID).GetAll();

            return View(model);
        }

        //[HttpPost]
        //public ActionResult ItemAssignment(EditBOMModel model)
        //{
        //    model.BOMDetails = new BOMDetailManager(model.ID).GetAll();
        //    return Persist(model, false);
        //}


        public ContentResult ItemAssignments(int id, SearchParams args)
        {
            var items = db.GetBOMItems(id);
            return Content(this.CreateDTReturnData(items, items.Count, items.Count, args.draw), "application/json");
        }

        public JsonResult CreateItemBOM(int id, string itemCode)
        {
            db.CreateItemBOM(id, itemCode, User.Identity.Name);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ValidateItem(int id, string itemCode)
        {

            // check if this item is already assigned to THIS BOM
            BillOfMaterial bom = db.GetBOMByItemCode(id, itemCode);
            if( bom != null)
            {
                return Json("This FG is already assigned to this BOM. Please choose a different Item.", JsonRequestBehavior.AllowGet);
            }

            // check if this item is already assigned to a different BOM
            if (db.IsItemBOMAttached(itemCode))
            {
                return Json("This FG is already assigned to another BOM. Please choose a different Item.", JsonRequestBehavior.AllowGet);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteItemBOM(int id)
        {
            db.DeleteItemBOM(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRawMaterial(string itemCode)
        {
            var dbItem = new SAP.Managers.SAPRawMaterialsManager();
            var item = dbItem.GetByID(itemCode);
            var retval = new
            {
                DocEntry = item.DocEntry,
                InvntryUom = item.InvntryUom,
                ItemCode = item.ItemCode,
                ItemName = item.ItemName,   
                ItmsGrpCod = item.ItmsGrpCod,
                U_ITEMCAT01 = item.U_ITEMCAT01,
                SAPUnitCost = dbItem.GetPrice(itemCode).Price
            };
            return Json(retval, JsonRequestBehavior.AllowGet);
        }



        public JsonResult IsUniqueBOMSeries(EditBOMModel model)
        {
			if (!db.QueryAll().Any(m => m.BOMSeries == model.BOMSeries.Trim() && m.BOMType == model.BOMType && m.BOMCode == model.BOMCode && m.ID != model.ID))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }

			string errMsg = $"Please specify a different value. {model.BOMSeries} already exists for {model.BOMTypeName} + {model.BOMCode}.";

            return Json(errMsg, JsonRequestBehavior.AllowGet);
        }

    }
}
