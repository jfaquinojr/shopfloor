﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ShopFloor.Entities;
using ShopFloor.Managers;
using ShopFloor.Web.Models;


namespace ShopFloor.Web.Controllers
{
    [Authorize]
    public class RouteOperationsController : BaseController
    {
        private RouteOperationsManager db = new RouteOperationsManager();

        // GET: RouteOperations
        public ActionResult Index()
        {
            return View();
        }

        // GET: RouteOperations/Create
        public ActionResult Create()
        {
            PopulateDropDowns();
            return View();
        }

        public ActionResult ReCreate(int rid)
        {
            PopulateDropDowns();
            EditRouteOperationModel model = new EditRouteOperationModel();
            model.RouteID = rid;
            return View("Create", model);
        }

        [HttpPost]
        public ActionResult ReCreate(EditRouteOperationModel model)
        {
            return Create(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(EditRouteOperationModel model)
        {
            PopulateDropDowns();
            if (ModelState.IsValid)
            {
                if (!db.IsDuplicate(m => m.RouteID == model.RouteID && m.OperationID == model.OperationID))
                {
                    model.CreatedBy = db.GetUserIDByUsername(User.Identity.Name);
                    model.CreatedOn = DateTime.Now;
                    int id = db.Create(model);
                    AddMessageSuccess("Route Operation created successfully.");
                    return RedirectToAction("Edit", new { id = id });
                }

                ModelState.AddModelError("", "Duplicate Route Operation found.");
            }

            return View("Create", model);
        }

        // GET: RouteOperations/Edit/5
        public ActionResult Edit(int? id)
        {
            PopulateDropDowns();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var e = db.GetByID(id.Value);
            if (e == null)
            {
                return HttpNotFound();
            }

            var model = db.Map(e, new EditRouteOperationModel());
            
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditRouteOperationModel model)
        {
            PopulateDropDowns();
            if (ModelState.IsValid)
            {
                if (!db.IsDuplicate(m => m.RouteID == model.RouteID && m.OperationID == model.OperationID && m.ID != model.ID))
                {
                    model.ModifiedBy = db.GetUserIDByUsername(User.Identity.Name);
                    model.ModifiedOn = DateTime.Now;
                    db.Save(model);
                    AddMessageSuccess("Route Operation saved successfully.");
                    return View(model);
                }

                ModelState.AddModelError("", "Duplicate Route Operation found.");
            }

            return View(model);
        }

        // GET: RouteOperations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            db.Remove(id.Value);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private void PopulateDropDowns()
        {
            ViewBag.Operations = this.CreateSelectList(new ShopFloorEntityManager<Operation>(m => m.ID).GetAll(), m => m.ID, m => m.OperCode + " - " + m.OperDesc);
            ViewBag.Routes = this.CreateSelectList(new RoutesManager().GetAll(), m => m.ID, m => m.RouteCode + " - " + m.RouteDesc);
        }
    }
}
