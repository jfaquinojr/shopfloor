﻿using ShopFloor.Entities;
using ShopFloor.Managers;
using System;
using System.Linq;
using System.Web.Mvc;

namespace ShopFloor.Web.Controllers
{
    [Authorize]
    public class MOTypesController : SearchableController<MOType>
    {
        public MOTypesController() : base(m=>m.ID, (list, filter) => { return list.Where(m => m.MOTypeCode == filter || m.MOTypeName.Contains(filter)); })
        {

        }

        // GET: MOTypes
        public ActionResult Index()
        {
            return View();
        }


        // GET: MOTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MOTypes/Create
        [HttpPost]
        public ActionResult Create(MOType model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    model.CreatedBy = db.GetUserIDByUsername(User.Identity.Name);
                    model.CreatedOn = DateTime.Now;
                    db.Create(model);
                    AddMessageSuccess("Record Created.");
                    return RedirectToAction("Index");
                }

                return View(model);
            }
            catch(Exception ex)
            {
                log.Error("Error creating MOType", ex);
                return View(model);
            }
        }

        // GET: MOTypes/Edit/5
        public ActionResult Edit(int id)
        {
            return View(db.GetByID(id));
        }

        // POST: MOTypes/Edit/5
        [HttpPost]
        public ActionResult Edit(MOType model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var entity = db.GetByID(model.ID);
                    db.Map(model, entity);
                    entity.ModifiedBy = db.GetUserIDByUsername(User.Identity.Name);
                    entity.ModifiedOn = DateTime.Now;
                    db.Save(entity);
                    AddMessageSuccess("Record Saved.");
                    return RedirectToAction("Index");
                }

                return View(model);
            }
            catch (Exception ex)
            {
                log.Error("Error saving MOType", ex);
                return View(model);
            }
        }

		public JsonResult GetMOTypeID(string moTypeCode)
		{
			int ret = 0;
			var motype = db.QueryAll().FirstOrDefault(m => m.MOTypeCode.ToUpper() == moTypeCode.Trim().ToUpper());
			if (motype != null)
			{
				ret = motype.ID;
			}
			return Json(ret, JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetMOType(int id)
		{
			return Json(db.GetByID(id), JsonRequestBehavior.AllowGet);
		}

	}
}
