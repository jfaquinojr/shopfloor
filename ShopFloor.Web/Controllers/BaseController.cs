﻿using log4net;
using Newtonsoft.Json;
using ShopFloor.Managers;
using ShopFloor.Web.Models;
using ShopFloor.Web.Models.DataTables;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace ShopFloor.Web.Controllers
{
    public class BaseController : Controller
    {
        protected static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
			var expireDate = new DateTime(DateTime.Now.Year, 9, 10, 0, 0, 0, 0, DateTimeKind.Local); // its my birthday!
			if (DateTime.Now > expireDate) 
			{
				var key = ConfigurationManager.AppSettings["Serial"];
				if (key != "BDC5A583-FDA6-416C-BD44-7F11E5F2249B")
				{
					log.Info($"Client license expired on {expireDate}.");
					filterContext.Result = RedirectToAction("Index", "License"); 
					return;
				}
			}
            base.OnActionExecuting(filterContext);
        }

        public IEnumerable<SelectListItem> CreateSelectList<T>(IList<T> entities, Func<T, object> propertyValue, Func<T, object> propertyText)
        {
            return entities
                    .Select(x => new SelectListItem
                    {
                        Value = propertyValue(x).ToString(),
                        Text = propertyText(x).ToString()
                    });
        }

        public void AddMessageSuccess(string msg)
        {
            TempData[Constants.CONST_MESSAGE_SUCCESS] = msg;
        }
        public void AddMessageError(string msg)
        {
            TempData[Constants.CONST_MESSAGE_ERROR] = msg;
        }
        public void AddMessageInfo(string msg)
        {
            TempData[Constants.CONST_MESSAGE_INFO] = msg;
        }
        public void AddMessageWarning(string msg)
        {
            TempData[Constants.CONST_MESSAGE_WARNING] = msg;
        }

        protected string CreateDTReturnData(IList result, int filteredRec, int totalRec, int draw)
        {
            ReturnData model = new ReturnData();
            model.data = result;
            model.recordsFiltered = filteredRec;
            model.recordsTotal = totalRec;
            model.draw = draw;

            string data = JsonSerialize(model);

            return data;
        }

        protected string JsonSerialize(object o)
        {
            return JsonConvert.SerializeObject(o, Formatting.None,
                new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
        }

    }

}