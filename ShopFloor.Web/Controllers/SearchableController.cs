﻿using ShopFloor.Managers;
using ShopFloor.Web.Models.DataTables;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ShopFloor.Web.Controllers
{
    public class SearchableController<TEntity> : BaseController
        where TEntity : class
    {

        protected ShopFloorSearchableEntities<TEntity> db;

        public SearchableController(Expression<Func<TEntity, int>> id, Func<DbSet<TEntity>, string, IQueryable<TEntity>> searchAction)
        {
            db = new ShopFloorSearchableEntities<TEntity>(id, searchAction);
        }


        /// <summary>
        /// Designed for use by shopofloor.datatable.js
        /// </summary>
        public ContentResult AjaxSearch(SearchParams args)
        {
            //HttpUtility.ParseQueryString(new Uri(fullUrl).Query)
            int page, row;
            string table = args.Table;
            string filter = args.Filter ?? "";
            string sortcolumn = "";
            string direction = "asc";
            if (args.order?.Length > 0 && args.columns.Length >= args.order.Length)
            {
                sortcolumn = args.columns[args.order[0].column].data;
                direction = args.order[0].dir;
            }
            string searchvalue = "";
            if (args.search != null) searchvalue = args.search.value;


            IList result = db.Search(searchvalue ?? "", args.length, args.start, out page, out row, sortcolumn, direction); // search all

            var data = CreateDTReturnData(result, page, row, args.draw);

            return Content(data, "application/json");
        }
    }
}
