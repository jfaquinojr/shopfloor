﻿using ShopFloor.Entities;
using ShopFloor.Managers;
using ShopFloor.Web.Models;
using System;
using System.Net;
using System.Web.Mvc;

namespace ShopFloor.Web.Controllers
{
    [Authorize]
    public class UsersController : BaseController
    {
        UsersManager db = new UsersManager();

        // GET: Users
        public ActionResult Index()
        {
            return View();
        }


        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.GetByID(id.Value);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            var model = new EditUserModel();
            PopulateDropDown();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(EditUserModel user)
        {
            if (ModelState.IsValid && !IsDuplicate(user))
            {
				// users entered this way are automatically verified by current user
				var entity = new User();
				entity.UserID = user.UserID;
				entity.EmpID = user.EmpID;
				entity.UserName = user.UserName;

				entity.IsVerified = true;
				entity.VerifiedOn = DateTime.Now;
                var verifiedby = db.GetByUsername(this.User.Identity.Name);
				entity.VerifiedBy = verifiedby.ID;
				entity.UserPW = db.HashText(user.UserPW);
				entity.ChangePassword = user.ChangePassword;

                db.Create(entity);

                // create and assign user class
                var dbUC = new UserClassManager();
                dbUC.ChangeUserClassFor(user.UserName, user.UserClassID, User.Identity.Name);

                return RedirectToAction("Index");
            }

            PopulateDropDown();
            return View(user);
        }

        // GET: Users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var user = db.GetByID(id.Value);
            if (user == null)
            {
                return HttpNotFound();
            }

            EditUserModel model = new EditUserModel();
            db.Map(user, model);

			model.UserClassID = db.GetUserClass(model.ID)?.ID ?? 0;

            PopulateDropDown();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditUserModel model)
        {
            if (ModelState.IsValid && !IsDuplicate(model))
            {
				var entity = db.GetByID(model.ID);
				entity.UserName = model.UserName;
				entity.UserID = model.UserID;
				entity.EmpID = model.EmpID;

                if (model.UserPW != entity.UserPW)
                    entity.UserPW = db.HashText(model.UserPW);
				entity.ModifiedBy = db.GetUserIDByUsername(this.User.Identity.Name);
				entity.ModifiedOn = DateTime.Now;
				entity.ChangePassword = model.ChangePassword;

                db.Save(entity);

				var dbUC = new UserClassManager();
				dbUC.ChangeUserClassFor(model.UserName, model.UserClassID, User.Identity.Name);

				AddMessageSuccess("User successfully saved.");
                return RedirectToAction("Index");
            }

            PopulateDropDown();
            return View(model);
        }

        public JsonResult DeleteRecord(int id)
        {
            db.Remove(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        public ActionResult UserRegistrations()
        {
            return View(new UsersManager().GetUserRegistrations());
        }

        public ActionResult GrantAccess(int id)
        {
            var mgr = new UsersManager();
            var user = mgr.GetByID(id);
            if (user.IsVerified.HasValue && !user.IsVerified.Value)
            {
                mgr.GrantAccess(id, this.User.Identity.Name);
                return View(user);
            }

            return View("AlreadyGranted", user);
        }

        private void PopulateDropDown()
        {
            var dbUC = new UserClassManager();
            ViewBag.UserClasses = this.CreateSelectList(dbUC.GetAll(), m => m.ID, m => m.UClassCode + " - " + m.UClassDesc);
        }

		public ActionResult IsValidUser(EditUserModel model)
		{
			if (IsDuplicate(model))
			{
				return Json("This username is already taken.", JsonRequestBehavior.AllowGet);
			}

			return Json(true, JsonRequestBehavior.AllowGet);
		}

		private bool IsDuplicate(EditUserModel model)
		{
			var isDuplicate = db.IsDuplicate(m => m.UserName == model.UserName && m.ID != model.ID);
			return isDuplicate;
		}
    }
}
