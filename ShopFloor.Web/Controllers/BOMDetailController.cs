﻿using ShopFloor.Entities;
using ShopFloor.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopFloor.Web.Controllers
{
    public class BOMDetailController : BaseController
    {
        private BOMDetailManager db = new BOMDetailManager(0);

        // GET: BOMDetail
        public ActionResult Index()
        {
            return View();
        }

        // GET: BOMDetail/Create
        public ActionResult Create()
        {
            return View();
        }

        public PartialViewResult CreatePartial(int id) //the id passed should be the BOMHeaderID 
        {
            var model = new BillOfMaterialDetail();
            model.BOMHeaderID = id;
            return PartialView("Create", model);
        }

        [HttpPost]
        public JsonResult CreatePartial(BillOfMaterialDetail model)
        {
            model.UserID = new UsersManager().GetByUsername(this.User.Identity.Name).ID;
            model.CreatedBy = db.GetUserIDByUsername(User.Identity.Name);
            model.CreatedOn = DateTime.Now;

			var dbSAPRMMgr = new SAP.Managers.SAPRawMaterialsManager();
			var item = dbSAPRMMgr.GetByID(model.RMCode);
            model.RMName = item.ItemName;
            model.UOM = item.InvntryUom;
            model.UnitCost = BOMDetailManager.ComputeUnitCost(model.RMFactor ?? 0, model.UOMFactor ?? 0, model.RMQuantity, model.SAPUnitCost ?? 0);

            db.Create(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ComputeUnitCost(decimal rmFactor, decimal uomFactor, decimal rmQty, decimal price)
        {
            return Json(BOMDetailManager.ComputeUnitCost(rmFactor, uomFactor, rmQty, price), JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult EditPartial(int id)
        {
            var entity = new BOMDetailManager(0).GetByID(id);
            var model = db.Map(entity, new BillOfMaterialDetail());

            var item = new SAP.Managers.SAPRawMaterialsManager().GetByID(model.RMCode);
            model.RMName = item.ItemName;
            model.UOM = item.InvntryUom;

            return PartialView("Create", model);
        }

        [HttpPost]

        public JsonResult EditPartial(BillOfMaterialDetail model)
        {
            model.ModifiedBy = db.GetUserIDByUsername(User.Identity.Name);
            model.ModifiedOn = DateTime.Now;

            var item = new SAP.Managers.SAPRawMaterialsManager().GetByID(model.RMCode);
            model.RMName = item.ItemName;
            model.UOM = item.InvntryUom;

            db.Save(model);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        // POST: BOMDetail/Create
        [HttpPost]
        public ActionResult Create(BillOfMaterialDetail model)
        {
            try
            {
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: BOMDetail/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: BOMDetail/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: BOMDetail/Delete/5
        public JsonResult Delete(int id)
        {
            new BOMDetailManager(0).Remove(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

    }
}
