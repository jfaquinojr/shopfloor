﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ShopFloor.Entities;
using ShopFloor.Managers;
using ShopFloor.Web.Models;

namespace ShopFloor.Web.Controllers
{
	[Authorize]
    public class ResourcesController : BaseController
    {
        private ResourceManager db = new ResourceManager();

        // GET: Resources
        public ActionResult Index()
        {
            return View();
        }

        // GET: Resources/Create
        public ActionResult Create()
        {
            var resourceCategoriesMgr = new ShopFloorEntityManager<ResourceCategory>(m=>m.ID);

            var model = new EditResourceModel();
            model.Active = true;
            model.UnitTag = true;

            PopulateDropdowns();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(EditResourceModel model)
        {
            if (ModelState.IsValid)
            {
				db.Create(model);
                AddMessageSuccess("Record created.");
                return RedirectToAction("Index");
            }

            PopulateDropdowns();
            return View(model);
        }

        // GET: Resources/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var entity = db.GetByID(id.Value);
            if (entity == null)
            {
                return HttpNotFound();
            }
            var model = db.Map(entity, new EditResourceModel());

            PopulateDropdowns();

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(EditResourceModel resource)
        {
            if (ModelState.IsValid)
            {
                db.Save(resource);
                AddMessageSuccess("Record saved.");
                return RedirectToAction("Index");
            }
            PopulateDropdowns();
            return View(resource);
        }

        // GET: Resources/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            db.Remove(id.Value);
            return RedirectToAction("Index");
        }

        private void PopulateDropdowns()
        {
            ViewBag.PrcCodes = this.CreateSelectList(new SAPPrcManager().GetAll(), m => m.PrcCode, m => m.PrcCode + " - " + m.PrcName);
            ViewBag.Categories = this.CreateSelectList(new ResourceCategoryManager().GetAll(), m => m.ID, m => m.Code + " - " + m.Description);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public JsonResult GetAllQAParamCodes()
        {
            var dbQA = new QAParamManager();
            return Json(dbQA.GetAll(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllQAParamResources(int id)
        {
            var dbQA = new QAParamResourceManager();
            return Json(dbQA.GetAllByResourceID(id), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllQAParamResourcesByResourceCode(string resourceCode)
        {
            var dbQA = new QAParamResourceManager();
            return Json(dbQA.GetAllByResourceCode(resourceCode), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddQAParam(QAParamResource model)
        {
            var dbPR = new QAParamResourceManager();
            model.CreatedBy = dbPR.GetUserIDByUsername(User.Identity.Name);
            model.CreatedOn = DateTime.Now;
            //model.QAParamID = model.QAParamID;
            //model.ResourceID = model.ResourceID;
            int id = dbPR.Create(model);
            return Json(dbPR.GetByID(id), JsonRequestBehavior.AllowGet);
        }

        public JsonResult RemoveQAParam(int qaParamId)
        {
            db.DeleteQAParamResources(qaParamId);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllResources()
        {
            var result = db.GetAll();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllSubresources(int id)
        {
            var result = db.GetAllSubresources(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllSubresourcesByCode(string resourceCode)
        {
            var result = db.GetAllSubresourcesByCode(resourceCode);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        public JsonResult AddSubresource(SubsResource sub)
        {
            sub.CreatedBy = db.GetUserIDByUsername(User.Identity.Name);
            sub.CreatedOn = DateTime.Now;
            sub.ResourceID = sub.ResourceID;
            sub.SubResourceID = sub.SubResourceID;
            db.AddSubresource(sub);
            return Json(sub, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteSubresource(int subResourceId)
        {
            db.DeleteSubresource(subResourceId);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}
