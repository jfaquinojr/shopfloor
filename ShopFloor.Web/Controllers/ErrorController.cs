﻿using ShopFloor.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopFloor.Web.Controllers
{
    [AllowAnonymous]
    public class ErrorController : Controller
    {
        public ActionResult Index()
        {
            ErrorInfo model = new ErrorInfo();
            model.Exception = new Exception("Unknown Error");
            return View("Error", model);
        }
        public ActionResult NotFound()
        {
            //Response.StatusCode = 200;  //you may want to set this to 200
            return View();
        }
    }
}