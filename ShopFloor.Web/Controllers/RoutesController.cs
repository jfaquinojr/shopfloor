﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ShopFloor.Entities;
using ShopFloor.Web.Models;
using ShopFloor.Managers;
using ShopFloor.SAP.Managers;

namespace ShopFloor.Web.Controllers
{
    [Authorize]
    public class RoutesController : BaseController
    {
        private RoutesManager db = new RoutesManager();

        // GET: Routes
        public ActionResult Index()
        {
            return View();
        }

        // GET: Routes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Route route = db.GetByID(id.Value);
            if (route == null)
            {
                return HttpNotFound();
            }
            return View(route);
        }

        // GET: Routes/Create
        public ActionResult Create()
        {
            var model = new EditRouteModel();
            model.Active = true;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(EditRouteModel model)
        {
            if (ModelState.IsValid)
            {
                if (!db.IsDuplicate(m => m.RouteCode == model.RouteCode))
                {
                    if (ModelState.IsValid)
                    {
                        model.CreatedOn = DateTime.Now;
                        model.CreatedBy = db.GetUserIDByUsername(User.Identity.Name);
                        db.Create(model);
                        AddMessageSuccess(model.RouteCode + " route has been created.");
                        return RedirectToAction("Index");
                    }

                }
                else
                {
                    ModelState.AddModelError("RouteCode", "A Route with the same RouteCode already exists.");
                }
            }

            return View(model);
        }

        // GET: Routes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Route route = db.GetByID(id.Value);
            if (route == null)
            {
                return HttpNotFound();
            }

            EditRouteModel model = db.Map(route, new EditRouteModel());
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditRouteModel model)
        {
            if (ModelState.IsValid)
            {
                if (!db.IsDuplicate(m => m.RouteCode == model.RouteCode && m.ID != model.ID))
                {
                    if (ModelState.IsValid)
                    {
                        model.ModifiedOn = DateTime.Now;
                        model.ModifiedBy = db.GetUserIDByUsername(User.Identity.Name);
                        db.Save(model);
                        AddMessageSuccess(model.RouteCode + " route has been updated.");
                        return RedirectToAction("Index");
                    }
                }
                else
                {
                    ModelState.AddModelError("RouteCode", "A Route with the same RouteCode already exists.");
                }

            }

            return View(model);
        }

        // GET: Routes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            db.Remove(id.Value);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult AssociateOperation(int id)
        {
            var mgr = new RoutesManager();
            var opmgr = new ShopFloorEntityManager<Operation>(m => m.ID);
            var route = mgr.GetByID(id);

            var model = new EditRouteOperationModel();
            model.RouteID = id;
            model.RouteCode = route.RouteCode;
            model.RouteDesc = route.RouteDesc;
            ViewBag.Operations = opmgr.GetAll();

            return View(model);
        }

        [HttpPost]
        public ActionResult AssociateOperation(EditRouteOperationModel model)
        {
            if (ModelState.IsValid)
            {
                // check for duplicates
                var routeOpMgr = new RouteOperationsManager();
                if (!routeOpMgr.IsDuplicate(m => m.RouteID == model.RouteID || m.OperationID == model.OperationID))
                {
                    // add 
                    routeOpMgr.Create(new RouteOperation { OperationID = model.OperationID, RouteID = model.RouteID });

                    // go back to list
                    return RedirectToAction("Operations", new { id = model.RouteID });
                }

                ModelState.AddModelError("", "This Operation is already associated with this Route.");
            }

            var opmgr = new ShopFloorEntityManager<Operation>(m => m.ID);
            ViewBag.Operations = opmgr.GetAll();

            return View(model);
        }


        public JsonResult GetSAPItemDetails(string itemcode)
        {
            var mgr = new SAP.Managers.SAPItemManager();
            var model = mgr.GetByItemCode(itemcode);
            return Json(model, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetRouteDetailsByID(int id)
        {
            var entity = db.GetByID(id);
            return Json(entity, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRouteDetailsByItemCode(string itemCode)
        {
            var entity = db.GetByItemCode(itemCode);
            return Json(entity, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetItemRouteByItemCode(string itemCode)
        {
            var entity = db.GetItemRouteByItemCode(itemCode);
            return Json(entity, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteRoute(int id)
        {
            db.Remove(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }


        #region RouteOperations
        public JsonResult GetOperations()
        {
            var jsondata = new ShopFloorEntityManager<Operation>(m => m.ID).GetAll();
            return Json(jsondata, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetRouteOperations(int id)
        {
            var operations = new RouteOperationsManager().GetAllByRouteID(id);
            var jsondata = (from o in operations
                            select new
                            {
                                ID = o.ID,
                                OperationID = o.OperationID,
                                RouteID = o.RouteID,
                                OperCode = o.Operation.OperCode,
                                OperDesc = o.Operation.OperDesc,
                                Sequence = o.Sequence,
                            });
            return Json(jsondata, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult AddRouteOperation(RouteOperation operation)
        {
            var newRo = new RouteOperation();
            newRo.CreatedBy = db.GetUserIDByUsername(User.Identity.Name);
            newRo.CreatedOn = DateTime.Now;
            newRo.OperationID = operation.OperationID;
            newRo.RouteID = operation.RouteID;

            var dbRO = new RouteOperationsManager();
            int id = dbRO.Create(newRo);
            var model = dbRO.GetByID(id);

            var jsondata = new
            {
                ID = model.ID,
                RouteID = model.RouteID,
                OperationID = model.OperationID,
                OperCode = model.Operation.OperCode,
                OperDesc = model.Operation.OperDesc,
                Sequence = model.Sequence,
            };

            return Json(jsondata, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteRouteOperation(int routeOperationId)
        {
            var dbRO = new RouteOperationsManager();
            dbRO.Remove(routeOperationId);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region RouteResources
        public JsonResult GetAllResources()
        {
            var dbRes = new ResourceManager();
            return Json(dbRes.GetAll(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetRouteResources(int id, int operationId)
        {
            var dbROR = new RouteOperationResourcesManager();
            List<RouteOperationResource> result;
            if (operationId > 0)
                result = dbROR.GetByRouteID(id, operationId);
            else
                result = dbROR.GetByRouteID(id);

            var jsondata = GetJSOnRouteResources(result);

            return Json(jsondata, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddRouteResource(RouteOperationResource routeResource)
        {
            var newRecord = new RouteOperationResource();
            newRecord.CreatedBy = db.GetUserIDByUsername(User.Identity.Name);
            newRecord.CreatedOn = DateTime.Now;

            newRecord.Active = true;
            newRecord.ResourceID = routeResource.ResourceID;
            newRecord.RouteOperationID = routeResource.RouteOperationID;

            var dbRO = new RouteOperationResourcesManager();
            int id = dbRO.Create(newRecord);
            var model = dbRO.GetByID(id);

            var jsondata = GetJSONRouteResource(model);

            return Json(jsondata, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteRouteResource(int routeOperationResourceId)
        {
            var dbRO = new RouteOperationResourcesManager();
            dbRO.Remove(routeOperationResourceId);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateRouteResourceLastTag(int routeOperationResourceId)
        {
            var dbRO = new RouteOperationResourcesManager();
            dbRO.SetLastTag(routeOperationResourceId);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        private object GetJSOnRouteResources(List<RouteOperationResource> result)
        {
            var jsondata = (from t in result
                            select GetJSONRouteResource(t))
                            .ToList();
            return jsondata;
        }

        private object GetJSONRouteResource(RouteOperationResource t)
        {
            return new
            {
                ID = t.ID,
                RouteOperationID = t.RouteOperationID,
                RouteID = t.RouteOperation.RouteID,
                OperCode = t.RouteOperation.Operation.OperCode,
                OperDesc = t.RouteOperation.Operation.OperDesc,
                ResourceID = t.ResourceID,
                ResourceCode = t.Resource.ResourceCode,
                ResourceDesc = t.Resource.ResourceDesc,
                Sequence = t.Sequence,
                LastTag = t.LastTag,
            };
        }

        #endregion

        #region Item Routes

        public JsonResult GetItemAssignments(int id)
        {
            var dbIR = new ItemRouteManager();
            return Json(dbIR.GetAllByRouteID(id), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ItemAssignmentExists(string itemcode) // return route if exists
        {
            var route = db.GetByItemCode(itemcode);
            if (route != null)
                return Json(route, JsonRequestBehavior.AllowGet);

            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddItemAssignment(ItemRoute model)
        {
            var item = new SAP.Managers.SAPItemManager().GetByID(model.ItemCode);

            var dbIR = new ItemRouteManager();
            var newRec = new ItemRoute();
            newRec.CreatedBy = dbIR.GetUserIDByUsername(User.Identity.Name);
            newRec.CreatedOn = DateTime.Now;
            newRec.RouteID = model.RouteID;
            newRec.ItemCode = item.ItemCode;
            newRec.ItemName = item.ItemName;
            newRec.ItmsGrpCod = item.ItmsGrpCod;
            newRec.U_ITEMCAT01 = item.U_ITEMCAT01;

            int id = dbIR.Create(newRec);

            return Json(dbIR.GetByID(id), JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteItemAssignment(int itemRouteId) {
            var dbIR = new ItemRouteManager();
            dbIR.Remove(itemRouteId);
            return Json(true, JsonRequestBehavior.AllowGet);
        }


        #endregion

    }
}
