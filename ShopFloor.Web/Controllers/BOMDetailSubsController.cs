﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ShopFloor.Entities;
using ShopFloor.Managers;
using ShopFloor.Web.Models;
using ShopFloor.Web.Models.DataTables;

namespace ShopFloor.Web.Controllers
{
	// Replace BaseEntity with correct Type
	public class BOMDetailSubsController : SearchableController<BOMDetailSub>
	{


		public BOMDetailSubsController() : base(m => m.ID, (list, filter) => { return list.Where(m => m.RMCodePrime.Contains(filter) || m.RMCodeSubs.Contains(filter) || m.RMCodeSubsName.Contains(filter)); })
		{

		}


		// GET: BOMDetailSubs
		public ActionResult Index()
		{
			var db = new ShopFloorEntityManager<BOMDetailSub>(m => m.ID);
			return View(db.GetAll());
		}


		// GET: BOMDetailSubs/Create
		//public ActionResult Create(int? bomDetailId)
		//{
		//	PopulateDropDown(bomDetailId);
		//	var sub = new EditBOMDetailSub { BOMDetailID = bomDetailId.GetValueOrDefault(0) };
		//	return View(sub);
		//}

		public ActionResult Create(int? bomHeaderId)
		{
			PopulateDropDown(bomHeaderId);
			var sub = new EditBOMDetailSub();
			return View(sub);
		}

		// POST: BOMDetailSubs/Create
		[HttpPost]
		public JsonResult Create(EditBOMDetailSub model)
		{
			try
			{
				if (ModelState.IsValid)
				{
					var dbBOMDetail = new BOMDetailManager();
					var det = dbBOMDetail.GetByID(model.BOMDetailID);
					model.RMCodePrime = det.RMCode;

					model.CreatedBy = db.GetUserIDByUsername(User.Identity.Name);
					model.CreatedOn = DateTime.Now;

					var e = db.Map(model, new BOMDetailSub());

					db.Create(e);

					return Json(e, JsonRequestBehavior.AllowGet);
				}

				return Json(false, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				log.Error("Error creating record", ex);
				return Json(false, JsonRequestBehavior.AllowGet);
			}
		}

		// GET: BOMDetailSubs/Edit/5
		public ActionResult Edit(int id)
		{
			var model = db.Map(db.QueryAllIncluding(m => m.BOMDetail).Where(m => m.ID == id).FirstOrDefault(), new EditBOMDetailSub());
			PopulateDropDown(model.BOMDetail?.BOMHeaderID);
			return View(model);
		}

		// POST: BOMDetailSubs/Edit/5
		[HttpPost]
		public JsonResult Edit(BOMDetailSub model)
		{
			try
			{
				if (ModelState.IsValid)
				{
					var dbBOMDetail = new BOMDetailManager();
					var det = dbBOMDetail.GetByID(model.BOMDetailID);
					model.RMCodePrime = det.RMCode;

					model.ModifiedBy = db.GetUserIDByUsername(User.Identity.Name);
					model.ModifiedOn = DateTime.Now;

					var e = db.Map(model, new BOMDetailSub());

					db.Save(e);

					return Json(e, JsonRequestBehavior.AllowGet);
				}

				return Json(false, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				log.Error("Error saving record", ex);
				return Json(false, JsonRequestBehavior.AllowGet);
			}
		}

		private void PopulateDropDown(int? bomHeaderId)
		{
			var dbBOMDetail = new BOMDetailManager(bomHeaderId.GetValueOrDefault(0));

			ViewBag.RMCodePrimes = CreateSelectList(dbBOMDetail.GetAll(), m => m.ID, o => $"{o.RMCode}|{o.RMName}");
		}


		public JsonResult IsValidRMCodeSub(BOMDetailSub model)
		{
			// check if exists
			var dbSAPITM = new SAP.Managers.SAPRawMaterialsManager();
			if (!dbSAPITM.QueryAll().Any(m => m.ItemCode == model.RMCodeSubs))
			{
				return Json("This RMCode does not exist in the database.", JsonRequestBehavior.AllowGet);
			}

			// check for duplicates
			if (db.QueryAll().Any(m => m.BOMDetailID == model.BOMDetailID && m.RMCodeSubs == model.RMCodeSubs && m.ID != model.ID))
			{
				return Json("Duplicate RMCodeSubs + RMCodePrime combination", JsonRequestBehavior.AllowGet);
			}

            // check if BomDetailSub == BomDetail
            var bomDetail = new BOMDetailManager().QueryAll().Where(m => m.ID == model.BOMDetailID).FirstOrDefault();
            if(model.RMCodeSubs == bomDetail?.RMCode)
            {
                return Json("You cannot substitute this RM with itself.", JsonRequestBehavior.AllowGet);
            }

			return Json(true, JsonRequestBehavior.AllowGet);
		}

		public ActionResult SubstituteRM(int bomHeaderId, SearchParams args)
		{
			var items = db.QueryAllIncluding(m => m.BOMDetail).Where(m => m.BOMDetail.BOMHeaderID == bomHeaderId).ToList();
			var response = this.CreateDTReturnData(items, items.Count, items.Count, args.draw);
			return Content(response, "application/json");
		}

		public ActionResult Delete(int id)
		{
			db.Remove(id);
			return Json(true, JsonRequestBehavior.AllowGet);
		}

	}

}
