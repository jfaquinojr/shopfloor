﻿using ShopFloor.Entities;
using ShopFloor.Managers;
using System;
using System.Web.Mvc;

namespace ShopFloor.Web.Controllers
{
    [Authorize]
    public class QAInspMethodController : BaseController
    {
        QAInspMethodManager db = new QAInspMethodManager();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(QAInspMethod model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedBy = db.GetUserIDByUsername(User.Identity.Name);
                model.CreatedOn = DateTime.Now;
                db.Create(model);
                AddMessageSuccess($"'{model.QAInspMCode}' successfully created.");
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Edit(int? id)
        {
            if(id == null)
            {
                AddMessageError("Invalid ID argument passed.");
                return RedirectToAction("Index");
            }

            var model = db.GetByID(id.Value);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(QAInspMethod model)
        {
            if (ModelState.IsValid)
            {
                model.ModifiedBy = db.GetUserIDByUsername(User.Identity.Name);
                model.ModifiedOn = DateTime.Now;
                db.Save(model);
                AddMessageSuccess($"'{model.QAInspMCode}' successfully saved.");
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public JsonResult Delete(int id)
        {
            db.Remove(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
		
    }
}