﻿using ShopFloor.Entities;
using ShopFloor.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopFloor.Web.Controllers
{
	[Authorize]
    public class RunRateReasonController : BaseController
    {
        private RunRateReasonManager db = new RunRateReasonManager();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(RunRateReason model)
        {
            if (ModelState.IsValid)
            {
                if (!db.IsDuplicate(m => m.ReasonCode == model.ReasonCode))
                {
                    model.CreatedBy = db.GetUserIDByUsername(User.Identity.Name);
                    model.CreatedOn = DateTime.Now;
                    db.Create(model);
                    AddMessageSuccess("RunRate Reason Successfully created.");
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("ReasonCode", "This ReasonCode already exists.");
                }
            }
            return View(model);
        }

		public ActionResult Edit(int id)
        {
            var entity = db.GetByID(id);
            return View(entity);
        }

        [HttpPost]
        public ActionResult Edit(RunRateReason model)
        {
            if (ModelState.IsValid)
            {
                if (!db.IsDuplicate(m => m.ReasonCode == model.ReasonCode && m.ID != model.ID))
                {
                    model.ModifiedBy = db.GetUserIDByUsername(User.Identity.Name);
                    model.ModifiedOn = DateTime.Now;
                    db.Save(model);
                    AddMessageSuccess("RunRate Reason Successfully saved.");
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("ReasonCode", "This ReasonCode already exists.");
                }
            }
            return View(model);
        }

        public JsonResult Delete(int id)
        {
            db.Remove(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
		
    }
}