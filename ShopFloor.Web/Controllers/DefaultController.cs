﻿using ShopFloor.Managers;
using ShopFloor.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopFloor.Web.Controllers
{
    [Authorize]
    public class DefaultController : BaseController
    {
        // GET: Default
        public ActionResult Index()
        {
            // determine user role
            //var mgr = new UsersManager();
            //var user = mgr.GetByUsername(this.User.Identity.Name);
            ////if (user.UserUserClasses.UserClasses.Any(m=>m.UClassCode == "AD"))
            //if(User.IsInRole("AD"))
            //{
            //    HomeAdminModel model = new HomeAdminModel();
            //    model.Registrations = mgr.GetUserRegistrations();
            //    return View("AdminPage", model);
            //}

            return View("AdminPage");
        }

        public ActionResult UnderConstruction()
        {
            return View();
        }

        [Authorize(Roles = "AD,SU")]
        public ActionResult AdminPage()
        {
            return View();
        }

        public ActionResult QALinks()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult About()
        {
            return View();
        }
}
}