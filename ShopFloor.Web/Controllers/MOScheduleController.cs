﻿using ShopFloor.DTO;
using ShopFloor.Entities;
using ShopFloor.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopFloor.Web.Controllers
{
	[Authorize]
    public class MOScheduleController : BaseController
    {
        MOResourceScheduleManager db = new MOResourceScheduleManager();

        public ActionResult Index()
        {
            var list = db.GetActiveMOSchedule();
            return View(list);
        }

        [HttpPost]
        public JsonResult Reschedule(IList<MOReschedule> newPrios)
		{
			// swap priorities:
			var oldPrios = db.GetActiveMOSchedule().ToArray();
			for (var i = 0; i < newPrios.Count; i++)
			{
				newPrios[i].MOPriority = oldPrios[i]?.MOPriority ?? newPrios[i].MOPriority;
			}

			// save new priorities to database
			var dbMO = new MOHeaderManager();
			int userId = db.GetUserIDByUsername(User.Identity.Name);
			MOHeader found;
			foreach (var mo in newPrios)
			{
				found = dbMO.GetByMONumber(mo.MONumber);
				if (found != null)
				{
					found.MOPriority = mo.MOPriority;
					found.ModifiedBy = userId;
					found.ModifiedOn = DateTime.Now;
					dbMO.Save(found);

					mo.MOHeaderID = found.ID; //<--- we will pass this value to ProcessReschedule below.
				}
			}

			rescheduleNow(newPrios);

			return Json(true, JsonRequestBehavior.AllowGet);
		}

		private void rescheduleNow(IList<MOReschedule> list)
		{
			// now reschedule
			var dbRSC = new MODetailRSCManager();
			dbRSC.ProcessReschedule(list.Select(m => m.MOHeaderID).ToArray(), User.Identity.Name);
		}

		[AllowAnonymous] /* client doesnt care about security so shush dont judge me */
		public JsonResult RescheduleNow()
		{
			var list = db.GetActiveMOSchedule().ToArray();
			rescheduleNow(list);

			return Json(true, JsonRequestBehavior.AllowGet);
		}
	}
}