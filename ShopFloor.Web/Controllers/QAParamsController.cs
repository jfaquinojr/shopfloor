﻿using ShopFloor.Entities;
using ShopFloor.Managers;
using System;
using System.Web.Mvc;

namespace ShopFloor.Web.Controllers
{
    [Authorize]
    public class QAParamsController : BaseController
    {
        private QAParamManager db = new QAParamManager();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(QAParam model)
        {
            if (ModelState.IsValid)
            {
                if (!db.IsDuplicate(m => m.QAParaCode == model.QAParaCode))
                {
                    var userid = db.GetUserIDByUsername(User.Identity.Name);
                    model.CreatedBy = userid;
                    model.CreatedOn = DateTime.Now;
                    int id = db.Create(model);
                    AddMessageSuccess("Record created.");
                    return RedirectToAction("Edit", new { id = id });
                }
                ModelState.AddModelError("QAParaCode", "Duplicat QA Param Code.");
            }
            return View(model);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                AddMessageError("Invalid ID argument passed.");
                return RedirectToAction("Index");
            }

            var model = db.GetByID(id.Value);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(QAParam model)
        {
            if (ModelState.IsValid)
            {
                if (!db.IsDuplicate(m => m.QAParaCode == model.QAParaCode && m.ID != model.ID))
                {
                    var userid = db.GetUserIDByUsername(User.Identity.Name);
                    model.ModifiedBy = userid;
                    model.ModifiedOn = DateTime.Now;
                    db.Save(model);
                    AddMessageSuccess("Record saved.");
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("QAParaCode", "Duplicat QA Param Code.");
            }
            return View(model);
        }

        public JsonResult Delete(int id)
        {
            db.Remove(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
		
    }
}