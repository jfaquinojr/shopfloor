﻿using ShopFloor.Entities;
using ShopFloor.Managers;
using System;
using System.Web.Mvc;

namespace ShopFloor.Web.Controllers
{
    [Authorize]
    public class QACriteriaController : BaseController
    {
        QACriteriaManager db = new QACriteriaManager();

        public ActionResult Index()
        {
            return View();
        }

		public ActionResult Create()
        {
            PopulateDropdown();
            return View();
        }

        [HttpPost]
        public ActionResult Create(QACriterion model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedBy = db.GetUserIDByUsername(User.Identity.Name);
                model.CreatedOn = DateTime.Now;
                db.Create(model);
                AddMessageSuccess($"Criteria '{model.QACritCode}' successfully created.");
                return RedirectToAction("Index");
            }
            PopulateDropdown();
            return View();
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                AddMessageError("Invalid ID argument passed.");
                return RedirectToAction("Index");
            }

            PopulateDropdown();

            var model = db.GetByID(id.Value);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(QACriterion model)
        {
            if (ModelState.IsValid)
            {
                model.ModifiedBy = db.GetUserIDByUsername(User.Identity.Name);
                model.ModifiedOn = DateTime.Now;
                db.Save(model);
                AddMessageSuccess($"Criteria '{model.QACritCode}' successfully saved.");
                return RedirectToAction("Index");
            }
            PopulateDropdown();
            return View();
        }

        public JsonResult Delete(int id)
        {
            db.Remove(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        private void PopulateDropdown()
        {
            ViewBag.InspectionMethods = base.CreateSelectList(new QAInspMethodManager().GetAll(), m => m.ID, m => $"{m.QAInspMCode}-{m.QAInspMDesc}");
        }
		
    }
}