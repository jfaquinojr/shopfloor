﻿using ShopFloor.Entities;
using ShopFloor.Managers;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace ShopFloor.Web.Models
{
}

namespace ShopFloor.Web.Controllers
{
    [Authorize]
    public class ActivityTypesController : BaseController
    {
		private ShopFloorEntityManager<ActivityType> db = new ShopFloorEntityManager<ActivityType>(m => m.ID);

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            var model = new ActivityType { Active = true };
            return View(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ActivityType model)
        {
            if (ModelState.IsValid)
            {
                if (HasDuplicateCode(model))
                {
                    ModelState.AddModelError("ActivityCode", "Activity Code already exists.");
                }
                else
                {
                    db.Create(model);
                    this.AddMessageSuccess("Record created.");
                    return RedirectToAction("Index");
                }

            }

            return View(model);
        }

        public ActionResult Edit(int? id)

        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ActivityType activityType = db.GetByID(id.Value);
            if (activityType == null)
            {
                return HttpNotFound();
            }
            return View(activityType);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ActivityType model)
        {
            if (ModelState.IsValid)
            {
                if (HasDuplicateCode(model))
                {
                    ModelState.AddModelError("ActivityCode", "Activity Code already exists.");
                }
                else
                {
                    db.Save(model);
                    this.AddMessageSuccess("Record saved.");
                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }

        public JsonResult Delete(int id)
        {
            db.Remove(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        public bool HasDuplicateCode(ActivityType model)
        {
            return db.QueryAll().Any(m => m.ActivityCode == model.ActivityCode && m.ID != model.ID);
        }
    }
}
