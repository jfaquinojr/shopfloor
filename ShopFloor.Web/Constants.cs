﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopFloor.Web
{
    public static class Constants
    {
        public const string CONST_MESSAGE_SUCCESS = "MessageSuccess";
        public const string CONST_MESSAGE_ERROR = "MessageError";
        public const string CONST_MESSAGE_WARNING = "MessageWarning";
        public const string CONST_MESSAGE_INFO = "MessageInfo";

        public const string CONST_FORMAT_DECIMAL_12 = "##,###,###.000000000000";
    }
}