﻿
$('#form-group-new').hide();

$('input:enabled:not([readonly]).txt-effectivity-date').datetimepicker({
	timepicker: false,
	format: 'm/d/Y',
	formatDate: 'm/d/Y'
});

urlBOMCopy = urlBOMCopy || '/BOM/copy';

$('#btn-copy-bom').click(function () {

	var frm = $('#FormBOM');
	frm.validate();
	if (frm.valid()) {
		frm.attr('action', urlBOMCopy);
		frm.submit();
	}

})
