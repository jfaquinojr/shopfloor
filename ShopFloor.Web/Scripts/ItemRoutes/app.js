﻿var app = angular.module('ItemRouteApp', []);


app.controller('RouteController', function ($http) {

    ctrl = this;

    this.ItemCode = '';
    
    this.listRoutes = [];
    this.gridRoutes = [];

    this.init = function (itemcode) {
        ctrl.ItemCode = itemcode;

        // populate routes dropdown
        $http({ 'method': 'GET', 'url': '/itemroutes/getroutes' })
        .success(function (data) {
            ctrl.listRoutes = data;
        });

        // poupulate routes grid
        $http({ 'method': 'GET', 'url': '/itemroutes/GetItemRoutes?itemcode=' + ctrl.ItemCode })
        .success(function (data) {
            ctrl.gridRoutes = data;
        });
    };

    this.AddRoute = function (route) {

        alert(JSON.stringify(route));

        // add routes
        var jsondata = { ItemCode: ctrl.ItemCode, RouteID: route.ID };
        $http({ 'method': 'POST', 'url': '/itemroutes/additemroute', 'data': JSON.stringify(jsondata) })
        .success(function (data) {
            ctrl.gridRoutes.push(data);
            toastr['success']('Item Route created successfully.');
        });

    };

    this.RemoveRoute = function (route) {
        if (!confirm('are you sure you want to remove this record?')) return;

        $http({ 'method': 'GET', 'url': '/itemroutes/deleteitemroute/' + route.ID })
        .success(function (data) {
            ctrl.gridRoutes = _.reject(ctrl.gridRoutes, function (m) { return (m.ID == route.ID); })
            toastr['success']('Item Route deleted from the database.');
        });
    };

});