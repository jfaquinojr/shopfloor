﻿
var app = angular.module('datacapture', []);

app.controller('FGGController', ['$http', '$compile', '$scope', '$interval', function ($http, $compile, $scope, $interval) {

	this.MOHeaderID = 0;

	this.selectedFGGIDs = []; // FGGs that are checked for Processing

	this.Resources = [];  // displayed in Resources dropdown
	this.SelectedResourceModel = undefined; // placeholder for the selected resource via dropdown

	this.status = 0; //0=not started, 1=started, 2=stopped, 3=closed;
	this.timeLoaded = moment().format('MM/DD/YYYY hh:mm A');
	this.timeStart = 'start';
	this.timeStop = '--/--/---- --:-- --';
	this.timeElapsed = '00:00:00';
	this.ScheduleStartDate = '';
	this.ScheduleEndDate = '';
	this.ScheduleElapsedTime = '';

	// ------ lookup tables start -----------
	this.listSubResources = [];
	this.listActivities = [];
	this.listAddlRawMaterials = [];
	this.listAddlReasons = [];
	this.listRunRateReasons = [];
	this.listQACriteria = [];

	this.selectedSubResource = {}
	this.selectedActivity = {}
	this.selectedAddlRawMaterial = {};
	this.selectedAddlReason = {};
	this.selectedRunRateReason = {};
	this.selectedQACriteria = {};
	this.rejectQty = 0;
	// ------ lookup tables stop -----------

	this.totalRejectQty = 0;
	this.gridQACriteria = [];
	this.gridQAParams = [];

	this.ResourceCode = '';
	this.SubResourceCode = '';
	this.SubResourceID = 0;
	this.shopFloorIDs = [];


	this.good = 0;
	this.runRateReasonId = null;
	this.noOfOuts = 0;
	this.addlRmCode = '';
	this.addlRmReasonId = 0;
	this.addlRmQty = 0;

	this.AddlRMCode = '';
	this.AddlRMReasonID = 0;
	this.AddlRMQty = 0;


	var ctrl = this;

	this.setMOHeaderID = function (id, dataCaptureId) {

		//alert(id);
		ctrl.MOHeaderID = id;
		if (id > 0) {

			// get FGG list
			if (!dataCaptureId) {
				$http({ method: 'GET', url: url_GetFGGJSONList + ctrl.MOHeaderID })
				.success(function (data) {
					ctrl.Items = data;
				});
			}


			// get addl RM list
			$http({ method: 'GET', url: url_GetAddlRM + ctrl.MOHeaderID })
			.success(function (data) {
				if (data) ctrl.listAddlRawMaterials = data;
			});

		}

		if (dataCaptureId > 0) {

			ctrl.shopFloorIDs.push(dataCaptureId);
			ctrl.selectedFGGIDs.push(fggID);

			// populate QA Parameters grid
			$http({ url: url_GetAllQAParamResourcesByResourceCode + dataCaptureId + '?resourcecode=' + ctrl.ResourceCode, method: 'GET' })
			.success(function (params) {
				ctrl.gridQAParams = params;
			});



			// get shopfloor data
			$http({ method: 'GET', url: url_GetDataCapture + dataCaptureId })
			.success(function (data) {
				if (data.Status == 'Started') {
					ctrl.status = 1;
				}
				else if (data.Status == 'Stopped') {
					ctrl.status = 2;
				}
				else if (data.Status == 'Completed') {
					ctrl.status = 3;
				}
				else {
					ctrl.status = 1;
				}

				ctrl.timeStart = data.StartDate;
				ctrl.timeStop = data.EndDate;
				ctrl.timeElapsed = data.ElapsedTime;
				ctrl.selectedActivity = data.ActivityType;
				ctrl.good = data.Good;
				ctrl.noOfOuts = data.NoOfOuts;
				ctrl.SubResourceID = data.SubResourceID;
				ctrl.runRateReasonId = data.RunRateReasonID;
				ctrl.AddlRMCode = data.ShopFloorAddl.RMItemCode;
				ctrl.AddlRMQty = data.ShopFloorAddl.AddlQty;
				ctrl.AddlRMReasonID = data.ShopFloorAddl.AdditionalReasonID;
				ctrl.ScheduleStartDate = data.ScheduleStartDate;
				ctrl.ScheduleEndDate = data.ScheduleEndDate;
				ctrl.ScheduleElapsedTime = data.ScheduleElapsedTime;

			});

			// get rejects
			$http({ url: url_GetRejectEntries + ctrl.shopFloorIDs[0], method: 'GET' })
			.success(function (data) {
				ctrl.gridQACriteria = data;
				ctrl.totalRejectQty = 0;
				_.each(ctrl.gridQACriteria, function (reject) {
					ctrl.totalRejectQty += reject.Reject;
				});
			});
		}

		// get activity list
		$http({ method: 'GET', url: url_GetActiveActivities })
		.success(function (data) {
			if (data) ctrl.listActivities = data;
		});

		// get addl reason list
		$http({ method: 'GET', url: url_GetAddlReasons })
		.success(function (data) {
			if (data) ctrl.listAddlReasons = data;
		});

		// get run rate reason list
		$http({ method: 'GET', url: url_GetRunRateReasons })
		.success(function (data) {
			if (data) ctrl.listRunRateReasons = data;
		});

		// get qacriteria
		$http({ method: 'GET', url: url_GetQACriteria })
		.success(function (data) {
			if (data) ctrl.listQACriteria = data;
		});

		// get subresources list
		ctrl.ResourceCode = rscResourceCode;
		$http({ url: url_GetAllSubresourcesByCode + ctrl.ResourceCode, method: 'GET' })
		.success(function (data) {
			ctrl.listSubResources = data;
		});

		if (ctrl.SubResourceCode.length > 0) {
			ctrl.UpdateSubResource({ SubResourceID: ctrl.SubResourceID, Subresource: { ResourceCode: ctrl.SubResourceCode } });
		}

	};


	this.stopResource = function ($event, resource) {

		$http({
			method: 'POST',
			url: url_StopResource,
			data: JSON.stringify(
				{
					shopFloorIDs: ctrl.shopFloorIDs,
				})
		})
		.success(function (data) {

			ctrl.timeStop = moment(data.EndDate).format('MM/DD/YYYY hh:mm A'); //drop the seconds
			ctrl.timeElapsed = data.ElapsedTime;

			ctrl.status = 2;
			toastr['success']('Resource stopped.');
		});
	}

	this.closeResource = function ($event, resource) {

		var err = [];
		if (!this.good) err.push('Good');
		if (!this.runRateReasonId) err.push('Run Rate Reason');

		var index = 0;
		_.each(this.gridQAParams, function (par) {
			if (!par.QAParaValueNum) err.push('Parameter entry value for ' + par.QAParam.QAParaCode);
		})

		if (err.length > 0) {
			
			var msg = 'The following fields are required: \n\n';
			_.each(err, function (e) {
				msg += e + '\n';
			});

			alert(msg);
			return false;
		}

		$http({
			method: 'POST',
			url: url_CloseResource,
			data: JSON.stringify(
				{
					shopFloorIDs: ctrl.shopFloorIDs,
				})
		})
		.success(function (data) {
			ctrl.status = 3;
			toastr['success']('Resource closed.');
		});
	}

	this.timerStart = function () {
		ctrl.timeStart = ctrl.timerTick.toLocaleTimeString();
	};

	this.timerTick = function () {
		if (ctrl.status == 0) { // Before Start
			ctrl.timeStart = moment().format('MM/DD/YYYY hh:mm:ss A');
		}

		else if (ctrl.status == 1) { // Started

		    var ss = ctrl.timeElapsed.split(":");
		    if (ss.length < 3) {
		        ss = [0, 0, 0];
		    }
		    var dt = new Date();
		    dt.setHours(ss[0]);
		    dt.setMinutes(ss[1]);
		    dt.setSeconds(ss[2]);
		    var dt2 = new Date(dt.valueOf() + 1000);
		    var ts = dt2.toTimeString().split(" ")[0];
		    ctrl.timeElapsed = ts;
                                
			ctrl.timeStop = ''; // moment().format('MM/DD/YYYY hh:mm:ss A');
		}

		else if (ctrl.status == 2) { // Stopped
			// do nothing. timeElapsed should be updated using ajax call to be more precise
		}
	}

	this.timerStop = function () {

	}

	// ticks every second
	$interval(this.timerTick, 1000);

	this.addRejectEntry = function ($event, qa, qty) {

		// search list if QACriteria you are trying to add already exists
		if (_.find(this.gridQACriteria, function (m) { if (m.QACriterion.QACritCode == qa.QACritCode) return true; })) {
			alert('The selected QA Criteria already exists in the list.');
			return;
		}

		$http({
			method: 'POST', url: url_AddRejectEntry, data:
				{
					shopFloorIDs: ctrl.shopFloorIDs,
					qaCriteriaId: qa.ID,
					qty: qty,
				}
		})
		.success(function (data) {

			ctrl.gridQACriteria.push(data);
			ctrl.totalRejectQty += qty;
			$scope.selectedQACriteria = undefined;
			$scope.rejectQty = 0;

			toastr['success']('Reject Entry created.');
		});

	}

	this.saveRejectEntry = function ($event, qa, oldValue) {

		var jsonData = {
			shopFloorIDs: ctrl.shopFloorIDs,
			model: qa,
		};
		$http({ method: 'POST', url: url_SaveRejectEntry, data: jsonData })
		.success(function (data) {

			ctrl.totalRejectQty -= oldValue;
			ctrl.totalRejectQty += qa.Reject;

			toastr['success']('Reject Entry saved.');
		});
	}

	this.deleteRejectEntry = function ($event, qa) {


		if (!confirm('are you sure you want to delete this item')) return;

		var jsonData = {
			shopFloorIDs: ctrl.shopFloorIDs,
			rejectId: qa.ID,
		}
		$http({ method: 'POST', url: url_DeleteRejectEntry, data: jsonData })
		.success(function (data) {
			ctrl.gridQACriteria = _.reject(ctrl.gridQACriteria, function (what) { if (what.ID == qa.ID) { return true; }});
			ctrl.totalRejectQty -= qa.Reject;
			toastr['success']('Reject Entry deleted.');
		});
	}


	this.saveGeneralFields = function (model) {

		if (!model || !ctrl.shopFloorIDs) return false;


		var postdata = {};
		postdata.ActivityTypeID = ctrl.selectedActivity.ID;
		if (model.runRateReasonId != undefined) postdata.RunRateReasonID = model.runRateReasonId;
		if (model.good != undefined) postdata.Good = model.good;
		if (model.noOfOuts != undefined) postdata.NoOfOuts = model.noOfOuts;

		$http({
			method: 'POST',
			url: url_UpdateGeneralForm,
			data: JSON.stringify({
				shopFloorIDs: this.shopFloorIDs,
				model: postdata
			})
		})
		.success(function (data) {
			toastr['success']('Record updated.');
		});

		return true;
	}

	this.saveQAParam = function (param) {

		var jsonData = {
			shopFloorIDs: ctrl.shopFloorIDs,
			model: { QAParamID: param.QAParamID, QAParamActual: param.QAParaValueNum },
		}

		$http({ method: 'POST', url: url_SaveQAParam, data: jsonData })
		.success(function (data) {
			toastr['success']('QA Param Entry saved.');
		});
	}

	this.UpdateSubResource = function (res) {
		if (res) {
			ctrl.SubResourceCode = res.Subresource.ResourceCode;
			ctrl.SubResourceID = res.SubResourceID;
		}
		else {
			ctrl.SubResourceCode = '';
			ctrl.SubResourceID = 0;
		}

		if (ctrl.shopFloorIDs.length == 0) return;

		var jsonData = {
			shopFloorIDs: ctrl.shopFloorIDs,
			model: { SubResourceID: ctrl.SubResourceID },
		}
		$http({ method: 'POST', url: url_AssignSubResource, data: JSON.stringify(jsonData) })
		.success(function () {
			toastr['success']('SubResource saved.');
		});
	}

	this.AddRM = function (addlrm) {
		var jsonData = {
			shopFloorIDs: ctrl.shopFloorIDs,
			model: { AddlQty: addlrm.AddlRMQty, RMItemCode: addlrm.AddlRMCode, AdditionalReasonID: addlrm.AddlRMReasonID, shopFloorIDs: ctrl.shopFloorIDs },
		}

		$http({ method: 'POST', url: url_AddAddlRM, data: jsonData })
		.success(function (data) {
			toastr['success']('Addl RM Created.');
		});
	}

	// when user ticks on Process Tag checkbox
	this.tagProcess = function ($event, $scope, fgg) {

		fgg.checked = !fgg.checked;

		var fggID = fgg.MODetailFGGID;

		if (fgg.checked == false) {
			// remove from list
			ctrl.selectedFGGIDs = _.without(ctrl.selectedFGGIDs, fggID);
		}
		else {
			// add to list only if it doesnt exist
			if (_.indexOf(this.selectedFGGIDs, fggID) < 0) {
				ctrl.selectedFGGIDs.push(fggID);
			}
		}

		$http({ method: 'POST', url: url_getresourcesforfggs, data: JSON.stringify(ctrl.selectedFGGIDs) })
		.success(function (data) {
			ctrl.Resources = data;
		});

	};

	// when user clicks the big Start button
	this.startResource = function ($event, resource) {

		if (!confirm("Are you sure you want to start? Once you do, you won't be able to change the activity and resource.")) return false;

		$http({
			method: 'POST',
			url: url_StartResource + resource.ResourceCode,
			data: JSON.stringify(
				{
					fggIDs: ctrl.selectedFGGIDs,
					activity: ctrl.selectedActivity
				})
		})
		.success(function (data) {
			var d = _.first(data);
			ctrl.status = 1;
			ctrl.timeStart = moment(d.StartDate).format('MM/DD/YYYY hh:mm A'); //drop the seconds
			ctrl.shopFloorIDs = _.pluck(data, 'ID');


			var shopFloorId = _.first(ctrl.shopFloorIDs);
			// populate QA Parameters grid
			$http({ url: url_GetAllQAParamResourcesByResourceCode + shopFloorId + '?resourcecode=' + ctrl.ResourceCode, method: 'GET' })
			.success(function (params) {
				ctrl.gridQAParams = params;
			});

			if (ctrl.SubResourceCode.length > 0) {
				ctrl.UpdateSubResource({ SubResourceID: ctrl.SubResourceID, Subresource: { ResourceCode: ctrl.SubResourceCode } });
			}

			toastr['success']('Resource started. WIP ID(s) created: ' + ctrl.shopFloorIDs.toString());
		});
	}

	this.setActivity = function (activity) {
		ctrl.selectedActivity = activity;
	}

	getPreviousResource = function (resourceCode, resources) {
		var prev = undefined;
		var retval = undefined;
		_.each(resources, function (res) {
			if (res.ResourceCode == resourceCode) {
				retval = prev;
			}
			prev = res;
		});

		return retval;
	}

	this.setResourceContinued = function(res){
		// get scheduled dates and times
		var urlRsc = url_GetMODetailRSC + '?resourceCode=' + res.ResourceCode + '&fggId=' + ctrl.selectedFGGIDs[0];
		console.log('Get Scheduled Dates and times: ' + urlRsc);
		$http({ url: urlRsc, method: 'GET' })
		.success(function (data) {
			ctrl.ScheduleStartDate = data.ScheduleStartDate;
			ctrl.ScheduleEndDate = data.ScheduleEndDate;
			ctrl.ScheduleElapsedTime = data.ScheduleElapsedTime;
		});

		ctrl.ResourceCode = res.ResourceCode;

		// get subresources list
		$http({ url: url_GetAllSubresourcesByCode + ctrl.ResourceCode, method: 'GET' })
		.success(function (data) {
			ctrl.listSubResources = data;
		});
	}

	this.setResource = function (res) {
		if (!res) {
			ctrl.ResourceCode = '';
			ctrl.listSubResources = [];
		}
		else {

			// client requirement:
			// User must not be able to select resource if the prerequisite resource is not yet been started.
			// Validate previous resource : (If previous resource s OnStreamTag = Null, do not accept selected resource).
			var prev = getPreviousResource(res.ResourceCode, ctrl.Resources);
			if (prev) {

				// Get OnStreamTag of resource
				$http({ url: url_IsOnStreamIsNull + '?resourceCode=' + prev.ResourceCode + '&fggID=' + ctrl.selectedFGGIDs[0], method: 'GET' })
				.success(function (isNull) {
					if (isNull) {
						ctrl.SelectedResourceModel = null;
						alert('Previous resource has not been started.');
					}
					else {

						ctrl.setResourceContinued(res);

					}
					
				});
			}
			else {
				ctrl.setResourceContinued(res);
			}
		}
	}

}]);

app.controller('ShopFloorController', ['$http', function ($http) {

}]);

app.controller('RejectsController', ['$http', function ($http) {
	this.FGGID = 0;

}]);



app.filter("jsDate", function () {
	return function (x) {
		if (x)
			return new Date(parseInt(x.substr(6)));
		return '';
	};
});
