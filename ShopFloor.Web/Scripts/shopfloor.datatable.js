﻿
/*

author: jfaquinojr@gmail.com
generic search table. example usage below:

<link href="~/Content/dataTables.bootstrap.css" rel="stylesheet" />
<script src="~/Scripts/jquery.dataTables.min.js"></script>
<script src="~/Scripts/dataTables.bootstrap.js"></script>
<script src="~/Scripts/shopfloor.datatable.js"></script>
<script>

    var f1 = function (id, searchtype) {
        alert(id + ' ' + searchtype);
    }

    $(function () {


        var tableRoute = new SearchTable('route', '.table-search', '.txt-table-search', '.btn-table-search', f1, undefined, undefined);
        tableRoute.addDataTableColumn('ItemCode', true, true);
        tableRoute.addDataTableColumn('RouteCode', true, true);
        tableRoute.addDataTableColumn('RouteDesc', true, true);
        tableRoute.showActionButtons = false;
        tableRoute.showSelectButton = true;
        tableRoute.initialize();

    });

</script>

*/

var SearchTable = function (_searchType, _dataTableSelector, _searchInputSelector, _searchButtonSelector, clickSelectRow, clickEditRow, clickDeleteRow) {

    var $that = this;

    this.dataTableSelector = _dataTableSelector;
    this.searchInputSelector = _searchInputSelector;
    this.searchButtonSelector = _searchButtonSelector;
    this.cols = [];
    this.searchType = _searchType;
    this.btnDeleteRow_Click = clickDeleteRow;
    this.btnEditRow_Click = clickEditRow;
    this.btnSelectRow_Click = clickSelectRow;
    this.showActionButtons = true;
    this.ShowDeleteButton = false;
    this.showSelectButton = false;
    this.showButtonText = true;
    this.table = {};
    this.PageLength = 50;
    this.ServerSide = true;
    this.IDField = "ID";
    this.AjaxURL = "/search/search?table=" + this.searchType;
    this.AjaxType = "POST";
    this.EmptyTableMessage = "No matching records found.";
    this.ZeroRecords = "No matching records found.";
    this.dom = 'lfrtip';

    this.eventAjaxStart = undefined;
    this.eventAjaxStop = undefined;


    this.btnEditID = 'btn-edit-row-' + new Date().getTime();
    this.btnSelectID = 'btn-select-row-' + new Date().getTime();
    this.btnDeleteID = 'btn-delete-row-' + new Date().getTime();

    function getFirstPropertyValue(data) {
        for (var property in data) {
            if (data.hasOwnProperty(property)) {
                return data[property];
            }
        }
    }

    this.RenderSelectCallBack = function (data, type, row, meta) {
        return "<button id='" + $that.btnSelectID + "' class='btn btn-primary btn-xs' data-table='" + $that.searchType + "' data-id='" + row[$that.IDField] + "' data-obj='" +  JSON.stringify(row).replace(/'/g, "\\'") + "'><span class='glyphicon glyphicon-check'></span>" + ($that.showButtonText ? '&nbsp;select' : '') + "</button>";
    }
    this.RenderActionCallBack = function (data, type, row, meta) {
        return "<button id='" + $that.btnEditID + "' class='btn btn-primary btn-xs' data-table='" + $that.searchType + "' data-id='" + row[$that.IDField] + "'><span class='glyphicon glyphicon-pencil'></span>" + ($that.showButtonText ? '&nbsp;edit' : '') + "</button>&nbsp;<button id='" + $that.btnDeleteID + "' class='btn btn-danger btn-xs' data-id='" + row[$that.IDField] + "' data-table='" + $that.searchType + "'><span class='glyphicon glyphicon-trash'></span>" + ($that.showButtonText ? '&nbsp;remove' : '') + "</button>";
    }
    this.RenderDeleteCallBack = function (data, type, row, meta) {
        return "<button id='" + $that.btnDeleteID + "' class='btn btn-danger btn-xs pull-right' data-table='" + $that.searchType + "' data-id='" + row[$that.IDField] + "'><span class='glyphicon glyphicon-trash'></span>" + ($that.showButtonText ? '&nbsp;remove' : '') + "</button>";
    }

    this.SearchTextKeyUp = function () {
        $that.table.search(this.value).draw();
    }

    this._Initialize = function () {
        var table = $(this.dataTableSelector)
            //.on('processing.dt', function (e, settings, processing) {
            //    if (processing) {
            //        $('.dataTables_processing').html('<i class="fa fa-refresh fa-spin"></i>&nbsp;Loading...');
            //    }
            //})
            .on('draw.dt', function () {
                //alert('TAble redrawn');
            })
            .on('preXhr.dt', function (e, settings, data) {

                if (this.eventAjaxStart) {
                    alert('test!');
                    this.eventAjaxStart(e, settings, data);
                }
            })
            .on('xhr.dt', function (e, settings, json, xhr) {

                if (this.eventAjaxStop) {
                    this.eventAjaxStop(e, settings);
                }
                else {
                    //if (restoreFullPageAjaxLoader) restoreFullPageAjaxLoader();
                    $(this).removeClass('loading ajax-loader');
                }
            })
            .DataTable({
            "serverSide": this.ServerSide,
            "processing": true,
            "ajax": { "url": this.AjaxURL, "type": this.AjaxType },
            "pageLength": this.PageLength,
            "order": [[0, "asc"]],
            "columns": this.cols,
            "autoWidth": false,
            "language": {
                "emptyTable": this.EmptyTableMessage,
                "zeroRecords": this.ZeroRecords
            },
            "dom" : this.dom,
            })
        ;

        return table;
    }



    $(this.dataTableSelector).on('click', '#' + $that.btnSelectID, function () {
        if ($that.btnSelectRow_Click) {
            $that.btnSelectRow_Click($(this).data('id'), $(this).data('obj'));
            return false;
        }
    });

    $(this.dataTableSelector).on('click', '#' + $that.btnEditID, function () {
        if ($that.btnEditRow_Click) {
            $that.btnEditRow_Click($(this).data('id'));
            return false;
        }
    });

    $(this.dataTableSelector).on('click', '#' + $that.btnDeleteID, function () {
        if ($that.btnDeleteRow_Click) {
            if (confirm('are you sure you want to delete this record?')) {
                $that.btnDeleteRow_Click($(this).data('id'));
            }
            return false;
        }
    });
}

SearchTable.prototype.addDataTableColumn = function (name, isOrderable, isSearchable, isVisible) {
    if (typeof (isVisible) == undefined) isVisible = true;
    this.cols.push({ "targets": [this.cols.length], data: name, "orderable": isOrderable, "searchable": isSearchable, "visible": isVisible });
}

SearchTable.prototype.addDataTableColumnCustom = function (render, isOrderable, isSearchable, isVisible) {
    if (typeof (isVisible) == undefined) isVisible = true;
    this.cols.push({
        "targets": [this.cols.length], data: null, "orderable": isOrderable, "searchable": isSearchable, "visible": isVisible,
        "render": render
    });
}


SearchTable.prototype.clearDataTableColumns = function ()
{
    var i = this.cols.length;
    this.cols.length = 0;
    //console.log(i + ' data columns cleard.');
}


SearchTable.prototype.initialize = function () {

    $that = this;

    //alert($that.btnSelectID + ' | ' + this.btnSelectID);
    // add select button
    if (this.showActionButtons) {
        this.cols.push({
            "targets": [$that.cols.length],
            "data": null,
            "searchable": false,
            "orderable": false,
            "render": $that.RenderActionCallBack
        });
    }

    // add action column
    if (this.showSelectButton) {
        this.cols.push({
            "targets": [$that.cols.length],
            "data": null,
            "searchable": false,
            "orderable": false,
            "render": $that.RenderSelectCallBack
        });
        //alert('select: ' + JSON.stringify(this.cols[this.cols.length - 1]));
    }


    // add delete button
    if (this.ShowDeleteButton) {
        this.cols.push({
            "targets": [$that.cols.length],
            "data": null,
            "searchable": false,
            "orderable": false,
            "render": $that.RenderDeleteCallBack
        });
        //alert('select: ' + JSON.stringify(this.cols[this.cols.length - 1]));
    }

    // auto append dynamic columns if none are already defined
    var count = $($that.dataTableSelector + ' thead th').length;
    if (!count) {

        //console.log("creating header columns");
        this.cols.forEach(function (arg) {
            if (arg.data) {
                //console.log("column name: " + arg.data);
                $($that.dataTableSelector + ' thead').append('<th>' + arg.data + '</th>');
            }
            else {
                //console.log("column name: (null)");
                $($that.dataTableSelector + ' thead').append('<th></th>');
            }
        });
        //console.log("done creating " + this.cols.length + " header columns");

    }


    // instantiate DataTable
    $that.table = $that._Initialize();

    // the following events filter the datatable upon keyup or search button click events
    //console.log('initializing keyup event of {0} for table {1}'.format($that.searchInputSelector, $that.AjaxURL));
    $($that.searchInputSelector).keyup($that.SearchTextKeyUp);

    $($that.searchButtonSelector).click(function () {
        //do nothing
    });


    //console.log("end initialize");
}

SearchTable.prototype.filter = function (args) {

    /* ============================
    // filter() code example

    // build search arguments
    var args = [];
    args.push({
        "colindex": 3,
        "value": selectedRoute.text()
    });

    args.push({
        "colindex": 4,
        "value": selectedOper.text()
    });

    theTable.filter(args);
    ============================ */

    var that = this;
    $.each(args, function (index, item) {
        that.table
            .columns(item.colindex)
            .search(item.value);
    });
    that.table.draw();
    
}


SearchTable.prototype.clearResults = function () {
    this.table
        .columns(0)
        .search('this is a nonsense value that is impossible to have. i use this just so the table results can be cleared')
        .draw();
}


SearchTable.prototype.reload = function(){
    var api = this.table;
    api.ajax.reload();
}

function isValidDate(d) {
    if (Object.prototype.toString.call(d) !== "[object Date]")
        return false;
    return !isNaN(d.getTime());
}

