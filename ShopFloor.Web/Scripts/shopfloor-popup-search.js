﻿$(document).on('click', '.btn-search-selected-row', function () {
    var code = $(this).data('id');
    var desc = $(this).data('desc');

    //save code to text field
    $('#SearchFGItemCode').val(code);
    $('#FGItemName').val(desc);
    $('#modal-search-fgitem').modal('hide');
});

function filterTable(s) {
    $("#search-item-table").dataTable().fnFilter(s);
}

var searchButton_click;

$(document).on('click', '.btn-search-selected-row', function () {
    var code = $(this).data('id');
    var desc = $(this).data('desc');

    if (searchButton_click) {
        searchButton_click(code, desc);
    }

    $('#modal-search-item').modal('hide');
        
});


$(function () {

    $('.btn-search-itemcode').click(function () {
        $('#modal-search-item').modal('show');
    });

    var table = $("#search-item-table").dataTable({
        "serverSide": true,
        "processing": true,
        "ajax": { "url": "/search/search?table=item", "type": "POST" },
        "pageLength": 10,
        //"pagingType": "simple",
        "order": [[1, "asc"]],
        "columnDefs": [{
            "targets": [0],
            "data": null,
            "searchable": false,
            "orderable": false,
            "render": function (data, type, full, meta) {
                return "<button class='btn btn-primary btn-xs btn-search-selected-row' data-id='" + data.ItemCode + "' data-desc='" + data.ItemName + "'>Select</button>";
            }
        },
            { "targets": [1], data: 'ItemCode' },
            { "targets": [2], data: 'ItemName' }
        ]
    });

    $('#txt-item-search').keyup(function () {
        filterTable(this.value);
    })

    $('#btn-item-search').click(function () {
        filterTable($('#txt-item-search').val());
    });

});



