﻿// ********************************************
// client insists on using cookieless sessions
// for multiple login per browser tabs. silly.
// but, if we dont, we dont get our peanuts ;)
// I transferred the code to Edit if you're
// looking for it. jfaquinojr@gmail.com
// ********************************************

//var app = angular.module('ResourcesApp', []);

//app.controller('QAParamController', function ($http, $scope) {

//    var ctrl = this;

//    this.ResourceID = 0;

//    this.listQAParams = [];

//    this.gridQAParams = [];

//    this.Init = function (resId) {
//        ctrl.ResourceID = resId;

//        // populate QA Parameters dropdown
//        $http({ url: '/resources/GetAllQAParamCodes', method: 'GET' })
//        .success(function (data) {
//            ctrl.listQAParams = data;
//        });

//        // populate QA Parameters grid
//        $http({ url: '/resources/GetAllQAParamResources/' + ctrl.ResourceID, method: 'GET' })
//        .success(function (data) {
//            ctrl.gridQAParams = data;
//        });
//    }

//    this.AddQAParam = function (param) {

//        // check if operation already exists
//        if (_.find(ctrl.gridQAParams, function (m) { return (m.QAParam.ID == param.ID) })) {
//            alert('This item already exists.');
//            return;
//        }

//        // add QA Parameters
//        var jsondata = { QAParamID: param.ID, ResourceID:ctrl.ResourceID };
//        $http({ url: '/resources/AddQAParam', method: 'POST', data: JSON.stringify(jsondata) })
//        .success(function (data) {
//            ctrl.gridQAParams.push(data);
//            toastr['success']('Record created.');
//        });
//    }

//    this.RemoveQAParam = function (param) {
//        if (!confirm('are you sure you want to remove this record?')) return;

//        $http({ url: '/resources/RemoveQAParam/?qaParamId=' + param.ID, method: 'GET' })
//        .success(function () {
//            ctrl.gridQAParams = _.reject(ctrl.gridQAParams, function (m) { return (m.ID == param.ID); });
//            toastr['success']('Record deleted.');
//        });
//    }




//})


//app.controller('SubResourceController', function($http, $scope){
//    var ctrl = this;

//    this.ResourceID = 0;

//    this.listSubresources = [];

//    this.gridSubresources = [];

//    this.Init = function (resId) {
//        ctrl.ResourceID = resId;

//        // populate Subresources dropdown
//        $http({ url: '/resources/GetAllResources', method: 'GET' })
//        .success(function (data) {
//            ctrl.listSubresources = data;
//        });

//        // populate Subresources grid
//        $http({ url: '/resources/GetAllSubresources/' + ctrl.ResourceID, method: 'GET' })
//        .success(function (data) {
//            ctrl.gridSubresources = data;
//        });
//    }

//    this.AddSubresource = function (sub) {

//        // search list if sub is already in it
//        if (_.find(this.gridSubresources, function (m) { if (m.Subresource.ID == sub.ID) return true; })) {
//            alert('The selected Resource already exists in the list.');
//            return;
//        }

//        // add Subresource
//        var jsonData = { ResourceID: ctrl.ResourceID, SubResourceID: sub.ID };
//        $http({ url: '/resources/AddSubresource/', method: 'POST', data: jsonData })
//        .success(function (data) {
//            ctrl.gridSubresources.push(data);
//            toastr['success']('Substitute Resource Added.');
//        });
//    }

//    this.RemoveSubresource = function (sub) {
//        if (!confirm('are you sure you want to remove this record?')) return;

//        $http({ url: '/resources/DeleteSubresource/?subResourceId=' + sub.ID, method: 'GET' })
//        .success(function (data) {
//            ctrl.gridSubresources = _.reject(ctrl.gridSubresources, function (m) { return (m.ID == sub.ID); });
//            toastr['success']('Substitute Resource deleted.');
//        });
        
//    }
//})