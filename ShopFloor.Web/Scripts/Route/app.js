﻿// ********************************************
// client insists on using cookieless sessions
// for multiple login per browser tabs. silly.
// but, if we dont, we dont get our peanuts ;)
// I transferred the code to Edit if you're
// looking for it. jfaquinojr@gmail.com
// ********************************************

//var app = angular.module('RouteApp', []);


//app.controller('OperationsController', function ($http, $rootScope) {

//    var ctrl = this;

//    this.RouteID = 0;
//    this.listOperations = []; // dropdown

//    this.gridOperations = []; // table

//    this.Init = function (routeid) {
//        ctrl.RouteID = routeid;

//        // populate dropdown list Operations
//        $http({ method: 'GET', url: '/routes/GetOperations/' })
//          .success(function (data) {
//              if (data) ctrl.listOperations = data;
//          });

//        // populate table Route Operations
//        $http({ method: 'GET', url: '/routes/GetRouteOperations/' + ctrl.RouteID })
//          .success(function (data) {
//              if (data) ctrl.gridOperations = data;
//          });
//    };

//    this.AddOperation = function (operation) {

//        // check if operation already exists
//        if (_.find(ctrl.gridOperations, function (m) { return (m.OperationID == operation.ID) }))
//        {
//            alert('This route operation already exists.');
//            return;
//        }

//        // associate this operation to route
//        operation.OperationID = operation.ID;
//        operation.RouteID = ctrl.RouteID;
//        $http({ 'method': 'POST', 'url': '/routes/AddRouteOperation/', 'data': JSON.stringify(operation) })
//        .success(function (newrouteoperation) {
//            ctrl.gridOperations.push(newrouteoperation);
//            $rootScope.$broadcast('operationCreated', ctrl.RouteID);
//            toastr['success']('Record successfully created.');
//        })
//        .error(function (data, status, headers, config) {
//            alert('An error has occurred while adding Operation');
//            //return $q.reject(result);
//        });
        

        
//    };

//    this.RemoveOperation = function (routeOp) {
//        if (!confirm('are you sure you want to remove this route operation?')) return;

//        $http({ 'method': 'GET', 'url': '/routes/DeleteRouteOperation/?routeOperationId=' + routeOp.ID })
//        .success(function () {
//            ctrl.gridOperations = _.reject(ctrl.gridOperations, function (m) { return (m.ID == routeOp.ID) });
//            $rootScope.$broadcast('operationRemoved', ctrl.RouteID);
//            toastr['success']('Record deleted.');
//        })
//        .error(function (data, status, headers, config) {
//            alert('This record is used elsewhere. Unable to delete.');
//        });
//    };

//});


//app.controller('ResourcesController', function ($http, $scope) {

//    var ctrl = this;

//    this.RouteID = 0;
//    this.listOperations = []; // dropdown
//    this.listResources = [];

//    this.gridResources = []; // table

//    $scope.$on('operationCreated', function (event, routeId) {
//        ctrl.Init(routeId);
//    });

//    $scope.$on('operationRemoved', function (event, routeId) {
//        ctrl.Init(routeId);
//    });

//    this.Init = function (routeid) {
//        ctrl.RouteID = routeid;


//        // populate Route Operations dropdown
//        $http({ method: 'GET', url: '/routes/GetRouteOperations/' + ctrl.RouteID })
//          .success(function (data) {
//              if (data) ctrl.listOperations = data;
//          });

//        // populate Resources dropdown
//        $http({ method: 'GET', url: '/routes/GetAllResources/' })
//          .success(function (data) {
//              if (data) ctrl.listResources = data;
//          });

//        // populate Route Operation Resources grid
//        $http({ method: 'GET', url: '/routes/GetRouteResources/' + ctrl.RouteID + '?operationId=0' })
//          .success(function (data) {
//              if (data) ctrl.gridResources = data;
//          });

//    };

//    this.ChangeOperation = function (operation) {

//        var opId = 0;
//        if (operation)
//            opId = operation.OperationID;

//        // populate Route Operation Resources grid
//        $http({ method: 'GET', url: '/routes/GetRouteResources/' + ctrl.RouteID + '?operationId=' + opId })
//          .success(function (data) {
//              if (data) ctrl.gridResources = data;
//          });
//    };

//    this.AddResource = function (routeOp, resource) {

//        // check if operation already exists
//        if (_.find(ctrl.gridResources, function (m) { return (m.RouteOperationID == routeOp.ID && m.ResourceID == resource.ID) })) {
//            alert('This routing setup already exists.');
//            return;
//        }

//        // associate this operation to route
//        var jsonData = { RouteOperationID: routeOp.ID, ResourceID: resource.ID }
//        $http({ 'method': 'POST', 'url': '/routes/AddRouteResource/', 'data': JSON.stringify(jsonData) })
//        .success(function (newrouteoperation) {
//            ctrl.gridResources.push(newrouteoperation);
//            toastr['success']('Record successfully created.');
//        })
//        .error(function (data, status, headers, config) {
//            alert('An error has occurred while adding Route Resource.');
//        });


//    };

//    this.RemoveResource = function (resource) {
//        if (!confirm('are you sure you want to remove this routing setup?')) return;

//        $http({ 'method': 'GET', 'url': '/routes/DeleteRouteResource/?routeOperationResourceId=' + resource.ID })
//        .success(function () {
//            ctrl.gridResources = _.reject(ctrl.gridResources, function (m) { return (m.ID == resource.ID) });
//            toastr['success']('Record deleted.');
//        })
//        .error(function (data, status, headers, config) {
//            alert('This record is still being used. Unable to delete. ');
//        });
//    };

//    this.CheckLastTag = function ($event, item) {

//        var checkbox = $event.target;
//        if (checkbox.checked == false) {

//        }
//        else {
//        }

//        $http({ 'method': 'GET', 'url': '/routes/UpdateRouteResourceLastTag/?routeOperationResourceId=' + item.ID })
//        .success(function () {
//            toastr['success']('Last Tag updated.');
//        })
//        .error(function (data, status, headers, config) {
//            alert('Unable to set the Last Tag. ');
//        });

//    };

//});


//app.controller('ItemsController', function ($http, $scope) {

//    var ctrl = this;

//    this.RouteID = 0;
//    this.gridItems = []; // table

//    this.Init = function (routeid) {
//        ctrl.RouteID = routeid;


//        // populate table Items
//        $http({ method: 'GET', url: '/routes/GetItemAssignments/' + ctrl.RouteID })
//          .success(function (data) {
//              if (data) ctrl.gridItems = data;
//          });
//    };

//    $scope.AddItem = function (itemcode) {

//        // check if item is already exists in ItemRoutes. 
//        $http({ method: 'GET', url: '/routes/ItemAssignmentExists/?itemcode=' + itemcode })
//          .success(function (route) {

//              if (!route) {


//                  // add itemroute
//                  var jsondata = { ItemCode: itemcode, RouteID: ctrl.RouteID };
//                  $http({ method: 'POST', url: '/routes/AddItemAssignment/', data: JSON.stringify(jsondata) })
//                    .success(function (data) {
//                        if (data) {
//                            ctrl.gridItems.push(data);
//                            toastr['success']('Record successfully created.');
//                        }
//                    })
//                  .error(function () {
//                      toastr['error']('Unable to create record');
//                  });

//              }
//              else
//              {
//                  alert('This item is already being used by ' + route.RouteCode + '.');
//              }

//          });


//    };

//    this.RemoveItem = function (item) {
//        if (!confirm('are you sure you want to remove this record?')) return;

//        $http({ method: 'GET', url: '/routes/DeleteItemAssignment/?itemRouteId=' + item.ID })
//          .success(function (data) {
//              ctrl.gridItems = _.reject(ctrl.gridItems, function (m) { return (m.ID == item.ID); });
//              toastr['success']('Record deleted.');
//          })
//          .error(function () {
//              alert('This record is still being used. Unable to delete.');
//          });

//    }

//});