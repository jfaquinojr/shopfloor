﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopFloor.Web.Models.DataTables
{
    public class SentParam
    {
        public int draw { get; set; }
        public int start { get; set; }
        public int length { get; set; }
        public string search_value { get; set; }
        public string search_regex { get; set; }
        public string order_i_column { get; set; }
        public string order_i_dir { get; set; }
        public string columns_i_date { get; set; }
        public string columns_i_name { get; set; }
        public string columns_i_searchable { get; set; }
        public string columns_i_orderable { get; set; }
        public string columns_i_search_value { get; set; }
        public string columns_i_search_regex { get; set; }

    }
}