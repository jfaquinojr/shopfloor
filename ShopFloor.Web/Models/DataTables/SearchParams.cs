﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopFloor.Web.Models.DataTables
{
    public class SearchParams
    {
        public string Table { get; set; }
        public string Filter { get; set; }
        public int draw { get; set; }
        public ColumnParam[] columns { get; set; }
        public OrderParam[] order { get; set; }
        public int start { get; set; }
        public int length { get; set; }
        public SearchParam search { get; set; }
    }

    public class OrderParam
    {
        public int column { get; set; }
        public string dir { get; set; }
    }
    public class ColumnParam
    {
        public string data { get; set; }
        public string name { get; set; }
        public bool searchable { get; set; }
        public bool orderable { get; set; }
        public SearchParam search { get; set; }


    }

    public class SearchParam
    {
        public string value { get; set; }
        public string regex { get; set; }
    }
}