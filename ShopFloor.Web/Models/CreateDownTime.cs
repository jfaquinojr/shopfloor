﻿using ShopFloor.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopFloor.Web.Models
{
	public class CreateDownTime
	{
		public DateTime CurrentDateTime { get; set; }
		[Required]
		[Remote("CheckIfMONumberExistsInHistory", "ManufacturingOrder", AdditionalFields = "MOTypeID")]
		public int MONumber { get; set; }
		[Required]
		public int RouteID { get; set; }
		public int MOTypeID { get; set; }
	}
}