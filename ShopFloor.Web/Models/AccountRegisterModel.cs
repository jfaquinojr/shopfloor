﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ShopFloor.Web.Models
{
    public class AccountRegisterModel
    {
        [Required]
        public string Username { get; set; }

        [Required]
        [Display(Name="Employee ID")]
        public string EmployeeID { get; set; }

        [Required]
        [Display(Name = "User ID")]
        public string UserID { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Confirm Password")]
        public string ConfirmPassword { get; set; }



    }
}