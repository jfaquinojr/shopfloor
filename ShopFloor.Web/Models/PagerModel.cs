﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopFloor.Web.Models
{
    public class PagerModel
    {
        public int PageSize { get; set; }
        public int PageCount { get; set; }
        public int CurrentPage { get; set; }
        public string SearchString { get; set; }
        public int TotalRowCount { get; set; }
        public string SortBy { get; set; }
        public bool Descending { get; set; }

    }
}