﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopFloor.Web.Models
{
    public class ErrorInfo
    {
        public Exception Exception { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
    }
}