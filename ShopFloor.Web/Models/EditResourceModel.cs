﻿using ShopFloor.Entities;
using ShopFloor.Managers;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace ShopFloor.Web.Models
{
    [NotMapped]
    public class EditResourceModel : Resource, IValidatableObject
    {
        public List<ResourceCategory> ResourceCategories { get; set; }
        public List<string> PRCCodes { get; set; }


        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if(ID == 0 && !string.IsNullOrWhiteSpace(ResourceCode))
            {
                var db = new ResourceManager();
                if (db.QueryAll().Any(m => m.ResourceCode.ToLower() == this.ResourceCode.ToLower()))
                {
                    yield return new ValidationResult("ResourceCode already exists.");
                }
            }
        }
    }


}