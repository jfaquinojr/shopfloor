﻿using ShopFloor.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ShopFloor.Web.Models
{
    public class ViewMOHeaderModel : MOHeader
    {
        [NotMapped]
        public string DisplayMOClosedTag
        {
            get
            {
                if (MOClosedTag ?? false)
                    return "Closed";

                return "Open";
            }
        }

        [NotMapped]
        public DateTime CurrentDateTime { get; set; }

    }
}