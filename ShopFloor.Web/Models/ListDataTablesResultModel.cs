﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopFloor.Web.Models
{
    public class ListDataTablesResultModel
    {
        public object aaData { get; set; }
        public string[] aoColumns { get; set; }
        public string sEcho { get; set; }
        public int iTotalRecords { get; set; }
        public int iTotalDisplayRecords { get; set; }
    }
}