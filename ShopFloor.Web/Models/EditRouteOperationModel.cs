﻿using ShopFloor.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopFloor.Web.Models
{
    public class EditRouteOperationModel : RouteOperation
    {
        public string RouteCode { get; set; }
        public string RouteDesc { get; set; }
        //public List<Operation> Operations { get; set; }
        //public List<Route> Routes { get; set; }
    }
}