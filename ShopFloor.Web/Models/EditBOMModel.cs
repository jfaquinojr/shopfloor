﻿using ShopFloor.Entities;
using ShopFloor.SAP.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopFloor.Web.Models
{
    [NotMapped]
    [MetadataType(typeof(BomHeaderMetadata))]
    public class EditBOMModel : BillOfMaterial
    {

        public IEnumerable<SelectListItem> GetBOMTypes()
        {
            var ret = new List<SelectListItem>();
            ret.Add(new SelectListItem { Value = "1", Text = "Estimation" });
            ret.Add(new SelectListItem { Value = "2", Text = "Product Devt" });
            ret.Add(new SelectListItem { Value = "3", Text = "Commercial" });
            return ret;
        }

        //public IEnumerable<SelectListItem> FGItemCodes { get; set; }

        public EditBOMModel()
        {
            BOMDetails = new List<BillOfMaterialDetail>();
        }

		public class BomHeaderMetadata
		{
			[Remote("IsUniqueBOMSeries", "BOM", AdditionalFields = "BOMCode, BOMType, ID")]
			public string BOMSeries { get; set; }
			public int ID { get; set; }
			[DisplayFormat(ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, DataFormatString = "{0:MM/dd/yyyy}")]
			public DateTime EffectFrom { get; set; }
			[DisplayFormat(ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, DataFormatString = "{0:MM/dd/yyyy}")]
			public DateTime EffectTo { get; set; }
		}
    }
}