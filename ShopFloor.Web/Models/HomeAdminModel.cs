﻿using ShopFloor.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopFloor.Web.Models
{
    public class HomeAdminModel
    {
        public List<User> Registrations { get; set; }

    }
}