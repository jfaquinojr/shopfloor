﻿using ShopFloor.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopFloor.Web.Models
{
    public class EditUserModel
    {
		public int ID { get; set; }
		public string UserID { get; set; }
		public string EmpID { get; set; }

		[Remote("IsValidUser", "Users", AdditionalFields = "ID")]
		[Required]
		public string UserName { get; set; }

		[Required]
		public int UserClassID { get; set; }

		[DataType(DataType.Password)]
		[Required]
		public string UserPW { get; set; }

		public bool ChangePassword { get; set; }

	}
}