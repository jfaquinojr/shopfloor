﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ShopFloor.Web.Models
{
	public class ProcessingModel
	{
		[Required]
		[DataType(DataType.Date)]
		public DateTime? DateFrom { get; set; }
		[Required]
		[DataType(DataType.Date)]
		public DateTime? DateTo { get; set; }

		public string LinkDescription { get; set; }

	}
}