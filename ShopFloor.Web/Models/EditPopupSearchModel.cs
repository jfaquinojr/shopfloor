﻿using ShopFloor.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopFloor.Web.Models
{

    public class EditPopupSearchModel : PagerModel
    {
        public EditPopupSearchModel()
        {
            SearchPrompt = "search for code or name";
            SearchTable = "item";
            DisplayFields = new string[] {"ItemCode", "ItemName"};
            IDField = "ItemCode";
        }

        public string SearchTable { get; set; }
        public string SearchPrompt { get; set; }

        public string[] DisplayFields { get; set; }
        public string IDField { get; set; }

        public object[] aaData { get; set; }
    }
}