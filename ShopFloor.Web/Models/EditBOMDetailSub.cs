﻿using ShopFloor.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ShopFloor.Web.Models
{
    [NotMapped]
    [MetadataType(typeof(EditBOMDetailSubMetaData))]
    public class EditBOMDetailSub : BOMDetailSub
    {
        public class EditBOMDetailSubMetaData
        {
            [Remote("IsValidRMCodeSub", "BOMDetailSubs", AdditionalFields = "BOMDetailID,ID")]
            [Display(Name = "RM Sub")]
            public string RMCodeSubs { get; set; }

            [Display(Name = "BOM Detail")]
            public int BOMDetailID { get; set; }


            [Display(Name = "RM Sub Name")]
            public string RMCodeSubsName { get; set; }
        }


    }
}
