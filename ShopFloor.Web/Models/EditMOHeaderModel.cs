﻿using ShopFloor.Entities;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace ShopFloor.Web.Models
{
	[MetadataType(typeof(EditMOHeaderMetaData))]
	public class EditMOHeaderModel : MOHeader
    {
        [NotMapped]
        public DateTime CurrentDateTime { get; set; } // holds the current date time the window was opened.

        [NotMapped]
        public bool IsMOClosing { get; set; }

		[NotMapped]
		public string RouteDisplayName { get; set; }


		public class EditMOHeaderMetaData
		{
			[Remote("CheckIfMONumberExistsInHistory", "ManufacturingOrder", AdditionalFields = "MOTypeID,ID")]
			public int MONumber { get; set; }
		}

    }
}