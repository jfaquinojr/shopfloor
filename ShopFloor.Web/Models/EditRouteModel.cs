﻿using ShopFloor.Entities;
using ShopFloor.SAP.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopFloor.Web.Models
{
    public class EditRouteModel : Route
    {
        public EditRouteModel()
        {
            Active = true;
        }

        //public List<Item> SAPItems { get; set; }
        public List<string> SBUCodes { get; set; }
    }
}