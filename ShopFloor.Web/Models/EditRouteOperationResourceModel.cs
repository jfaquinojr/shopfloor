﻿using ShopFloor.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopFloor.Web.Models
{
    [NotMapped]
    public class EditRouteOperationResourceModel : RouteOperationResource
    {
        //public IEnumerable<SelectListItem> RouteOperationList { get; set; }
        //public IEnumerable<SelectListItem> ResourceList { get; set; }

        [Required]
        public int OperationID { get; set; }
        [Required]
        public int RouteID { get; set; }

    }
}