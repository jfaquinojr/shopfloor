﻿using ShopFloor.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopFloor.Web.Models
{
	[NotMapped]
	public class EditMODetailFggModel : MODetailFgg
	{
		public string MOTypeCode { get; set; }
	}
}