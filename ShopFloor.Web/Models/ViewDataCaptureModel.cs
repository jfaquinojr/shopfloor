﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ShopFloor.Web.Models
{
    public class ViewDataCaptureFGG
    {
        public int MODetailFGGID { get; set; }
        public string ItemCode { get; set; }
        public string ItemDescription { get; set; }
        public int MOQuantity { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public bool Process { get; set; }
    }
}