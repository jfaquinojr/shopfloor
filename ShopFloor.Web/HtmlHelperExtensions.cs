﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using HtmlAgilityPack;

namespace ShopFloor.Web
{
    public static class WebExtensions
    {
        public static MvcHtmlString ListGroupItem(this HtmlHelper helper, string text, string action, string controller, string menu)
        {
            return ListGroupItem(helper, text, action, controller, menu, null);
        }

        public static MvcHtmlString ListGroupItem(this HtmlHelper helper, string text, string action, string controller)
        {
            return ListGroupItem(helper, text, action, controller, action, null);
        }

        public static MvcHtmlString ListGroupItem(this HtmlHelper helper, string text, string action, string controller, string menu, object routeValues)
        {
            // Convert the routeValues to something we can modify.
            var routeValuesLocal =
                routeValues as IDictionary<string, object>
                ?? new RouteValueDictionary(routeValues);

            var htmlAttributesLocal = new RouteValueDictionary();
            if (helper.ViewBag.ActiveMenu == menu)
            {
                htmlAttributesLocal.Add("class", "list-group-item active");
            }
            else
            {
                htmlAttributesLocal.Add("class", "list-group-item");
            }

            return helper.ActionLink(text, action, controller, new RouteValueDictionary(routeValuesLocal), htmlAttributesLocal);
        }

        public static MvcHtmlString ComboBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> selectList, object htmlAttributes)
        {
            // add list attribute
            //var attr = new RouteValueDictionary(htmlAttributes);
            var ex = (MemberExpression)expression.Body;
            string name = ex.Member.Name;
            string txtname = "txt-" + ex.Member.Name;
            //attr.Add("list", name);
        
            StringBuilder b = new StringBuilder();
            b.AppendLine("<input type=\"text\" id=\"" + txtname + "\" class='form-control' list=\""+ name +"\">");
            b.AppendFormat("<datalist id=\"{0}\">", name);
            b.AppendLine();

            foreach(var i in selectList)
            {
                b.AppendFormat("<option value=\"{0}\" text=\"{1}\">{1}</option>", i.Value, i.Text);
                b.AppendLine();
            }
            b.AppendLine("</datalist>");
            /*
                <input type="text" list="browsers">
                <datalist id="browsers">
                  <option value="Firefox">
                  <option value="IE">
                  <option value="Chrome">
                  <option value="Opera">
                  <option value="Safari">
                </datalist>
             */

            return new MvcHtmlString(b.ToString());
        }

        public static MvcForm BeginForm(this HtmlHelper htmlHelper, string formId)
        {
            return BeginForm(htmlHelper, formId, FormMethod.Post);
        }

        public static MvcForm BeginForm(this HtmlHelper htmlHelper, string formId, FormMethod method)
        {
            var rawUrl = htmlHelper.ViewContext.HttpContext.Request.RawUrl;
            var formAction = htmlHelper.ViewContext.HttpContext.Response.ApplyAppPathModifier("~/") + rawUrl;
            var builder = new TagBuilder("form");
            builder.MergeAttributes(new RouteValueDictionary());
            builder.MergeAttribute("action", formAction);
            builder.MergeAttribute("method", HtmlHelper.GetFormMethodString(method), true);
            builder.MergeAttribute("id", formId);
            htmlHelper.ViewContext.Writer.Write(builder.ToString(TagRenderMode.StartTag));
            var form = new MvcForm(htmlHelper.ViewContext);

            return form;
        }

		public static MvcHtmlString IsDisabled(this MvcHtmlString htmlString, bool disabled)
		{
			string rawstring = htmlString.ToString();
			if (disabled)
			{
				var htmlDoc = new HtmlDocument();
				htmlDoc.LoadHtml(rawstring);

				htmlDoc.DocumentNode.ChildNodes[0].Attributes.Add("disabled", "disabled");
				rawstring = htmlDoc.DocumentNode.OuterHtml;
			}
			return new MvcHtmlString(rawstring);
		}

		public static MvcHtmlString IsReadonly(this MvcHtmlString htmlString, bool @readonly)
		{
			string rawstring = htmlString.ToString();
			if (@readonly)
			{
				var htmlDoc = new HtmlDocument();
				htmlDoc.LoadHtml(rawstring);

				htmlDoc.DocumentNode.ChildNodes[0].Attributes.Add("readonly", "readonly");
				rawstring = htmlDoc.DocumentNode.OuterHtml;

			}
			return new MvcHtmlString(rawstring);
		}
	}
}