﻿using System.Web;

namespace ShopFloor.Web
{
    public static class MenuUtil
    {
        public static bool HasModuleAccess(string module, System.Security.Principal.IPrincipal user, HttpSessionStateBase session)
        {
            //roles: "AD,SU,OP,PL,PD,CE,QA"

            //if (session["ModuleRoles"] == null)
            //{
            //    var moduleRoles = new Dictionary<string, string[]>();
            //    moduleRoles.Add("TASKS", "AD,SU,OP,PL,PD".Split(','));
            //    moduleRoles.Add("ROUTING", "AD,SU,PD,QA".Split(','));
            //    moduleRoles.Add("LOOKUP", "AD,SU,QA".Split(','));
            //    moduleRoles.Add("ADMIN", "AD,SU".Split(','));
            //    session["ModuleRoles"] = moduleRoles;
            //}
            //var modules = session["ModuleRoles"] as Dictionary<string, string[]>;
            //return modules[module].Any(mr => roles.Any(r => r.ToLower() == mr.ToLower()));


            bool hasAccess = false;
            switch (module.ToLower())
            {
                case "admin":
                    hasAccess = user.IsInRole("AD") || user.IsInRole("SU");
                    break;
                case "tasks":
                    hasAccess = user.IsInRole("AD") || user.IsInRole("SU") || user.IsInRole("OP") || user.IsInRole("PL") || user.IsInRole("PL") || user.IsInRole("PD");
                    break;
                case "routing":
                    hasAccess = user.IsInRole("AD") || user.IsInRole("SU") || user.IsInRole("PD") || user.IsInRole("QA");
                    break;
                case "lookup":
                default:
                    hasAccess = user.IsInRole("AD") || user.IsInRole("SU") || user.IsInRole("QA");
                    break;
            }

            return hasAccess;

        }
    }
}