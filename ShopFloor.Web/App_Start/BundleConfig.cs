﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace ShopFloor.Web
{
	public class BundleConfig
	{
		public static void RegisterBundles(BundleCollection bundles)
		{


			bundles.Add(new ScriptBundle("~/Scripts/shopfloor").Include(
				"~/Scripts/jquery-{version}.js",
				"~/Scripts/bootstrap.*",
				"~/Scripts/jquery.validate.*",
				"~/Scripts/jquery.validate.unobtrusive.*",
				"~/Scripts/toastr.*",
				"~/Scripts/pace.*",
				"~/Scripts/jquery-ui-{version}.js",
				"~/Scripts/modernizr-{version}.js")
			);

			bundles.Add(new StyleBundle("~/Styles/shopfloor").Include(
				"~/Content/bootstrap.*",
				"~/Content/toastr.*",
				"~/Content/shop-item.*",
				"~/Content/sticky-footer-navbar.*",
				"~/Content/Site.*",
				"~/Content/font-awesome.*",
				"~/Content/pace.*")
			);

			bundles.Add(new ScriptBundle("~/Scripts/dt").Include(
				"~/Scripts/jquery.dataTables.*",
				"~/Scripts/dataTables.bootstrap.*")
			);

			bundles.Add(new StyleBundle("~/Styles/dt").Include(
				"~/Content/dataTables.bootstrap.*")
			);

			bundles.Add(new ScriptBundle("~/Scripts/wip").Include(
				"~/Scripts/xxxxxxx.js",
				"~/Scripts/xxxxxxx.js",
				"~/Scripts/xxxxxxx.js",
				"~/Scripts/xxxxxxx.js")
			);
		}
	}
}