﻿using ShopFloor.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Data.Entity;
using ShopFloor.SAP.Entities;
using System.Web.Optimization;

namespace ShopFloor.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);
            Database.SetInitializer<SAPDb>(null);
        }

        void Application_PostAuthenticateRequest()
        {
            if (User.Identity.IsAuthenticated)
            {
                var name = User.Identity.Name;
                UsersManager db = new UsersManager();
                var roles = db.GetRoles(name);
                HttpContext.Current.User =
                    Thread.CurrentPrincipal =
                    new GenericPrincipal(User.Identity, roles.ToArray());
            }
        }

    }
}
