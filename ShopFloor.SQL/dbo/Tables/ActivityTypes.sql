﻿CREATE TABLE [dbo].[ActivityTypes] (
    [ID]           INT          IDENTITY (1, 1) NOT NULL,
    [ActivityCode] VARCHAR (8)  NOT NULL,
    [ActivityName] VARCHAR (30) NOT NULL,
    [IsProductive] BIT          NULL,
    [Active]       BIT          NULL,
    [CompJETag]    BIT          NULL,
    [CreatedBy]    INT          NULL,
    [CreatedOn]    DATETIME     NULL,
    [ModifiedBy]   INT          NULL,
    [ModifiedOn]   DATETIME     NULL,
    CONSTRAINT [PK_ActivityTypes] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [IX_ActivityTypes] UNIQUE NONCLUSTERED ([ActivityCode] ASC)
);

