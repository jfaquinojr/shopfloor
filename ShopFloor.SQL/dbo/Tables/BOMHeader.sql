﻿CREATE TABLE [dbo].[BOMHeader] (
    [ID]             INT          IDENTITY (1, 1) NOT NULL,
    [BOMCode]        VARCHAR (12) NOT NULL,
    [BOMType]        TINYINT      NOT NULL,
    [BOMName]        VARCHAR (50) NOT NULL,
    [BOMSeries]      VARCHAR (10) NOT NULL,
    [UserID]         INT          NOT NULL,
    [IsFinal]        BIT          NULL,
    [CreatedBy]      INT          NULL,
    [CreatedOn]      DATETIME     NULL,
    [ModifiedBy]     INT          NULL,
    [ModifiedOn]     DATETIME     NULL,
    [EffectFrom]     DATETIME     NULL,
    [EffectTo]       DATETIME     NULL,
    [BOMHeadeIDCopy] INT          NULL,
    CONSTRAINT [PK_BOMHeader] PRIMARY KEY CLUSTERED ([ID] ASC)
);










GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_BOMHeader]
    ON [dbo].[BOMHeader]([BOMCode] ASC, [BOMType] ASC, [BOMSeries] ASC);



