﻿CREATE TABLE [dbo].[QAParams] (
    [ID]              INT             IDENTITY (1, 1) NOT NULL,
    [QAParaCode]      VARCHAR (15)    NOT NULL,
    [QAParaDesc]      VARCHAR (40)    NOT NULL,
    [QAParaValueNum]  NUMERIC (19, 6) NOT NULL,
    [CreatedBy]       INT             NULL,
    [CreatedOn]       DATETIME        NULL,
    [ModifiedBy]      INT             NULL,
    [ModifiedOn]      DATETIME        NULL,
    [QAParaValueNum2] NUMERIC (19, 6) NULL,
    CONSTRAINT [PK_QAParams] PRIMARY KEY CLUSTERED ([ID] ASC)
);



