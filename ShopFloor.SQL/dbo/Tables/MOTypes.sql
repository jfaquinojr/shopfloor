﻿CREATE TABLE [dbo].[MOTypes] (
    [ID]            INT          IDENTITY (1, 1) NOT NULL,
    [MOTypeCode]    VARCHAR (1)  NOT NULL,
    [MOTypeName]    VARCHAR (50) NOT NULL,
    [MOSeriesStart] INT          NOT NULL,
    [MOSeriesEnd]   INT          NULL,
    [CreatedBy]     INT          NULL,
    [CreatedOn]     DATETIME     CONSTRAINT [DF_MOTypes_CreatedOn] DEFAULT (getdate()) NULL,
    [ModifiedBy]    INT          NULL,
    [ModifiedOn]    DATETIME     NULL,
    [IsDeleted]     BIT          NULL,
    CONSTRAINT [PK_MOTypes] PRIMARY KEY CLUSTERED ([ID] ASC)
);






GO


