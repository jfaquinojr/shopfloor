﻿CREATE TABLE [dbo].[RunRateReason] (
    [ID]         INT           IDENTITY (1, 1) NOT NULL,
    [ReasonCode] VARCHAR (30)  NOT NULL,
    [ReasonName] VARCHAR (100) NOT NULL,
    [CreatedBy]  INT           NULL,
    [CreatedOn]  DATETIME      NULL,
    [ModifiedBy] INT           NULL,
    [ModifiedOn] DATETIME      NULL,
    CONSTRAINT [PK_RunRateReason] PRIMARY KEY CLUSTERED ([ID] ASC)
);

