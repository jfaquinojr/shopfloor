﻿CREATE TABLE [dbo].[JEProcessingHistory] (
    [ID]          INT      IDENTITY (1, 1) NOT NULL,
    [DateFrom]    DATETIME NOT NULL,
    [DateTo]      DATETIME NOT NULL,
    [GeneratedBy] INT      NOT NULL,
    [GeneratedOn] DATETIME NOT NULL,
    CONSTRAINT [PK_JEProcessingHistory] PRIMARY KEY CLUSTERED ([ID] ASC)
);



