﻿CREATE TABLE [dbo].[HSMODetailITM] (
    [ID]         INT             IDENTITY (1, 1) NOT NULL,
    [MOType]     VARCHAR (1)     NOT NULL,
    [MONumber]   INT             NOT NULL,
    [MOPriority] INT             NULL,
    [MONumbFGNo] INT             NOT NULL,
    [FGItemCode] VARCHAR (20)    NULL,
    [RMItemCode] VARCHAR (20)    NULL,
    [MOQty]      NUMERIC (19, 6) NULL,
    [SAPIssueNo] NUMERIC (9)     NULL,
    [IssuedQty]  NUMERIC (19, 6) NULL,
    CONSTRAINT [PK_HSMODetailITM] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [IX_HSMODetailITM] UNIQUE NONCLUSTERED ([MONumber] ASC, [MOType] ASC, [MONumbFGNo] ASC, [RMItemCode] ASC)
);







