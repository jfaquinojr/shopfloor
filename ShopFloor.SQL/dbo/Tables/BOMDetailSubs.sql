﻿CREATE TABLE [dbo].[BOMDetailSubs] (
    [ID]             INT              IDENTITY (1, 1) NOT NULL,
    [BOMDetailID]    INT              NOT NULL,
    [RMCodePrime]    VARCHAR (20)     NULL,
    [RMCodeSubs]     VARCHAR (20)     NOT NULL,
    [RMCodeSubsName] VARCHAR (500)    NULL,
    [RMQuantity]     NUMERIC (19, 12) NULL,
    [RMFactor]       NUMERIC (19, 12) NULL,
    [UOM]            VARCHAR (10)     NULL,
    [UOMFactor]      NUMERIC (19, 12) NULL,
    [Allowance]      NUMERIC (19, 12) NULL,
    [ItmsGrpCode]    SMALLINT         NULL,
    [UnitCost]       NUMERIC (19, 12) NULL,
    [UserID]         INT              NULL,
    [CreatedBy]      INT              NULL,
    [CreatedOn]      DATETIME         NULL,
    [ModifiedBy]     INT              NULL,
    [ModifiedOn]     DATETIME         NULL,
    CONSTRAINT [PK_BOMDetailSubs] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [IX_BOMDetailSubs] UNIQUE NONCLUSTERED ([BOMDetailID] ASC, [RMCodeSubs] ASC)
);












GO


