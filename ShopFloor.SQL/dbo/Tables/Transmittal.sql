﻿CREATE TABLE [dbo].[Transmittal] (
    [ID]       INT IDENTITY (1, 1) NOT NULL,
    [MODetailFGGID]  INT NOT NULL,
    [TrnsmQty] INT NOT NULL,
    CONSTRAINT [PK_Transmittal] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [IX_Transmittal_FGG] UNIQUE NONCLUSTERED ([MODetailFGGID] ASC)
);

