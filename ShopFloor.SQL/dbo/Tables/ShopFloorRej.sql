﻿CREATE TABLE [dbo].[ShopFloorRej] (
    [ID]           INT             IDENTITY (1, 1) NOT NULL,
    [ShopFloorID]  INT             NOT NULL,
    [Reject]       NUMERIC (19, 6) NOT NULL,
    [QACriteriaID] INT             NOT NULL,
    [CreatedBy]    INT             NULL,
    [CreatedOn]    DATETIME        NULL,
    [ModifiedBy]   INT             NULL,
    [ModifiedOn]   DATETIME        NULL,
    CONSTRAINT [PK_ShopFloorRej] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_ShopFloorRej_QACriteria] FOREIGN KEY ([QACriteriaID]) REFERENCES [dbo].[QACriteria] ([ID]),
    CONSTRAINT [FK_ShopFloorRej_ShopFloor] FOREIGN KEY ([ShopFloorID]) REFERENCES [dbo].[ShopFloor] ([ID])
);

