﻿CREATE TABLE [dbo].[ShopFloor] (
    [ID]              INT             IDENTITY (1, 1) NOT NULL,
    [StartDate]       DATETIME        NULL,
    [EndDate]         DATETIME        NULL,
    [BatchPrcnt]      INT             NULL,
    [ActivityTypeID]  INT             NULL,
    [Duration]        NUMERIC (19, 6) NULL,
    [Good]            NUMERIC (19, 6) NULL,
    [ItmsGrpCode]     SMALLINT        NULL,
    [LastTag]         BIT             NULL,
    [UserID]          INT             NULL,
    [RunRateReasonID] INT             NULL,
    [FinJETag]        BIT             NULL,
    [MODetailRSCID]   INT             NULL,
    [CreatedBy]       INT             NULL,
    [CreatedOn]       DATETIME        NULL,
    [ModifiedBy]      INT             NULL,
    [ModifiedOn]      DATETIME        NULL,
    [SubResourceID]   INT             NULL,
    [RscStartDate]    DATETIME        NULL,
    [RscEndDate]      DATETIME        NULL,
    [ResourceCode]    VARCHAR (30)    NULL,
    [ResourceDesc]    VARCHAR (100)   NULL,
    [MONumber]        INT             NULL,
    [ItemCode]        VARCHAR (20)    NULL,
    [ItemName]        VARCHAR (100)   NULL,
    [MOHeaderID]      INT             NULL,
    [MODetailFGGID]   INT             NULL,
    [NoOfOuts]        INT             NULL,
    CONSTRAINT [PK_ShopFloor] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_ShopFloor_ActivityTypes] FOREIGN KEY ([ActivityTypeID]) REFERENCES [dbo].[ActivityTypes] ([ID]),
    CONSTRAINT [FK_ShopFloor_RunRateReason] FOREIGN KEY ([RunRateReasonID]) REFERENCES [dbo].[RunRateReason] ([ID])
);











