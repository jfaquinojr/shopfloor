﻿CREATE TABLE [dbo].[HSMODetailFGG] (
    [ID]           INT          IDENTITY (1, 1) NOT NULL,
    [MOType]       VARCHAR (1)  NOT NULL,
    [MONumber]     INT          NOT NULL,
    [FGItemCode]   VARCHAR (20) NOT NULL,
    [BatchQty]     NUMERIC (12) NULL,
    [BatchPrcnt]   INT          NULL,
    [SONo]         INT          NULL,
    [DeliveryDate] DATETIME     NULL,
    [MOPriority]   INT          NULL,
    [OutsTag]      INT          NULL,
    CONSTRAINT [PK_HSMODetailFGG_1] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [IX_HSMODetailFGG] UNIQUE NONCLUSTERED ([MONumber] ASC, [MOType] ASC, [FGItemCode] ASC)
);







