﻿CREATE TABLE [dbo].[MODetailRSC] (
    [ID]            INT             IDENTITY (1, 1) NOT NULL,
    [MODetailFGGID] INT             NOT NULL,
    [FromDate]      DATETIME        NULL,
    [ToDate]        DATETIME        NULL,
    [EstTime]       NUMERIC (19, 6) NULL,
    [OrigFromDate]  DATETIME        NULL,
    [OrigToDate]    DATETIME        NULL,
    [OrigEstTime]   NUMERIC (19, 6) NULL,
    [Sequence]      INT             NULL,
    [ClosedTag]     BIT             NULL,
    [OnStreamTag]   BIT             NULL,
    [OnHold]        BIT             NULL,
    [PriorityTag]   BIT             NULL,
    [BatchPercent]  FLOAT (53)      NULL,
    [SetupTime]     NUMERIC (19, 6) NULL,
    [StockTime]     NUMERIC (19, 6) NULL,
    [CreatedBy]     INT             NULL,
    [CreatedOn]     DATETIME        NULL,
    [ModifiedBy]    INT             NULL,
    [ModifiedOn]    DATETIME        NULL,
    [OperationCode] VARCHAR (30)    NULL,
    [ResourceID]    INT             NULL,
    [ResourceCode]  VARCHAR (30)    NULL,
    [ResourceDesc]  VARCHAR (100)   NULL,
    CONSTRAINT [PK_MODetailRSC] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_MODetailRSC_MODetailFGG] FOREIGN KEY ([MODetailFGGID]) REFERENCES [dbo].[MODetailFGG] ([ID])
);







