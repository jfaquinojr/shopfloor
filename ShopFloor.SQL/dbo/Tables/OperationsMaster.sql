﻿CREATE TABLE [dbo].[OperationsMaster] (
    [ID]         INT          IDENTITY (1, 1) NOT NULL,
    [OperCode]   VARCHAR (5)  NOT NULL,
    [OperDesc]   VARCHAR (25) NULL,
    [Sequence]   INT          NULL,
    [Active]     BIT          CONSTRAINT [DF_OperationsMaster_Active] DEFAULT ((1)) NULL,
    [CreatedBy]  INT          NULL,
    [CreatedOn]  DATETIME     NULL,
    [ModifiedBy] INT          NULL,
    [ModifiedOn] DATETIME     NULL,
    CONSTRAINT [PK_OperationsMaster_1] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [IX_OperationsMaster] UNIQUE NONCLUSTERED ([OperCode] ASC)
);

