﻿CREATE TABLE [dbo].[Users] (
    [ID]             INT           IDENTITY (1, 1) NOT NULL,
    [UserID]         VARCHAR (10)  NULL,
    [EmpID]          VARCHAR (20)  NULL,
    [UserName]       VARCHAR (60)  NOT NULL,
    [UserPW]         VARCHAR (100) NOT NULL,
    [VerifiedBy]     INT           NULL,
    [VerifiedOn]     DATETIME      NULL,
    [IsVerified]     BIT           CONSTRAINT [DF_Users_IsVerified] DEFAULT ((0)) NULL,
    [RegisterDate]   DATETIME      NULL,
    [RequestDate]    DATETIME      NULL,
    [ChangePassword] BIT           NULL,
    [CreatedBy]      INT           NULL,
    [CreatedOn]      DATETIME      NULL,
    [ModifiedBy]     INT           NULL,
    [ModifiedOn]     DATETIME      NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED ([ID] ASC)
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Users_Username]
    ON [dbo].[Users]([UserName] ASC);

