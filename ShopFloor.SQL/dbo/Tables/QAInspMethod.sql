﻿CREATE TABLE [dbo].[QAInspMethod] (
    [ID]          INT          IDENTITY (1, 1) NOT NULL,
    [QAInspMCode] VARCHAR (15) NOT NULL,
    [QAInspMDesc] VARCHAR (40) NOT NULL,
    [CreatedBy]   INT          NULL,
    [CreatedOn]   DATETIME     NULL,
    [ModifiedBy]  INT          NULL,
    [ModifiedOn]  DATETIME     NULL,
    CONSTRAINT [PK_QAInspMethod] PRIMARY KEY CLUSTERED ([ID] ASC)
);

