﻿CREATE TABLE [dbo].[MOHeader] (
    [ID]              INT           IDENTITY (1, 1) NOT NULL,
    [MONumber]        INT           NOT NULL,
    [MOPriority]      INT           NULL,
    [MOClosedTag]     BIT           NULL,
    [GangRunTag]      BIT           NULL,
    [CreatedBy]       INT           NULL,
    [CreatedOn]       DATETIME      NULL,
    [ModifiedBy]      INT           NULL,
    [ModifiedOn]      DATETIME      NULL,
    [IsScheduled]     BIT           NULL,
    [IsDeleted]       BIT           NULL,
    [MOTypeID]        INT           NOT NULL,
    [MOInstructions]  VARCHAR (300) NULL,
    [DownTimeRouteID] INT           NULL,
    CONSTRAINT [PK_MOHeader] PRIMARY KEY CLUSTERED ([ID] ASC)
);
















GO


