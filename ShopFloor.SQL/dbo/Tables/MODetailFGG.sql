﻿CREATE TABLE [dbo].[MODetailFGG] (
    [ID]              INT           IDENTITY (1, 1) NOT NULL,
    [MOHeaderID]      INT           NOT NULL,
    [BatchQty]        FLOAT (53)    NULL,
    [BatchPrcnt]      FLOAT (53)    NULL,
    [SONo]            INT           NULL,
    [DeliveryDate]    DATETIME      NULL,
    [OutsTag]         INT           NULL,
    [CreatedBy]       INT           NULL,
    [CreatedOn]       DATETIME      NULL,
    [ModifiedBy]      INT           NULL,
    [ModifiedOn]      DATETIME      NULL,
    [IsScheduled]     BIT           NULL,
    [SAPRDR1DocEntry] INT           NULL,
    [SAPRDR1LineNum]  INT           NULL,
    [ItemCode]        VARCHAR (20)  NOT NULL,
    [ItemName]        VARCHAR (100) NULL,
    CONSTRAINT [PK_MODetailFGG] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_MODetailFGG_MOHeader] FOREIGN KEY ([MOHeaderID]) REFERENCES [dbo].[MOHeader] ([ID])
);







