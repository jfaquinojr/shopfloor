﻿CREATE TABLE [dbo].[HSMOHeader] (
    [ID]             INT           IDENTITY (1, 1) NOT NULL,
    [MOType]         VARCHAR (1)   NOT NULL,
    [MONumber]       INT           NOT NULL,
    [MOPriority]     INT           NULL,
    [DateCreated]    DATETIME      NULL,
    [GangRunTag]     SMALLINT      NULL,
    [MOClosedTag]    SMALLINT      NULL,
    [MOInstructions] VARCHAR (300) NULL,
    CONSTRAINT [PK_HSMOHeader] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [IX_HSMOHeader] UNIQUE NONCLUSTERED ([MOType] ASC, [MONumber] ASC)
);









