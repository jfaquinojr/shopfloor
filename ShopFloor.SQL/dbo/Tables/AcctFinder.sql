﻿CREATE TABLE [dbo].[AcctFinder] (
    [MOType]         VARCHAR (1)    NOT NULL,
    [ItmsGrpCode]    SMALLINT       NOT NULL,
    [AcctCode]       NVARCHAR (15)  NOT NULL,
    [DBCDTag]        SMALLINT       NOT NULL,
    [AcctName]       NVARCHAR (100) NULL,
    [ContraAcctCode] NVARCHAR (15)  NOT NULL,
    [ContraAcctName] NVARCHAR (100) NULL,
    CONSTRAINT [PK_AcctFinder] PRIMARY KEY CLUSTERED ([MOType] ASC, [ItmsGrpCode] ASC, [DBCDTag] ASC)
);

