﻿CREATE TABLE [dbo].[AdditionalReasons] (
    [ID]         INT          IDENTITY (1, 1) NOT NULL,
    [ReasonCode] VARCHAR (8)  NOT NULL,
    [ReasonName] VARCHAR (30) NOT NULL,
    [Active]     BIT          NULL,
    [CreatedBy]  INT          NULL,
    [CreatedOn]  DATETIME     NULL,
    [ModifiedBy] INT          NULL,
    [ModifiedOn] DATETIME     NULL,
    CONSTRAINT [PK_AdditionalReasons] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [IX_AdditionalReasons] UNIQUE NONCLUSTERED ([ReasonCode] ASC)
);

