﻿CREATE TABLE [dbo].[UserUserClass] (
    [ID]          INT      IDENTITY (1, 1) NOT NULL,
    [UserID]      INT      NULL,
    [UserClassID] INT      NULL,
    [CreatedBy]   INT      NULL,
    [CreatedOn]   DATETIME NULL,
    [ModifiedBy]  INT      NULL,
    [ModifiedOn]  DATETIME NULL,
    CONSTRAINT [PK_UserUserClass] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_UserUserClass_UserClass] FOREIGN KEY ([UserClassID]) REFERENCES [dbo].[UserClass] ([ID]),
    CONSTRAINT [FK_UserUserClass_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([ID])
);

