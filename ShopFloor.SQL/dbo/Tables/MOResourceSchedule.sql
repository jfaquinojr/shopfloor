﻿CREATE TABLE [dbo].[MOResourceSchedule] (
    [ID]            INT      IDENTITY (1, 1) NOT NULL,
    [MODetailRSCID] INT      NOT NULL,
    [ResourceID]    INT      NOT NULL,
    [StartDateTime] DATETIME NULL,
    [EndDateTime]   DATETIME NULL,
    [CreatedBy]     INT      NULL,
    [CreatedOn]     DATETIME NULL,
    [ModifiedBy]    INT      NULL,
    [ModifiedOn]    DATETIME NULL,
    [OnStreamTag]   BIT      NULL,
    [OnHold]        BIT      NULL,
    [Closed]        BIT      NULL,
    [MOHeaderID]    INT      NOT NULL,
    CONSTRAINT [PK_MOResourceSchedule] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_MOResourceSchedule_MODetailRSC] FOREIGN KEY ([MODetailRSCID]) REFERENCES [dbo].[MODetailRSC] ([ID])
);

