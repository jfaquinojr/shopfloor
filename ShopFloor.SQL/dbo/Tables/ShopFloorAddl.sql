﻿CREATE TABLE [dbo].[ShopFloorAddl] (
    [ID]                 INT             IDENTITY (1, 1) NOT NULL,
    [ShopFloorID]        INT             NOT NULL,
    [RMItemCode]         VARCHAR (20)    NOT NULL,
    [AddlQty]            NUMERIC (19, 6) NOT NULL,
    [AdditionalReasonID] INT             NOT NULL,
    [CreatedBy]          INT             NULL,
    [CreatedOn]          DATETIME        NULL,
    [ModifiedBy]         INT             NULL,
    [ModifiedOn]         DATETIME        NULL,
    CONSTRAINT [PK_ShopFloorAddl] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_ShopFloorAddl_AdditionalReasons] FOREIGN KEY ([AdditionalReasonID]) REFERENCES [dbo].[AdditionalReasons] ([ID]),
    CONSTRAINT [FK_ShopFloorAddl_ShopFloor] FOREIGN KEY ([ShopFloorID]) REFERENCES [dbo].[ShopFloor] ([ID])
);

