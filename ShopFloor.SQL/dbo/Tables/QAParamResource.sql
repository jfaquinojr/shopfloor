﻿CREATE TABLE [dbo].[QAParamResource] (
    [ID]         INT      IDENTITY (1, 1) NOT NULL,
    [ResourceID] INT      NOT NULL,
    [QAParamID]  INT      NOT NULL,
    [CreatedBy]  INT      NULL,
    [CreatedOn]  DATETIME NULL,
    [ModifiedBy] INT      NULL,
    [ModifiedOn] DATETIME NULL,
    CONSTRAINT [PK_QAParamResource] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_QAParamResource_QAParams] FOREIGN KEY ([QAParamID]) REFERENCES [dbo].[QAParams] ([ID])
);

