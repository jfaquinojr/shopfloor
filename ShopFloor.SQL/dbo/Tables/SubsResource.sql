﻿CREATE TABLE [dbo].[SubsResource] (
    [ID]            INT           IDENTITY (1, 1) NOT NULL,
    [SubResourceID] INT           NOT NULL,
    [Description]   VARCHAR (100) NULL,
    [ResourceID]    INT           NOT NULL,
    [CreatedBy]     INT           NULL,
    [CreatedOn]     DATETIME      NULL,
    [ModifiedBy]    INT           NULL,
    [ModifiedOn]    DATETIME      NULL,
    CONSTRAINT [PK_SubsResource] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_SubsResource_ResourceMaster] FOREIGN KEY ([ResourceID]) REFERENCES [dbo].[ResourceMaster] ([ID])
);

