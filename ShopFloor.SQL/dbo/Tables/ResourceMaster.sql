﻿CREATE TABLE [dbo].[ResourceMaster] (
    [ID]                 INT             IDENTITY (1, 1) NOT NULL,
    [ResourceCode]       VARCHAR (30)    NOT NULL,
    [ResourceDesc]       VARCHAR (100)   NULL,
    [PrcCode]            NVARCHAR (30)   NULL,
    [ResourceCategoryID] INT             NULL,
    [SetupTime]          NUMERIC (19, 6) NULL,
    [StockTime]          NUMERIC (19, 6) NULL,
    [OnStreamTag]        BIT             NULL,
    [FromDate]           DATETIME        NULL,
    [ToDate]             DATETIME        NULL,
    [ResourceRate]       NUMERIC (19, 6) NULL,
    [UnitTag]            BIT             NULL,
    [Active]             BIT             NULL,
    [CreatedBy]          INT             NULL,
    [CreatedOn]          DATETIME        NULL,
    [ModifiedBy]         INT             NULL,
    [ModifiedOn]         DATETIME        NULL,
    [CostRate]           NUMERIC (19, 6) NULL,
    CONSTRAINT [PK_ResourceMaster] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_ResourceMaster_ResourceCategory] FOREIGN KEY ([ResourceCategoryID]) REFERENCES [dbo].[ResourceCategory] ([ID]),
    CONSTRAINT [IX_ResourceMaster] UNIQUE NONCLUSTERED ([ResourceCode] ASC)
);







