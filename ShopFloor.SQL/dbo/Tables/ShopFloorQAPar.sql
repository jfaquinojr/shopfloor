﻿CREATE TABLE [dbo].[ShopFloorQAPar] (
    [ID]            INT             IDENTITY (1, 1) NOT NULL,
    [ShopFloorID]   INT             NOT NULL,
    [QAParamID]     INT             NOT NULL,
    [QAParamActual] NUMERIC (19, 6) NOT NULL,
    [CreatedBy]     INT             NULL,
    [CreatedOn]     DATETIME        NULL,
    [ModifiedBy]    INT             NULL,
    [ModifiedOn]    DATETIME        NULL,
    CONSTRAINT [PK_ShopFloorQAPar] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_ShopFloorQAPar_QAParams] FOREIGN KEY ([QAParamID]) REFERENCES [dbo].[QAParams] ([ID]),
    CONSTRAINT [FK_ShopFloorQAPar_ShopFloor] FOREIGN KEY ([ShopFloorID]) REFERENCES [dbo].[ShopFloor] ([ID])
);

