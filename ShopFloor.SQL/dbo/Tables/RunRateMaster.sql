﻿CREATE TABLE [dbo].[RunRateMaster] (
    [ID]          INT      IDENTITY (1, 1) NOT NULL,
    [ResourceID]  INT      NOT NULL,
    [ItemRouteID] INT      NOT NULL,
    [Rate]        INT      NOT NULL,
    [CreatedBy]   INT      NULL,
    [CreatedOn]   DATETIME NULL,
    [ModifiedBy]  INT      NULL,
    [ModifiedOn]  DATETIME NULL,
    CONSTRAINT [PK_RunRateMaster] PRIMARY KEY CLUSTERED ([ID] ASC)
);

