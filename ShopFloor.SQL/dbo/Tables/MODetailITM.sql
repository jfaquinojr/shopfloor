﻿CREATE TABLE [dbo].[MODetailITM] (
    [ID]            INT              IDENTITY (1, 1) NOT NULL,
    [MODetailFGGID] INT              NOT NULL,
    [RMItemCode]    VARCHAR (20)     NOT NULL,
    [RMItemName]    VARCHAR (100)    NULL,
    [MOQty]         NUMERIC (19, 6)  NULL,
    [IssuedQty]     NUMERIC (19, 6)  NULL,
    [SAPIssueNo]    NUMERIC (18)     NULL,
    [UnitOfMeasure] VARCHAR (100)    NULL,
    [RMFactor]      NUMERIC (19, 12) NULL,
    [UOMFactor]     NUMERIC (19, 12) NULL,
    [Allowance]     NUMERIC (19, 12) NULL,
    [ItmsGrpCod]    SMALLINT         NULL,
    [CreatedBy]     INT              NULL,
    [CreatedOn]     DATETIME         NULL,
    [ModifiedBy]    INT              NULL,
    [ModifiedOn]    DATETIME         NULL,
    [BOMDetailID]   INT              NULL,
    [SAPUnitCost]   NUMERIC (19, 6)  NULL,
    [BOMHeaderID]   INT              NULL,
    [IssuedQty2]    NUMERIC (19, 6)  NULL,
    CONSTRAINT [PK_MODetailITM] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_MODetailITM_MODetailFGG] FOREIGN KEY ([MODetailFGGID]) REFERENCES [dbo].[MODetailFGG] ([ID])
);











