﻿CREATE TABLE [dbo].[ResourceCategory] (
    [ID]          INT           IDENTITY (1, 1) NOT NULL,
    [Code]        VARCHAR (10)  NOT NULL,
    [Description] VARCHAR (100) NULL,
    [Active]      BIT           CONSTRAINT [DF_ResourceCategory_Active] DEFAULT ((1)) NULL,
    [CreatedBy]   INT           NULL,
    [CreatedOn]   DATETIME      NULL,
    [ModifiedBy]  INT           NULL,
    [ModifiedOn]  DATETIME      NULL,
    CONSTRAINT [PK_ResourceCategory] PRIMARY KEY CLUSTERED ([ID] ASC)
);

