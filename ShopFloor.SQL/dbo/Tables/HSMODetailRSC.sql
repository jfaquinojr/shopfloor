﻿CREATE TABLE [dbo].[HSMODetailRSC] (
    [ID]           INT             IDENTITY (1, 1) NOT NULL,
    [MOType]       VARCHAR (1)     NOT NULL,
    [MONumber]     INT             NOT NULL,
    [FGItemCode]   VARCHAR (20)    NOT NULL,
    [ResourceCode] VARCHAR (30)    NOT NULL,
    [SetupTime]    NUMERIC (19, 6) NULL,
    [StockTime]    NUMERIC (19, 6) NULL,
    [FromDate]     DATETIME        NULL,
    [ToDate]       DATETIME        NULL,
    [EstTime]      NUMERIC (19, 6) NULL,
    [OrigFromDate] DATETIME        NULL,
    [OrigToDate]   DATETIME        NULL,
    [OrigEstTime]  NUMERIC (19, 6) NULL,
    [RSCSeq]       INT             NULL,
    [ClosedTag]    SMALLINT        NULL,
    [OnStreamTag]  SMALLINT        NULL,
    [MOStat]       SMALLINT        NULL,
    [MOPriority]   INT             NULL,
    [PriorityTag]  SMALLINT        NULL,
    [BatchPrcnt]   SMALLINT        NULL,
    CONSTRAINT [PK_HSMODetailRSC] PRIMARY KEY CLUSTERED ([ID] ASC)
);






GO


