﻿CREATE TABLE [dbo].[UserClass] (
    [ID]         INT           IDENTITY (1, 1) NOT NULL,
    [UClassCode] CHAR (2)      NOT NULL,
    [UClassDesc] VARCHAR (500) NULL,
    [CreatedBy]  INT           NULL,
    [CreatedOn]  DATETIME      NULL,
    [ModifiedBy] INT           NULL,
    [ModifiedOn] DATETIME      NULL,
    CONSTRAINT [PK_UserClass] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_UserClass]
    ON [dbo].[UserClass]([UClassCode] ASC);

