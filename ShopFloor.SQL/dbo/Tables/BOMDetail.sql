﻿CREATE TABLE [dbo].[BOMDetail] (
    [ID]          INT              IDENTITY (1, 1) NOT NULL,
    [BOMHeaderID] INT              NOT NULL,
    [RMCode]      VARCHAR (20)     NOT NULL,
    [RMName]      VARCHAR (500)    NOT NULL,
    [RMQuantity]  NUMERIC (19, 12) NOT NULL,
    [RMFactor]    NUMERIC (19, 12) NULL,
    [UOM]         VARCHAR (10)     NULL,
    [UOMFactor]   NUMERIC (19, 12) NULL,
    [Allowance]   NUMERIC (19, 12) NULL,
    [ItmsGrpCod]  SMALLINT         NULL,
    [UnitCost]    NUMERIC (19, 12) NULL,
    [UserID]      INT              NOT NULL,
    [CreatedBy]   INT              NULL,
    [CreatedOn]   DATETIME         NULL,
    [ModifiedBy]  INT              NULL,
    [ModifiedOn]  DATETIME         NULL,
    [ItmsGrpCode] INT              NULL,
    [SAPUnitCost] NUMERIC (19, 12) NULL,
    CONSTRAINT [PK_BOMDetail] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_BOMDetail_BOMHeader] FOREIGN KEY ([BOMHeaderID]) REFERENCES [dbo].[BOMHeader] ([ID]),
    CONSTRAINT [IX_BOMDetail] UNIQUE NONCLUSTERED ([BOMHeaderID] ASC, [RMCode] ASC)
);





