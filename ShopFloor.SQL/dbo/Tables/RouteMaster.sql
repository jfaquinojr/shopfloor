﻿CREATE TABLE [dbo].[RouteMaster] (
    [ID]          INT           IDENTITY (1, 1) NOT NULL,
    [RouteCode]   VARCHAR (5)   NOT NULL,
    [RouteDesc]   VARCHAR (100) NULL,
    [Active]      BIT           NULL,
    [SBUCode]     VARCHAR (5)   NULL,
    [CreatedBy]   INT           NULL,
    [CreatedOn]   DATETIME      NULL,
    [ModifiedBy]  INT           NULL,
    [ModifiedOn]  DATETIME      NULL,
    [DownTimeTag] BIT           NULL,
    CONSTRAINT [PK_OperationsMaster] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [IX_RouteMaster] UNIQUE NONCLUSTERED ([RouteCode] ASC)
);







