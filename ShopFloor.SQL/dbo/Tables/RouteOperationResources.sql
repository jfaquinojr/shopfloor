﻿CREATE TABLE [dbo].[RouteOperationResources] (
    [ID]               INT      IDENTITY (1, 1) NOT NULL,
    [RouteOperationID] INT      NOT NULL,
    [ResourceID]       INT      NOT NULL,
    [Active]           BIT      NULL,
    [Sequence]         INT      NULL,
    [LastTag]          BIT      NULL,
    [CreatedBy]        INT      NULL,
    [CreatedOn]        DATETIME NULL,
    [ModifiedBy]       INT      NULL,
    [ModifiedOn]       DATETIME NULL,
    CONSTRAINT [PK_RouteOperationResources] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_RouteOperationResources_ResourceMaster] FOREIGN KEY ([ResourceID]) REFERENCES [dbo].[ResourceMaster] ([ID]),
    CONSTRAINT [FK_RouteOperationResources_RouteOperations] FOREIGN KEY ([RouteOperationID]) REFERENCES [dbo].[RouteOperations] ([ID])
);

