﻿CREATE TABLE [dbo].[QACriteria] (
    [ID]             INT          IDENTITY (1, 1) NOT NULL,
    [QACritCode]     VARCHAR (15) NOT NULL,
    [QACritDesc]     VARCHAR (60) NOT NULL,
    [QAInspMethodID] INT          NOT NULL,
    [CreatedBy]      INT          NULL,
    [CreatedOn]      DATETIME     NULL,
    [ModifiedBy]     INT          NULL,
    [ModifiedOn]     DATETIME     NULL,
    CONSTRAINT [PK_QACriteria] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_QACriteria_QAInspMethod] FOREIGN KEY ([QAInspMethodID]) REFERENCES [dbo].[QAInspMethod] ([ID])
);

