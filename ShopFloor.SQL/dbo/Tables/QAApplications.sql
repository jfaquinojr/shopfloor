﻿CREATE TABLE [dbo].[QAApplications] (
    [ID]         INT          IDENTITY (1, 1) NOT NULL,
    [QAApplCode] VARCHAR (15) NOT NULL,
    [QAApplDesc] VARCHAR (40) NOT NULL,
    [CreatedBy]  INT          NULL,
    [CreatedOn]  DATETIME     NULL,
    [ModifiedBy] INT          NULL,
    [ModifiedOn] DATETIME     NULL,
    CONSTRAINT [PK_QAApplications] PRIMARY KEY CLUSTERED ([ID] ASC)
);

