﻿CREATE TABLE [dbo].[ItemRoutes] (
    [ID]          INT           IDENTITY (1, 1) NOT NULL,
    [RouteID]     INT           NOT NULL,
    [ItemCode]    VARCHAR (20)  NOT NULL,
    [ItemName]    VARCHAR (100) NOT NULL,
    [ItmsGrpCod]  INT           NOT NULL,
    [U_ITEMCAT01] VARCHAR (30)  NULL,
    [CreatedBy]   INT           NULL,
    [CreatedOn]   DATETIME      NULL,
    [ModifiedBy]  INT           NULL,
    [ModifiedOn]  DATETIME      NULL,
    [Active]      BIT           CONSTRAINT [DF_ItemRoutes_Active] DEFAULT ((1)) NULL,
    CONSTRAINT [PK_ItemRoutes] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_ItemRoutes]
    ON [dbo].[ItemRoutes]([ItemCode] ASC);

