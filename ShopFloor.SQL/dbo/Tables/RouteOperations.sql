﻿CREATE TABLE [dbo].[RouteOperations] (
    [ID]          INT      IDENTITY (1, 1) NOT NULL,
    [RouteID]     INT      NOT NULL,
    [OperationID] INT      NOT NULL,
    [Sequence]    INT      NULL,
    [CreatedBy]   INT      NULL,
    [CreatedOn]   DATETIME NULL,
    [ModifiedBy]  INT      NULL,
    [ModifiedOn]  DATETIME NULL,
    CONSTRAINT [PK_RouteOperations] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_RouteOperations_OperationsMaster] FOREIGN KEY ([OperationID]) REFERENCES [dbo].[OperationsMaster] ([ID]),
    CONSTRAINT [FK_RouteOperations_RouteMaster] FOREIGN KEY ([RouteID]) REFERENCES [dbo].[RouteMaster] ([ID]),
    CONSTRAINT [IX_RouteOperations] UNIQUE NONCLUSTERED ([OperationID] ASC, [RouteID] ASC)
);

