﻿CREATE TABLE [dbo].[ItemBOM] (
    [ID]          INT           IDENTITY (1, 1) NOT NULL,
    [BOMHeaderID] INT           NOT NULL,
    [ItemCode]    VARCHAR (20)  NOT NULL,
    [ItemName]    VARCHAR (500) NOT NULL,
    [CreatedBy]   INT           NULL,
    [CreatedOn]   DATETIME      NULL,
    [ModifiedBy]  INT           NULL,
    [ModifiedOn]  DATETIME      NULL,
    CONSTRAINT [PK_ItemBOM] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_ItemBOM_BOMHeader] FOREIGN KEY ([BOMHeaderID]) REFERENCES [dbo].[BOMHeader] ([ID])
);






GO
CREATE NONCLUSTERED INDEX [IX_ItemBOM]
    ON [dbo].[ItemBOM]([BOMHeaderID] ASC, [ItemCode] ASC);

