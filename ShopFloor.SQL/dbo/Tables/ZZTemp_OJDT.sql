﻿CREATE TABLE [dbo].[ZZTemp_OJDT] (
    [ID]          INT           IDENTITY (1, 1) NOT NULL,
    [JDT_Num]     VARCHAR (10)  NULL,
    [RefDate]     DATE          NULL,
    [Memo]        VARCHAR (100) NULL,
    [Ref1]        VARCHAR (100) NULL,
    [Ref2]        VARCHAR (100) NULL,
    [TransCode]   VARCHAR (4)   NULL,
    [Project]     VARCHAR (8)   NULL,
    [Taxdate]     DATE          NULL,
    [Indicator]   VARCHAR (2)   NULL,
    [AutoStoreno] VARCHAR (3)   NULL,
    [StornoDate]  DATE          NULL,
    [VatDate]     DATE          NULL,
    [Series]      VARCHAR (2)   NULL,
    [StampTax]    VARCHAR (3)   NULL,
    [AutoVat]     VARCHAR (3)   NULL,
    [ReportEU]    VARCHAR (3)   NULL,
    [Report347]   VARCHAR (3)   NULL,
    [Location]    VARCHAR (3)   NULL,
    [CreatedBy]   VARCHAR (11)  NULL,
    [BlockDunn]   VARCHAR (3)   NULL,
    [AutoWT]      VARCHAR (3)   NULL,
    [Corisptivi]  VARCHAR (3)   NULL,
    [MOTypeCode]  VARCHAR (1)   NULL,
    [MONumber]    INT           NULL,
    [Combi]       VARCHAR (3)   NULL,
    CONSTRAINT [PK_ZZTemp_OJDT] PRIMARY KEY CLUSTERED ([ID] ASC)
);



