﻿
CREATE PROCEDURE usp_JEProcessing
    @From	datetime, 
    @To		datetime 
AS 
BEGIN


-----------======  For Creating ZZTemp_OJDT (JE Header)
delete from ZZTemp_OJDT
------ then save OJDT Data to this temporary table.
insert into ZZTemp_OJDT 
select 
ROW_NUMBER() over (order by month(SHF.EndDate), AMO.MOTypeCode, AMO.MONumber) JDT_NUM,
GETDATE() as 'RefDate', 
DateName(month,DateAdd(month,month(SHF.EndDate),0)- 1) + ' ' + rtrim(cast(YEAR(SHF.EndDate) as CHAR)) + ' MO# ' + MOTypeCode + '-' 
+ cast(AMO.MONumber as CHAR) as 'Memo',
CHAR(0) as 'Ref1', CHAR(0) as 'Ref2', CHAR(0) as 'TransCode', CHAR(0) as 'Project', GETDATE() as 'Taxdate',
null as 'Indicator', 'tNO' as 'AutoStoreno', null as 'StornoDate', null as 'VatDate', '17' as 'Series', 'tNO' as StampTax,
'tNO' as 'AutoVat', 'tNO' as 'ReportEU', 'tNO' as 'Report347', null as 'Location', null as 'CreatedBy', 'tNO' as 'BlockDunn',
'tNO' as 'AutoWT', 'tNO' as 'Corisptivi',
AMO.MOTypeCode, AMO.MONumber,  
ltrim(rtrim(cast(ACT.CompJETag as char)))+ ltrim(rtrim(cast(ACT.IsProductive as char))) + 
ltrim(rtrim(cast(0 /*ACT.DownTimeTag */ as char))) as 'Combi'
from ShopFloor SHF 
left join Z001MODetailRSCAll AMO on SHF.MODetailRSCID = AMO.ID
left join ActivityTypes ACT on SHF.ActivityTypeID = ACT.ID
where (ACT.CompJETag = 1 and SHF.Duration <> 0)
 AND SHF.EndDate >= @From AND SHF.EndDate <= @To
group by month(SHF.EndDate), 
DateName(month,DateAdd(month,month(SHF.EndDate),0)- 1) + ' ' + rtrim(cast(YEAR(SHF.EndDate) as CHAR)), 
AMO.MOTypeCode, AMO.MONumber, ltrim(rtrim(cast(ACT.CompJETag as char)))+ 
ltrim(rtrim(cast(ACT.IsProductive as char))) + ltrim(rtrim(cast(0 /*ACT.DownTimeTag*/ as char)))
order by month(SHF.EndDate), AMO.MOTypeCode, AMO.MONumber
update ZZTemp_OJDT set Memo = 
case when Combi = '110' then 'Labor and Overhead Allocation ftm '
    else 'Downtime Cost ftm '
end + Memo
-----------======  End updating ZZTemp_OJDT

-----------======  For Creating ZZTemp_JDT1 (JE Detail (Debit and Credit))
delete from ZZTemp_JDT1
--------============  Insert data to ZZTemp_JDT1 - Debit
insert into ZZTemp_JDT1 (JdtNum, LineNum, Line_ID, Account, Debit, Credit, FCDebit, FCCredit, 
FCCurrency, DueDate, ShortName, ContraAct, LineMemo, RefDate, Ref2Date, Ref1, Ref2, Project,
ProfitCode, TaxDate, BaseSum, VatGroup, SYSDeb, SYSCred, VatDate, VatLine, SYSBaseSum,
VatAmount, SYSVatSum, GrossValue, Ref3Line, OcrCode2, OcrCode3, OcrCode4, TaxCode, TaxPostAcc, 
OcrCode5, Location, ControlAccount, WTLiable, WTLine, PayBlock, PayBlckRef, MOType, MONumber, Combi)
select NULL, 0, 0, ACF.AcctCode, 
sum(((datediff(SECOND, DATEADD(D, 0, DATEDIFF(D, 0, SHF.StartDate)) + STM.ValidTime,
DATEADD(D, 0, DATEDIFF(D, 0, SHF.EndDate)) + ST2.ValidTime))/3600.0) * RSC.ResourceRate),0,
sum(((datediff(SECOND, DATEADD(D, 0, DATEDIFF(D, 0, SHF.StartDate)) + STM.ValidTime, 
DATEADD(D, 0, DATEDIFF(D, 0, SHF.EndDate)) + ST2.ValidTime))/3600.0) * RSC.ResourceRate), 0,
NULL, getdate(), ACF.AcctCode, ACF.ContraAcctCode, 
'L/O ftm ' + DateName(month,DateAdd(month,month(SHF.EndDate),0)- 1) + ' ' + 
rtrim(cast(YEAR(SHF.EndDate) as CHAR)) + ' MO# ' + MOTypeCode + '-' + cast(AMO.MONumber as CHAR), 
getdate(), NULL, '', '', '', '', 
DATEADD(month, ((YEAR(GETDATE()) - 1900) * 12) + MONTH(getdate()), -1), 0, NULL,
sum(((datediff(SECOND, DATEADD(D, 0, DATEDIFF(D, 0, SHF.StartDate)) + STM.ValidTime, 
DATEADD(D, 0, DATEDIFF(D, 0, SHF.EndDate)) + ST2.ValidTime))/3600.0) * RSC.ResourceRate), 0,
NULL, 'N', 0, 0, 0, 0, NULL, NULL, 
NULL, NULL, NULL, 'N', NULL, NULL, ACF.ContraAcctCode, 'N', 
'N', 'N', NULL, AMO.MOTypeCode, AMO.MONumber, 
ltrim(rtrim(cast(ACT.CompJETag as char)))+ ltrim(rtrim(cast(ACT.IsProductive as char))) + 
ltrim(rtrim(cast(0 /*ACT.DownTimeTag*/ as char)))
from ShopFloor SHF 
left join Z001MODetailRSCAll AMO on SHF.MODetailRSCID = AMO.ID
left join SAP_DEV.dbo.OITM SPI on AMO.ItemCode collate SQL_Latin1_General_CP850_CI_AS = SPI.ItemCode
left join ActivityTypes ACT on SHF.ActivityTypeID = ACT.ID
left join AcctFinder ACF on AMO.MOTypeCode = ACF.MOType and SPI.ItmsGrpCod = ACF.ItmsGrpCode and ACT.IsProductive = ACF.DBCDTag
left join ResourceMaster RSC on AMO.ResourceCode = RSC.ResourceCode
left join StandardTime STM on cast(SHF.StartDate as Time) >= STM.TimeFrm and cast(SHF.StartDate as Time) <= STM.TimeTo 
left join StandardTime2 ST2 on cast(SHF.EndDate as Time) >= ST2.TimeFrm and cast(SHF.EndDate as Time) <= ST2.TimeTo 
where ACT.CompJETag = 1 and SHF.Duration <> 0
 AND SHF.EndDate >= @From AND SHF.EndDate <= @To
group by AMO.MOTypeCode, AMO.MONumber, ACF.AcctCode, ACF.ContraAcctCode, 
'L/O ftm ' + DateName(month,DateAdd(month,month(SHF.EndDate),0)- 1) + ' ' + 
rtrim(cast(YEAR(SHF.EndDate) as CHAR)) + ' MO# ' + MOTypeCode + '-' + cast(AMO.MONumber as CHAR), SHF.ActivityTypeID,
ltrim(rtrim(cast(ACT.CompJETag as char)))+ ltrim(rtrim(cast(ACT.IsProductive as char))) + 
ltrim(rtrim(cast(0 /*ACT.DownTimeTag*/ as char)))
order by AMO.MOTypeCode, AMO.MONumber, ACF.AcctCode, ACF.ContraAcctCode, 
'L/O ftm ' + DateName(month,DateAdd(month,month(SHF.EndDate),0)- 1) + ' ' + 
rtrim(cast(YEAR(SHF.EndDate) as CHAR)) + ' MO# ' + MOTypeCode + '-' + cast(AMO.MONumber as CHAR), SHF.ActivityTypeID,
ltrim(rtrim(cast(ACT.CompJETag as char)))+ ltrim(rtrim(cast(ACT.IsProductive as char))) + 
ltrim(rtrim(cast(0 /*ACT.DownTimeTag*/ as char))) --as 'Combi'
--------============  End of insert at ZZTemp_JDT1 - Debit only

--------============  Insert data to ZZTemp_JDT1 - Credit    
insert into ZZTemp_JDT1 (JdtNum, LineNum, Line_ID, Account, Debit, Credit, FCDebit, FCCredit, 
FCCurrency, DueDate, ShortName, ContraAct, LineMemo, RefDate, Ref2Date, Ref1, Ref2, Project,
ProfitCode, TaxDate, BaseSum, VatGroup, SYSDeb, SYSCred, VatDate, VatLine, SYSBaseSum,
VatAmount, SYSVatSum, GrossValue, Ref3Line, OcrCode2, OcrCode3, OcrCode4, TaxCode, TaxPostAcc, 
OcrCode5, Location, ControlAccount, WTLiable, WTLine, PayBlock, PayBlckRef, MOType, MONumber, Combi)
select NULL, 0, 0, ACF.ContraAcctCode, 
0, sum(((datediff(SECOND, DATEADD(D, 0, DATEDIFF(D, 0, SHF.StartDate)) + STM.ValidTime,
DATEADD(D, 0, DATEDIFF(D, 0, SHF.EndDate)) + ST2.ValidTime))/3600.0) * RSC.ResourceRate),
0, sum(((datediff(SECOND, DATEADD(D, 0, DATEDIFF(D, 0, SHF.StartDate)) + STM.ValidTime, 
DATEADD(D, 0, DATEDIFF(D, 0, SHF.EndDate)) + ST2.ValidTime))/3600.0) * RSC.ResourceRate), 
NULL, getdate(), ACF.ContraAcctCode, ACF.AcctCode, 
'L/O ftm ' + DateName(month,DateAdd(month,month(SHF.EndDate),0)- 1) + ' ' + 
rtrim(cast(YEAR(SHF.EndDate) as CHAR)) + ' MO# ' + MOTypeCode + '-' + cast(AMO.MONumber as CHAR), 
getdate(), NULL, '', '', '', '', 
DATEADD(month, ((YEAR(GETDATE()) - 1900) * 12) + MONTH(getdate()), -1), 0, NULL,
0, sum(((datediff(SECOND, DATEADD(D, 0, DATEDIFF(D, 0, SHF.StartDate)) + STM.ValidTime, 
DATEADD(D, 0, DATEDIFF(D, 0, SHF.EndDate)) + ST2.ValidTime))/3600.0) * RSC.ResourceRate), 
NULL, 'N', 0, 0, 0, 0, NULL, NULL, 
NULL, NULL, NULL, 'N', NULL, NULL, ACF.AcctCode, 'N', 
'N', 'N', NULL, AMO.MOTypeCode, AMO.MONumber, 
ltrim(rtrim(cast(ACT.CompJETag as char)))+ ltrim(rtrim(cast(ACT.IsProductive as char))) + 
ltrim(rtrim(cast(0 /*ACT.DownTimeTag*/ as char)))
from ShopFloor SHF 
left join Z001MODetailRSCAll AMO on SHF.MODetailRSCID = AMO.ID
left join SAP_DEV.dbo.OITM SPI on AMO.ItemCode collate SQL_Latin1_General_CP850_CI_AS = SPI.ItemCode
left join ActivityTypes ACT on SHF.ActivityTypeID = ACT.ID
left join AcctFinder ACF on AMO.MOTypeCode = ACF.MOType and SPI.ItmsGrpCod = ACF.ItmsGrpCode and ACT.IsProductive = ACF.DBCDTag
left join ResourceMaster RSC on AMO.ResourceCode = RSC.ResourceCode
left join StandardTime STM on cast(SHF.StartDate as Time) >= STM.TimeFrm and cast(SHF.StartDate as Time) <= STM.TimeTo 
left join StandardTime2 ST2 on cast(SHF.EndDate as Time) >= ST2.TimeFrm and cast(SHF.EndDate as Time) <= ST2.TimeTo 
where ACT.CompJETag = 1 and SHF.Duration <> 0
 AND SHF.EndDate >= @From AND SHF.EndDate <= @To
group by AMO.MOTypeCode, AMO.MONumber, ACF.AcctCode, ACF.ContraAcctCode, 
'L/O ftm ' + DateName(month,DateAdd(month,month(SHF.EndDate),0)- 1) + ' ' + 
rtrim(cast(YEAR(SHF.EndDate) as CHAR)) + ' MO# ' + MOTypeCode + '-' + cast(AMO.MONumber as CHAR), SHF.ActivityTypeID,
ltrim(rtrim(cast(ACT.CompJETag as char)))+ ltrim(rtrim(cast(ACT.IsProductive as char))) + 
ltrim(rtrim(cast(0 /*ACT.DownTimeTag*/ as char)))
order by AMO.MOTypeCode, AMO.MONumber, ACF.AcctCode, ACF.ContraAcctCode, 
'L/O ftm ' + DateName(month,DateAdd(month,month(SHF.EndDate),0)- 1) + ' ' + 
rtrim(cast(YEAR(SHF.EndDate) as CHAR)) + ' MO# ' + MOTypeCode + '-' + cast(AMO.MONumber as CHAR), SHF.ActivityTypeID,
ltrim(rtrim(cast(ACT.CompJETag as char)))+ ltrim(rtrim(cast(ACT.IsProductive as char))) + 
ltrim(rtrim(cast(0 /*ACT.DownTimeTag*/ as char)))
--------============  End of insert at ZZTemp_JDT1 - Credit only

--------============  For getting JdtNum and Detail Line Numbers
delete from ZZTemp_JDT1LiNo
--------============ Insert JDT_Num, LineNum, Line_ID at ZZTemp_JDT1 --- Ask Monir if he can improve this.
----Get JdtNum first......
update ZZTemp_JDT1 
set JdtNum = ZZTemp_OJDT.JDT_Num
from ZZTemp_OJDT
where ZZTemp_JDT1.MOType =  ZZTemp_OJDT.MOTypeCode and ZZTemp_JDT1.MONumber =  ZZTemp_OJDT.MONumber
and ZZTemp_JDT1.Combi =  ZZTemp_OJDT.Combi

----- insert the Detail Line Number and References and into the temporary table
insert into ZZTemp_JDT1LiNo -- (PriKey, Description)
select JdtNum, (ROW_NUMBER() OVER(PARTITION BY JdtNum ORDER BY LineNum))-1 as LineNumberLT, Account FROM ZZTemp_JDT1

----- now update ZZTemp_JDT1 table using data from the temporary table
update ZZTemp_JDT1 set LineNum = ZZTemp_JDT1LiNo.ZLinenum, Line_ID = ZZTemp_JDT1LiNo.ZLinenum
from ZZTemp_JDT1LiNo where JdtNum = ZJdtNum and Account = ZAccount
---- HA!

--select * from ZZTemp_JDT1 order by MOType, MONumber
--select * from ZZTemp_JDT1LiNo
--select * from ZZTemp_OJDT
--select * from ZZTemp_JDT1 order by JdtNum, LineNum


END