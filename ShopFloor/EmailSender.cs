﻿using System.Net.Mail;
using System.Net;

namespace ShopFloor
{
    /// <summary>
    /// Currently unused.
    /// </summary>
    public class EmailSender
    {
        static void Send(string to, string body, string subject)
        {
            //TODO: refactor!
            var fromAddress = new MailAddress("shopfloor.emailsender@gmail.com");
            var toAddress = new MailAddress(to);
            const string fromPassword = "shopfloor@2015";

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };
            using (var message = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body
            })
            {
                smtp.Send(message);
            }
        }
    }
}




