﻿using ShopFloor.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Business
{
	public class CalculatorFactory
	{
		public static IMOCalculator Create(MODetailItm itm)
		{
			if (itm.MODetailFgg == null)
				throw new ArgumentException("MODetailFGG is null");
			if(itm.MODetailFgg.MOHeader == null)
				throw new ArgumentException("MODetailFGG.MOHeader is null");

			MODetailFgg fgg = itm.MODetailFgg;
			MOHeader mo = fgg.MOHeader;

			if (itm.ItmsGrpCod == 103)
			{
				if (mo.GangRunTag.GetValueOrDefault())
				{
					return new Group103GangRunCalculator(itm, mo);
				}

				return new Group103NonGangRunCalculator(itm, fgg);
			}

			return new RegularMOCalculator(itm, fgg);
		}
	}


	public interface IMOCalculator
	{
		decimal ComputeMOQty(decimal rMQty, decimal uOMFactor, decimal allowance, decimal rmFactor);
	}

	public class RegularMOCalculator : IMOCalculator
	{
		private MODetailItm _itm;
		private MODetailFgg _fgg;
		public RegularMOCalculator(MODetailItm itm, MODetailFgg fgg)
		{
			_itm = itm;
			_fgg = fgg;
		}


		public decimal ComputeMOQty(decimal rMQty, decimal uOMFactor, decimal allowance, decimal rmFactor)
		{
			decimal batchQty = (decimal)_fgg.BatchQty.GetValueOrDefault(0);
			return (batchQty * rMQty * rmFactor * uOMFactor) + allowance;
		}
	}

	public class Group103GangRunCalculator : IMOCalculator
	{
		private MODetailItm _itm;
		private MOHeader _mo;
		public Group103GangRunCalculator(MODetailItm itm, MOHeader mo)
		{
			_itm = itm;
			_mo = mo;
		}


		public decimal ComputeMOQty(decimal rMQty, decimal uOMFactor, decimal allowance, decimal rmFactor)
		{
			decimal batchQty = _mo.FinishedGoods.Max(m => (decimal) m.BatchQty * (Decimal.Divide(1, (decimal) m.OutsTag)));
			var fgg = _itm.MODetailFgg;
			var moQty = (decimal)(batchQty * rMQty * uOMFactor) + allowance;
			return moQty;
		}
	}


	public class Group103NonGangRunCalculator : IMOCalculator
	{
		private MODetailItm _itm;
		private MODetailFgg _fgg;
		public Group103NonGangRunCalculator(MODetailItm itm, MODetailFgg fgg)
		{
			_itm = itm;
			_fgg = fgg;
		}

		public decimal ComputeMOQty(decimal rMQty, decimal uOMFactor, decimal allowance, decimal rmFactor)
		{
			var fgg = _itm.MODetailFgg;
			decimal outsDivision = Decimal.Divide(1, (decimal)fgg.OutsTag);
			var moQty = (decimal)((decimal)fgg.BatchQty.GetValueOrDefault() * outsDivision * rMQty * uOMFactor) + allowance;
			return moQty;
		}
	}

}
