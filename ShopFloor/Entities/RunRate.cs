﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Entities
{
    partial class RunRate
    {
        [ForeignKey("ResourceID")]
        public Resource Resource { get; set; }
        [ForeignKey("ItemRouteID")]
        public ItemRoute ItemRoute { get; set; }
    }
}
