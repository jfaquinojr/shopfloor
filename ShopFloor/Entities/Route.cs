﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Entities
{
    [MetadataType(typeof(RouteMetaData))]
    public partial class Route
    {
        public List<ItemRoute> ItemRoutes = new List<ItemRoute>();

        class RouteMetaData
        {
#pragma warning disable 0649
            [Display(Name = "Route Code")]
            public string RouteCode;

            [Display(Name = "Route Description")]
            public string RouteDesc;

            [Display(Name = "SBU Code")]
            public string SBUCode;
#pragma warning restore 0649
        }
    }
}
