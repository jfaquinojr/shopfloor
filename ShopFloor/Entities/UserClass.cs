﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Entities
{
    [MetadataType(typeof(UserClassMetaData))]
    public partial class UserClass
    {
        public List<UserUserClass> UserUserClasses { get; set; }

        //public string UClassCode;
        class UserClassMetaData
        {
#pragma warning disable 0649

            [Display(Name = "Code")]
            public string UClassCode;

            [Display(Name = "Description")]
            public string UClassDesc;
#pragma warning restore 0649
        }
    }
}
