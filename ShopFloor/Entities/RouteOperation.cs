﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Entities
{
    [MetadataType(typeof(RouteOperationMetaData))]
    public partial class RouteOperation
    {
        public virtual Operation Operation { get; set; }
        public virtual Route Route { get; set; }

        [NotMapped]
        [Display(Name="Route Operation")]
        public string DisplayName
        {
            get
            {
                return this.Route?.RouteCode + "-" + this.Operation?.OperCode;
            }
        }

        class RouteOperationMetaData
        {
#pragma warning disable 0649
            [Required]
            [Display(Name="Operation")]
            public int OperationID;

            [Display(Name = "Route")]
            public string RouteID;
#pragma warning restore 0649
        }
    }
}
