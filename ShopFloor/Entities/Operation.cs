﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Entities
{
    [MetadataType(typeof(OperationMetadata))]
    public partial class Operation
    {

        public class OperationMetadata
        {
            [Display(Name="Operation Code")]
            public string OperCode { get; set; }
            [Display(Name = "Description")]
            public string OperDesc { get; set; }
            public int Sequence { get; set; }
        }
    }
}
