﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Entities
{
    public partial class SubsResource
    {
        [ForeignKey("ResourceID")]
        public Resource Parent { get; set; }
        [ForeignKey("SubResourceID")]
        public Resource Subresource { get; set; }
    }
}
