﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Entities
{
    public partial class QAParamResource
    {
        [ForeignKey("ResourceID")]
        public Resource Resource { get; set; }

        [ForeignKey("QAParamID")]
        public QAParam QAParam { get; set; }
    }
}
