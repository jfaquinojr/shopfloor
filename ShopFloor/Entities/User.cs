﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;

namespace ShopFloor.Entities
{
    public partial class User
    {
        public List<UserUserClass> UserUserClasses { get; set; }
    }
}
