﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Entities
{
    
    [MetadataType(typeof(ResourceMetadata))]
    public partial class Resource
    {
        public virtual ResourceCategory ResourceCategory { get; set; }


        public class ResourceMetadata
        {
            [Display(Name="Resource Code")]
            public string ResourceCode;

            [Display(Name="Description")]
            public string ResourceDesc;

            [Display(Name = "Resource Standard")]
			[DisplayFormat(DataFormatString = "{0:F6}", ApplyFormatInEditMode = true)]
			public double ResourceRate;
            [Display(Name = "Cost Center")]
            public string PrcCode { get; set; }
            [Display(Name = "Category")]
            public int ResourceCategoryID { get; set; }
            [Display(Name = "Setup Time")]
			[DisplayFormat(DataFormatString = "{0:F6}", ApplyFormatInEditMode = true)]
			public double SetupTime { get; set; }
            [Display(Name = "Stock Time")]
			[DisplayFormat(DataFormatString = "{0:F6}", ApplyFormatInEditMode = true)]
			public double StockTime { get; set; }
            [Display(Name = "OnStream")]
            public bool OnStreamTag { get; set; }
            public DateTime FromDate { get; set; }
            public DateTime ToDate { get; set; }
            [Display(Name = "Pieces Tag")]
            public bool UnitTag { get; set; }
			[Display(Name = "Cost per Hour")]
			[DisplayFormat(DataFormatString = "{0:F6}", ApplyFormatInEditMode = true)]
			public double CostRate { get; set; }
		}

    }
}
