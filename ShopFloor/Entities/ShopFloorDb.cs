﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Entities
{
    public partial class ShopFloorDb : DbContext
    {
        public Action<DbModelBuilder> BeforeModelCreating;

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            
            SetupPrecision(modelBuilder);
            SetupPrecision2(modelBuilder);

            base.OnModelCreating(modelBuilder);
        }

        protected void SetupPrecision(DbModelBuilder modelBuilder)
        {
            // http://stackoverflow.com/a/7769162/578334
            modelBuilder.Entity<Resource>().Property(m => m.ResourceRate).HasPrecision(19, 6);
            modelBuilder.Entity<Resource>().Property(m => m.SetupTime).HasPrecision(19, 6);
            modelBuilder.Entity<Resource>().Property(m => m.StockTime).HasPrecision(19, 6);
            modelBuilder.Entity<Resource>().Property(m => m.CostRate).HasPrecision(19, 6);

            modelBuilder.Entity<MODetailRsc>().Property(m => m.SetupTime).HasPrecision(19, 6);
            modelBuilder.Entity<MODetailRsc>().Property(m => m.StockTime).HasPrecision(19, 6);
            modelBuilder.Entity<MODetailRsc>().Property(m => m.EstTime).HasPrecision(19, 6);
            modelBuilder.Entity<MODetailRsc>().Property(m => m.OrigEstTime).HasPrecision(19, 6);

            modelBuilder.Entity<MODetailItm>().Property(m => m.MOQty).HasPrecision(19, 6);
            modelBuilder.Entity<MODetailItm>().Property(m => m.IssuedQty).HasPrecision(19, 6);
            modelBuilder.Entity<MODetailItm>().Property(m => m.IssuedQty2).HasPrecision(19, 6);
            modelBuilder.Entity<MODetailItm>().Property(m => m.SAPUnitCost).HasPrecision(19, 6);
            modelBuilder.Entity<MODetailItm>().Property(m => m.RMFactor).HasPrecision(19, 12);
            modelBuilder.Entity<MODetailItm>().Property(m => m.UOMFactor).HasPrecision(19, 12);
            modelBuilder.Entity<MODetailItm>().Property(m => m.Allowance).HasPrecision(19, 12);

            modelBuilder.Entity<BillOfMaterialDetail>().Property(m => m.RMQuantity).HasPrecision(19, 12);
            modelBuilder.Entity<BillOfMaterialDetail>().Property(m => m.RMFactor).HasPrecision(19, 12);
            modelBuilder.Entity<BillOfMaterialDetail>().Property(m => m.UOMFactor).HasPrecision(19, 12);
            modelBuilder.Entity<BillOfMaterialDetail>().Property(m => m.Allowance).HasPrecision(19, 12);
            modelBuilder.Entity<BillOfMaterialDetail>().Property(m => m.UnitCost).HasPrecision(19, 12);
            modelBuilder.Entity<BillOfMaterialDetail>().Property(m => m.SAPUnitCost).HasPrecision(19, 12);

            modelBuilder.Entity<BOMDetailSub>().Property(m => m.RMQuantity).HasPrecision(19, 12);
            modelBuilder.Entity<BOMDetailSub>().Property(m => m.RMFactor).HasPrecision(19, 12);
            modelBuilder.Entity<BOMDetailSub>().Property(m => m.UOMFactor).HasPrecision(19, 12);
            modelBuilder.Entity<BOMDetailSub>().Property(m => m.Allowance).HasPrecision(19, 12);
            modelBuilder.Entity<BOMDetailSub>().Property(m => m.UnitCost).HasPrecision(19, 12);

            modelBuilder.Entity<ShopFloorDataCapture>().Property(m => m.Duration).HasPrecision(19, 6);
            modelBuilder.Entity<ShopFloorDataCapture>().Property(m => m.Good).HasPrecision(19, 6);

            modelBuilder.Entity<ShopFloorRej>().Property(m => m.Reject).HasPrecision(19, 6);

            modelBuilder.Entity<ShopFloorAddl>().Property(m => m.AddlQty).HasPrecision(19, 6);

            modelBuilder.Entity<QAParam>().Property(m => m.QAParaValueNum).HasPrecision(19, 6);
            modelBuilder.Entity<QAParam>().Property(m => m.QAParaValueNum2).HasPrecision(19, 6);



        }

        protected virtual void SetupPrecision2(DbModelBuilder modelBuilder)
        {

        }
    }
}
