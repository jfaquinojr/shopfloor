﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Entities
{
    public partial class ShopFloorRej
    {
        [ForeignKey("QACriteriaID")]
        public virtual QACriterion QACriterion { get; set; }
    }
}
