﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Entities
{
	[MetadataType(typeof(QAParamMetaData))]
	public partial class QAParam
	{
		class QAParamMetaData
		{
			[Display(Name = "Value 1")]
			[DisplayFormat(DataFormatString = "{0:F6}", ApplyFormatInEditMode = true)]
			public decimal? QAParaValueNum { get; set; }

			[Display(Name = "Value 2")]
			[DisplayFormat(DataFormatString = "{0:F6}", ApplyFormatInEditMode = true)]
			public decimal QAParaValueNum2 { get; set; }
		}
	}
}
