﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ShopFloor.Entities
{
    public partial class BillOfMaterialDetail
    {
        [ForeignKey("BOMHeaderID")]
        public BillOfMaterial BillOfMaterial { get; set; }

        private const string FORMAT_TENDECIMAL = "###,###.0000000000";

        public string UoMFactorDisplay { get { return this.UOMFactor.GetValueOrDefault(0).ToString(FORMAT_TENDECIMAL); } }
        public string RMFactorDisplay { get { return this.RMFactor.GetValueOrDefault(0).ToString(FORMAT_TENDECIMAL); } }
        public string RMQuantityDisplay { get { return this.RMQuantity.ToString(FORMAT_TENDECIMAL); } }
        public string AllowanceDisplay { get { return this.Allowance.GetValueOrDefault(0).ToString(FORMAT_TENDECIMAL); } }
    }
}
