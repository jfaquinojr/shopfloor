﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Entities
{
	public partial class MOType
	{
		public const string CONST_CODES_MO = "M";
		public const string CONST_CODES_ScheduledRepair = "R";
		public const string CONST_CODES_Filler = "F";
		public const string CONST_CODES_Sheeting = "S";
		public const string CONST_CODES_PrePress = "P";
		public const string CONST_CODES_ProductDevelopment = "D";
	}
}
