﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShopFloor.Entities
{
    public partial class ShopFloorDataCapture
    {
        //public MODetailRsc MODetailRsc { get; set; }

        public List<ShopFloorAddl> ShopFloorAddls { get; set; }

        [NotMapped]
        public string Username { get; set; }

        [NotMapped]
        public string Status { get; set; }

        [NotMapped]
        public ActivityType ActivityType { get; set; }

        [NotMapped]
        public string DisplayStartDate
        {
            get
            {
                return this.StartDate.GetValueOrDefault(DateTime.MinValue).ToString(Constants.CONST_FORMAT_DATETIME);
            }
        }

        [NotMapped]
        public string DisplayEndDate
        {
            get
            {
                string ret = "";
                if (this.EndDate.HasValue)
                {
                    ret = this.EndDate.GetValueOrDefault(DateTime.MinValue).ToString(Constants.CONST_FORMAT_DATETIME);
                }
                return ret;
            }
        }

        [NotMapped]
        public string DisplayElapsedTime
        {
            get
            {
                string ret = "";
				TimeSpan ts;
                if (Status?.ToLower() != "started")
                {
					ts = (this.EndDate ?? DateTime.MinValue) - (this.StartDate ?? DateTime.MinValue);
                }
                else
                {
					ts = DateTime.Now - (this.StartDate ?? DateTime.MinValue);
                }

				// show days?
				if(ts.Days > 0)
				{
					ret = string.Format("{0:d\\.hh\\:mm\\:ss}", ts);
				}
				else
				{
					ret = string.Format("{0:hh\\:mm\\:ss}", ts);
				}
                return ret;
            }
        }


    }
}
