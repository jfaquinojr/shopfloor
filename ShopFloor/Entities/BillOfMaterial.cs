﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ShopFloor.Entities
{
	public partial class BillOfMaterial
	{
		public List<BillOfMaterialDetail> BOMDetails { get; set; } = new List<BillOfMaterialDetail>();
		public List<ItemBom> ItemBOMs { get; set; } = new List<ItemBom>();

		[Display(Name = "BOM Type")]
		public string BOMTypeName
		{
			get
			{
				switch (this.BOMType)
				{
					case 1:
						return "Estimation";
					case 2:
						return "Product Devt";
					case 3:
						return "Commercial";
					default:
						return "Unknown";
				}
			}
		}
	}
}
