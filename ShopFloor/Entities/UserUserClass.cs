﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Entities
{
    public partial class UserUserClass
    {
        [ForeignKey("UserID")]
        public User User { get; set; }
        [ForeignKey("UserClassID")]
        public UserClass UserClass { get; set; }
    }
}
