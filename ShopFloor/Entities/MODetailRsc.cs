﻿using ShopFloor.Schedule;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShopFloor.Entities
{
    public partial class MODetailRsc : ISchedule
    {
        #region ISchedule
        [NotMapped]
        public int ScheduleID
        {
            get
            {
                return this.ResourceID ?? 0;
            }
        }

        [NotMapped]
        public int GroupID
        {
            get
            {
                return this.MODetailFGGID;
            }
        }

        [NotMapped]
        public double AddHours
        {
            get
            {
                var hours = (double)((this.EstTime ?? 0) + (this.SetupTime ?? 0) + (this.StockTime ?? 0));
                return hours;
            }
        }

        [NotMapped]
        public DateTime StartDate
        {
            get
            {
                return this.FromDate ?? DateTime.MinValue;
            }

            set
            {
                this.FromDate = value;
            }
        }

        [NotMapped]
        public DateTime EndDate
        {
            get
            {
                return this.ToDate ?? DateTime.MinValue;
            }

            set
            {
                this.ToDate = value;
            }
        }

        #endregion


        public MODetailFgg MODetailFgg { get; set; } 


        public override string ToString()
        {
            return $"ID: {ScheduleID}, Group: {GroupID}, From {StartDate} To {EndDate}";
        }

    }
}
