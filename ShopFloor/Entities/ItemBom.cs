﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Entities
{
    public partial class ItemBom
    {
        [ForeignKey("BOMHeaderID")]
        public BillOfMaterial BillOfMaterial { get; set; }
    }
}
