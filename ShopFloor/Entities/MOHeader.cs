﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Entities
{
    public partial class MOHeader
    {
        public MOType MOType { get; set; }
        public virtual List<MODetailFgg> FinishedGoods { get; set; }

    }
}
