﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShopFloor.Entities
{
    public partial class MODetailFgg
    {
        public virtual List<MODetailItm> RawMaterials { get; set; } = new List<MODetailItm>();
        public virtual List<MODetailRsc> Resources { get; set; }  = new List<MODetailRsc>();

		////public virtual Route Route { get; set; }
		//[ForeignKey("ItemRouteID")]
		//public ItemRoute ItemRoute { get; set; }

		public MOHeader MOHeader { get; set; }

        [NotMapped]
        public bool IsClosed
        {
            get
            {
                bool closed = true;
                foreach(var rsc in Resources)
                {
                    if(!rsc.ClosedTag ?? false)
                    {
                        // if even one rsc is not closed, whole FGG is not
                        closed = false;
                        break;
                    }
                }
                return closed;
            }
        }

        [NotMapped]
        public Transmittal Transmittal { get; set; }

        [NotMapped]
        public decimal TotalGoods { get; set; }
    }
}
