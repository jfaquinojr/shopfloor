﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShopFloor.Entities
{
	public partial class BOMDetailSub
	{
		[ForeignKey("BOMDetailID")]
		public BillOfMaterialDetail BOMDetail { get; set; }

		[NotMapped]
		public int BOMHeaderID { get; set; }

	}
}
