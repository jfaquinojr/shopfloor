﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Entities
{
    [MetadataType(typeof(RouteOperationResourceMetaData))]
    public partial class RouteOperationResource
    {
        public virtual Resource Resource { get; set; }

        public virtual RouteOperation RouteOperation { get; set; }

        class RouteOperationResourceMetaData
        {
#pragma warning disable 0649
            [Display(Name = "Route Operation")]
            public int RouteOperationID;

            [Display(Name = "Resource")]
            public string ResourceID;
#pragma warning restore 0649
        }
    }
}
