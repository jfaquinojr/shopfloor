﻿using ShopFloor.Schedule;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Entities
{
    public partial class MOResourceSchedule : ISchedule
    {
        public MODetailRsc MODetailRsc { get; set; }


        #region ISchedule

        [NotMapped]
        public double AddHours
        {
            get
            {
                return 0;
            }
        }

        [NotMapped]
        public DateTime EndDate
        {
            get
            {
                return this.EndDateTime ?? DateTime.MinValue;
            }

            set
            {
                this.EndDateTime = value;
            }
        }

        [NotMapped]
        public int GroupID
        {
            get
            {
                return this.MODetailRsc?.MODetailFGGID ?? 0;
            }
        }

        [NotMapped]
        public int ScheduleID
        {
            get
            {
                return this.ResourceID;
            }
        }

        [NotMapped]
        public DateTime StartDate
        {
            get
            {
                return this.StartDateTime ?? DateTime.MinValue;
            }

            set
            {
                this.StartDateTime = value;
            }
        }

        #endregion

        public override string ToString()
        {
            return $"ID: {ScheduleID}, Group: {GroupID}, From {StartDate} To {EndDate}";
        }
    }
}
