﻿using ShopFloor.Business;
using ShopFloor.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Managers
{
	public class MODetailITMManager : ShopFloorEntityManager<MODetailItm>
	{
		public MODetailITMManager() : base(m => m.ID)
		{

		}

		public void CreateFromItemCode(string itemCode, int fggID, int userID)
		{
			var order = new SAPOrderManager().GetByItemCode(itemCode);
			int SAPIssueNo = order?.DocNum ?? 0;

			var dbBOMDet = new BOMDetailManager();
			var bomdetails = dbBOMDet.GetByItemCode(itemCode);

			var fgg = new MODetailFGGManager().GetByID(fggID);
			fgg.MOHeader = new MOHeaderManager().GetByID(fgg.MOHeaderID);

			foreach (var d in bomdetails)
			{
				
				var itm = new MODetailItm
				{
					MODetailFGGID = fggID,
					RMItemName = d.RMName,
					RMItemCode = d.RMCode,
					RMFactor = d.RMFactor,
					UnitOfMeasure = d.UOM,
					CreatedBy = userID,
					CreatedOn = DateTime.Now,
					//SAPIssueNo = SAPIssueNo,
					//IssuedQty = d.RMQuantity,
					Allowance = d.Allowance,
					UOMFactor = d.UOMFactor,
					ItmsGrpCod = d.ItmsGrpCod,
					BOMDetailID = d.ID,
					SAPUnitCost = d.SAPUnitCost,
					BOMHeaderID = d.BOMHeaderID,
					IssuedQty2 = d.RMQuantity,
					MODetailFgg = fgg
				};

				var calc = CalculatorFactory.Create(itm);
				itm.MOQty = calc.ComputeMOQty(d.RMQuantity, d.UOMFactor.GetValueOrDefault(), d.Allowance.GetValueOrDefault(), d.RMFactor.GetValueOrDefault());

				itm.MODetailFgg = null;
				this.Create(itm);
			}
		}
	}
}
