﻿using AutoMapper;
using log4net;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace ShopFloor.Managers
{
    public class EntityManager<TEntity, TDbContext, TKey> : IDisposable
        where TEntity : class
        where TDbContext : DbContext, new()
    {
        protected static readonly ILog _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        protected TDbContext db;
        protected Expression<Func<TEntity, TKey>> _getter;

        protected Func<string, IQueryable<TEntity>> _search;
        public EntityManager(Expression<Func<TEntity, TKey>> getter)
        {
            _getter = getter;
            db = new TDbContext();
        }

        public virtual TEntity GetByID(TKey id)
        {
            return db.Set<TEntity>().AsEnumerable().FirstOrDefault(m => _getter.Compile()(m).Equals(id));
        }

        public virtual TEntity GetByIDIncluding(TKey id, params Expression<Func<TEntity, object>>[] include)
        {
            var query = db.Set<TEntity>().AsQueryable();

            query = include.Aggregate(query, (itm, exp) => itm.Include(exp));

            return query.ToList().FirstOrDefault(m => _getter.Compile()(m).Equals(id));
        }

        protected IList SearchEntity(string what, int pageSize, int currentPage, out int filteredCount, out int totalCount, string sortColumns, string direction)
        {
            if (pageSize == 0) pageSize = 50;
            //if (currentPage == 0) currentPage = 1;

            if (_search == null)
                throw new ApplicationException("Search method not specified.");

            var list = _search(what);

            totalCount = db.Set<TEntity>().Count();
            filteredCount = list.Count();

            if (string.IsNullOrEmpty(sortColumns))
            {
                list = Queryable.OrderBy(list, (dynamic)_getter);
            }
            else
            {
                list = direction == "desc" ? list.OrderByDescending(sortColumns) : list.OrderBy(sortColumns);
            }


            return list.Skip(currentPage).Take(pageSize).ToList();

        }

        public virtual List<TEntity> GetAll()
        {
            return db.Set<TEntity>().ToList();
        }

        public virtual void Save(TEntity entity)
        {
            try
            {
                if (entity.GetType() != typeof(TEntity) && !entity.GetType().FullName.Contains("DynamicProxies"))
                {
                    // EF wont recognize if entity is a derived model/viewmodel class so:
                    entity = Map(entity, Activator.CreateInstance<TEntity>());
                }

                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                _logger.Error($"Unable to save record. {entity.GetType().Name}", ex);
                throw;
            }
        }

        public virtual TKey Create(TEntity entity)
        {
            try
            {
                if (entity.GetType() != typeof(TEntity))
                {
                    // EF wont recognize if entity is a derived model/viewmodel class so:
                    entity = Map(entity, Activator.CreateInstance<TEntity>());
                }
                var e = db.Set<TEntity>().Add(entity);
                db.SaveChanges();

                return _getter.Compile()(e);
            }
            catch (Exception ex)
            {
                _logger.Error("Unable to create " + entity.GetType().Name, ex);
                throw;
            }
        }

        public virtual void Remove(TKey id)
        {
            try
            {
                db.Entry(GetByID(id)).State = EntityState.Deleted;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                _logger.Error($"Unable to delete record with id={id}", ex);
                throw;
            }
        }

        public virtual bool IsDuplicate(Expression<Func<TEntity, bool>> predicate)
        {
            return db.Set<TEntity>().Any(predicate);
        }

        public TDest Map<TSource, TDest>(TSource source, TDest target)
        {
            Mapper.CreateMap<TSource, TDest>();
            Mapper.Map(source, target);
            return target;
        }

        public void Dispose()
        {
            db.Dispose();
        }

        private int GetNextSequence10(int number)
        {
            return ((number + 10) / 10) * 10;
        }

        public int GetNextSequence(Expression<Func<TEntity, int?>> property)
        {
            var max = GetMaxIntValue(property);
            return GetNextSequence10(max);
        }

        public int GetMaxIntValue(Expression<Func<TEntity, int?>> property)
        {
            return db.Set<TEntity>().Max(property) ?? 0;
        }

        public int GetMaxIntValue(Expression<Func<TEntity, int?>> property, Expression<Func<TEntity, bool>> predicate)
        {
            var max = db.Set<TEntity>().Where(predicate).Max(property);
            return max.GetValueOrDefault(0);
        }


        public int GetNextSequence(Expression<Func<TEntity, int?>> property, Expression<Func<TEntity, bool>> predicate)
        {
            var max = db.Set<TEntity>().Where(predicate).Max(property);
            return GetNextSequence10(max ?? 0);
        }

        public void Flush()
        {
            db.SaveChanges();
        }

        public IQueryable<TEntity> QueryAll()
        {
            return db.Set<TEntity>().AsQueryable();
        }

        public IQueryable<TEntity> QueryAllIncluding(params Expression<Func<TEntity, object>>[] include)
        {
            var ret = QueryAll();

            return include.Aggregate(ret, (current, p) => current.Include(p));
        }

    }

}
