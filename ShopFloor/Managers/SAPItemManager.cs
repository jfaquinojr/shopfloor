﻿using ShopFloor.Managers;
using ShopFloor.SAP.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using ShopFloor;

namespace ShopFloor.SAP.Managers
{
    /// <summary>
    /// This class will contain every SAP-ITM Db operations in ShopFloor
    /// </summary>
    public class SAPItemManager : EntityManager<Item, SAPDb, string>, ISearchable2
    {
        // items that belong to these groupcodes are identified as Finished Goods
        protected short[] groupCodes = new short[] { 113, 114, 115, 116, 117, 118, 119, 126 };

		//2.Use this script: (select OITM.ItemCode, OITM.ItemName, OITM.ItmsGrpCod, OITM.InvntryUom, ITM1.Price from ITM1, OITM where ITM1.ItemCode = OITM.ItemCode and ITM1.PriceList = 3


		public override List<Item> GetAll()
        {
            return db.Items
                .Where(m => groupCodes.Contains(m.ItmsGrpCod))
                .ToList();
        }

        public Item GetByDocEntry(int docEntry)
        {
            return db.Items.FirstOrDefault(m => m.DocEntry == docEntry);
        }

        public SAPItemManager()
            : base(m => m.ItemCode)
        {
            _search = (what) =>
            {
                return db.Items
                    .Where(m => groupCodes.Contains(m.ItmsGrpCod) && (m.ItemCode.Contains(what) || m.ItemName.Contains(what)));
            };
        }

        public Item GetByItemCode(string itemCode)
        {
            return db.Items
                .Where(m => groupCodes.Contains(m.ItmsGrpCod))
                .FirstOrDefault(m => m.ItemCode == itemCode);
        }

        public IList Search(string what)
        {
            return _search(what).ToList();
        }


        public IList Search(string what, int pageSize, int currentPage, out int pageCount, out int rowCount, string sortColumns, string direction)
        {
            return SearchEntity(what, pageSize, currentPage, out pageCount, out rowCount, sortColumns, direction);
        }

		public virtual ItemPrice GetPrice(string itemCode)
		{
			return db.ItemPrices.FirstOrDefault(m => m.ItemCode == itemCode);
		}
    }

}

namespace ShopFloor.SAP.Entities
{
    public class SAPDb : DbContext
    {
        public SAPDb() : base("name=SAPDb") { }
        public virtual DbSet<Item> Items { get; set; }
        public virtual DbSet<CostCenter> CostCenters { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<RDR1> RDR1s { get; set; }
		public virtual DbSet<ItemPrice> ItemPrices { get; set; }
    }

    [Table("OITM", Schema = "dbo")]
    public partial class Item
    {
        [Key]
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public Int16 ItmsGrpCod { get; set; }
        public string U_ITEMCAT01 { get; set; }
        public string InvntryUom { get; set; }
        public int DocEntry { get; set; }

		[ForeignKey("ItemCode")]
		public List<ItemPrice> ItemPrices { get; set; } = new List<ItemPrice>();
    }

	[Table("ITM1", Schema = "dbo")]
	public partial class ItemPrice
	{
		[Key, Column(Order=1)]
		public string ItemCode { get; set; }
		[Key, Column(Order = 2)]
		public short PriceList { get; set; }
		public decimal? Price { get; set; }

		[ForeignKey("ItemCode")]
		public Item Item { get; set; }
	}

    [Table("OPRC", Schema = "dbo")]
    public partial class CostCenter
    {
        [Key]
        public string PrcCode { get; set; }
        public string PrcName { get; set; }
    }

    [Table("ORDR", Schema = "dbo")]
    public partial class Order
    {
        [Key]
        public int DocEntry { get; set; }
        public int DocNum { get; set; }
        public DateTime DocDueDate { get; set; }
        //public decimal? DocRate { get; set; }
        public string Canceled { get; set; }


        [ForeignKey("DocEntry")]
        public RDR1 OrderEx { get; set; }
    }

    [Table("RDR1", Schema = "dbo")]
    public partial class RDR1
    {
        [Key]
        public int DocEntry { get; set; }
        //[Key, Column(Order = 1)]
        public int LineNum { get; set; }
        public decimal OpenQty { get; set; }
        public string ItemCode { get; set; }
        public string LineStatus { get; set; }
    }
}
