﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Managers
{
    public interface ISearchable<TEntity>
    {
        List<TEntity> Search<TKey>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, TKey>> sortColumn, int pageSize, int currentPage, out int pageCount, out int rowCount);
        
    }

    public interface ISearchable2
    {
        IList Search(string what);
        IList Search(string what, int pageSize, int currentPage, out int filteredCount, out int totalCount, string sortColumns, string dir);
    }
}
