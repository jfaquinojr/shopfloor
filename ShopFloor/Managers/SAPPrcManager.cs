﻿using ShopFloor.SAP.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Managers
{
    public class SAPPrcManager : EntityManager<CostCenter, SAPDb, string>, ISearchable2
    {
        public SAPPrcManager()
            : base(m => m.PrcCode)
        {
            db = new SAPDb();
            _search = (what) =>
            {
                return db.CostCenters.Where(m => m.PrcCode.Contains(what) || m.PrcName.Contains(what));
            };
        }

        public IList Search(string what)
        {
            return _search(what).ToList();
        }

        public IList Search(string what, int pageSize, int currentPage, out int pageCount, out int rowCount, string sortColumns, string direction)
        {
            return SearchEntity(what, pageSize, currentPage, out pageCount, out rowCount, sortColumns, direction);
        }
    }
}
