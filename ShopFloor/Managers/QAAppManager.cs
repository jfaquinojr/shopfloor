﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShopFloor.Entities;

namespace ShopFloor.Managers
{
    public class QAAppManager : ShopFloorEntityManager<QAApplication>, ISearchable2
    {
        public QAAppManager() : base(m => m.ID) {
            _search = (what) =>
            {
                return db.QAApplications
                    .Where(m => m.QAApplCode.Contains(what) || m.QAApplDesc.Contains(what));
            };
        }

        public IList Search(string what)
        {
            return _search(what).ToList();
        }

        public IList Search(string what, int pageSize, int currentPage, out int filteredCount, out int totalCount, string sortColumns, string dir)
        {
            return SearchEntity(what, pageSize, currentPage, out filteredCount, out totalCount, sortColumns, dir);
        }
    }
}
