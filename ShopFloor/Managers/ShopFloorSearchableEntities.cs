﻿using System;
using System.Collections;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace ShopFloor.Managers
{
    public class ShopFloorSearchableEntities<TEntity> : ShopFloorEntityManager<TEntity>, ISearchable2 
        where TEntity : class
    {

        public ShopFloorSearchableEntities(Expression<Func<TEntity, int>> id, Func<DbSet<TEntity>, string, IQueryable<TEntity>> searchAction) : base(id)
        {
            _search = (what) =>
            {
                return searchAction(db.Set<TEntity>(), what);
            };
        }

        public IList Search(string what)
        {
            return _search(what).ToList();
        }

        public IList Search(string what, int pageSize, int currentPage, out int filteredCount, out int totalCount, string sortColumns, string dir)
        {
            return this.SearchEntity(what, pageSize, currentPage, out filteredCount, out totalCount, sortColumns, dir);
        }
    }
}
