﻿using ShopFloor.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Managers
{
    public class ResourceCategoryManager : ShopFloorEntityManager<ResourceCategory>, ISearchable2
    {
        public ResourceCategoryManager()
            : base(m => m.ID)
        {
            _search = (what) =>
            {
                return db.ResourceCategories.Where(m => m.Code.Contains(what) || m.Description.Contains(what));
            };
        }

        public IList Search(string what)
        {
            return _search(what).ToList();
        }

        public IList Search(string what, int pageSize, int currentPage, out int pageCount, out int rowCount, string sortColumns, string direction)
        {
            return SearchEntity(what, pageSize, currentPage, out pageCount, out rowCount, sortColumns, direction);
        }
    }
}

