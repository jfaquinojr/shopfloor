﻿using ShopFloor.DTO;
using ShopFloor.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;

namespace ShopFloor.Managers
{
    public class MOHeaderManager : ShopFloorEntityManager<MOHeader>, ISearchable2
    {
        public MOHeaderManager()
            : base(m => m.ID)
        {
            _search = (what) =>
            {
                return db.MOHeaders
                .Include(m => m.FinishedGoods)
                .Include(m => m.MOType)
                .AsEnumerable()
                .Where(m => m.MONumber.ToString().Contains(what)
                    || m.MOType.MOTypeCode == what || m.MOType.MOTypeName.Contains(what))
                .AsQueryable();
            };
        }

        public override MOHeader GetByID(int id)
        {
            return db.MOHeaders
                .Include(m => m.FinishedGoods)
                .Include(m => m.MOType)
                .Where(m => m.ID == id)
                .Single();
        }

        public MOHeader GetByMONumber(int moNumber)
        {
            return db.MOHeaders
                .Include("FinishedGoods")
                .Where(m => m.MONumber == moNumber)
                .FirstOrDefault();
        }

        public void CloseMO(int id)
        {
            var mo = GetByID(id);
            mo.MOClosedTag = true;
            Save(mo);

            ArchiveAllClosedMO();
        }


        public string[] ArchiveAllClosedMO()
        {
            var ret = new List<string>();

            // get all closed MOs
            var listOfClosedMOs = db.MOHeaders
                .Include( m=>m.MOType)
                .Where(m => m.MOClosedTag == true)
                .ToList();

            // for every closed MO:
            foreach(var mo in listOfClosedMOs)
            {

                this.ArchiveMO(mo);

                try
                {
                    db.SaveChanges();


                    // eradicate MO
                    var dbMO = new MOHeaderManager();
                    dbMO.Remove(mo.ID);


                    ret.Add(mo.ID.ToString());


                }
                catch (Exception ex)
                {
                    Trace.TraceError(ex.Message);
                    throw;
                }
                
            }




            return ret.ToArray();
        }


        public void ArchiveMO(MOHeader mo)
        {
            // insert into HSMOHeader
            var hsMO = new HSMOHeader();
            hsMO.MOType = mo.MOType.MOTypeCode;
            hsMO.MONumber = mo.MONumber;
            hsMO.MOPriority = mo.MOPriority;
            hsMO.DateCreated = mo.CreatedOn;
            hsMO.GangRunTag = (short)(mo.GangRunTag.GetValueOrDefault(false) ? 1 : 0);
            hsMO.MOClosedTag = 1;
            db.HSMOHeaders.Add(hsMO);



            // insert into HSMODetailFGG
            var fggs = db.MODetailFggs.Where(m => m.MOHeaderID == mo.ID)
                .ToList();
            foreach (var fgg in fggs)
            {
                var hsFGG = new HSMODetailFgg();
                hsFGG.MOType = mo.MOType.MOTypeCode;
                hsFGG.MONumber = mo.MONumber;
                hsFGG.MOPriority = mo.MOPriority;
                hsFGG.FGItemCode = fgg.ItemCode;
				hsFGG.BatchQty = (int)fgg.BatchQty.GetValueOrDefault(0);
                hsFGG.BatchPrcnt = (int)fgg.BatchPrcnt.GetValueOrDefault(0);
                hsFGG.SONo = fgg.SONo;
                hsFGG.DeliveryDate = fgg.DeliveryDate;
                hsFGG.OutsTag = fgg.OutsTag;
                db.HSMODetailFggs.Add(hsFGG);



                // insert into HSMODetailITM
                var itms = db.MODetailItms.Where(m => m.MODetailFGGID == fgg.ID).ToList();
                foreach (var itm in itms)
                {
                    var hsITM = new HSMODetailItm();
                    hsITM.MOType = mo.MOType.MOTypeCode;
                    hsITM.MONumber = mo.MONumber;
                    hsITM.MOPriority = mo.MOPriority;
                    hsITM.MONumbFGNo = fgg.ID;
                    hsITM.FGItemCode = fgg.ItemCode;
                    hsITM.RMItemCode = itm.RMItemCode;
                    hsITM.MOQty = itm.MOQty;
                    hsITM.SAPIssueNo = itm.SAPIssueNo;
                    hsITM.IssuedQty = itm.IssuedQty;
                    db.HSMODetailItms.Add(hsITM);
                }



                // insert into HSMODetailRSC
                var rscs = db.MODetailRscs.Where(m => m.MODetailFGGID == fgg.ID)
                    .ToList();
                foreach (var rsc in rscs)
                {
                    var hsRSC = new HSMODetailRsc();
                    hsRSC.MOType = mo.MOType.MOTypeCode;
                    hsRSC.MONumber = mo.MONumber;
                    hsRSC.FGItemCode = fgg.ItemCode;
                    hsRSC.ResourceCode = rsc.ResourceCode;
                    hsRSC.SetupTime = rsc.SetupTime;
                    hsRSC.StockTime = rsc.StockTime;
                    hsRSC.EstTime = rsc.EstTime;
					hsRSC.FromDate = rsc.FromDate;
					hsRSC.ToDate = rsc.ToDate;
                    hsRSC.OrigFromDate = rsc.OrigFromDate;
                    hsRSC.OrigToDate = rsc.OrigToDate;
                    hsRSC.OrigEstTime = rsc.OrigEstTime;
                    hsRSC.RSCSeq = rsc.ID; // per request, instead of sequence, assign MOdetailRSCID
                    hsRSC.ClosedTag = (short)(rsc.ClosedTag.GetValueOrDefault(false) ? 1 : 0);
                    hsRSC.OnStreamTag = (short)(rsc.OnStreamTag.GetValueOrDefault(false) ? 1 : 0);
                    hsRSC.MOStat = (short)(rsc.OnHold.GetValueOrDefault(false) ? 0 : 1);
                    hsRSC.MOPriority = mo.MOPriority;
                    hsRSC.PriorityTag = (short)(rsc.PriorityTag.GetValueOrDefault(false) ? 1 : 0);
                    hsRSC.BatchPrcnt = (short)rsc.BatchPercent.GetValueOrDefault(0);
                    db.HSMODetailRscs.Add(hsRSC);
                }
            }
        }
        

        public override int Create(MOHeader entity)
        {
            // assign MO Priority. add 10 to maximum priority.
            entity.MOPriority = GetNextSequence(m => m.MOPriority);
            return base.Create(entity);
        }

        public IList Search(string what)
        {
            return _search(what).ToList();
        }

        public IList Search(string what, int pageSize, int currentPage, out int pageCount, out int rowCount, string sortColumns, string direction)
        {
            return SearchEntity(what, pageSize, currentPage, out pageCount, out rowCount, sortColumns, direction);
        }

        public List<MODetailFgg> GetFinishedGoods(int id)
        {
            var result = db.MODetailFggs
                .Include(m => m.RawMaterials)
                .Include(m => m.MOHeader)
                .OrderBy(m => m.ID)
                .Where(m => m.MOHeaderID == id);

            var returnset = (from fgg in result.ToList()
                             let transm = fgg.Transmittal = db.Transmittals.Where(m => m.MODetailFGGID == fgg.ID).FirstOrDefault()
                             let goods = fgg.TotalGoods = db.ShopFloorDataCaptures.Where(m => m.MODetailFGGID == fgg.ID).Sum(m => m.Good).GetValueOrDefault(0)
                             select fgg);

            return returnset.ToList();
        }

        public MODetailFgg GetFinishedGood(int fggId)
        {
            var result = db.MODetailFggs
                .Single(m => m.ID == fggId);
            return result;
        }

        public List<MOReschedule> GetMOByResource2()
        {
            var minmaxDates =
            from sched in db.MOResourceSchedules
            group sched by sched.MOHeaderID into g
            select new
            {
                MOHeaderID = g.Key,
                MinStartDate = g.Min(m => m.StartDateTime),
                MaxEndDate = g.Max(m => m.EndDateTime)
            };

            var result = (
                from mo in db.MOHeaders
                    .Include(m => m.MOType)
                    .AsEnumerable() // <-- because of string.Join below
                join dates in minmaxDates on mo.ID equals dates.MOHeaderID
                where mo.IsScheduled == true && mo.MOType.MOTypeCode == Constants.CONST_MOTYPES_MO
                orderby mo.MOPriority
                let fggs = mo.FinishedGoods.Where(f => f.MOHeaderID == mo.ID)
                select new MOReschedule
                {
                    MOHeaderID = mo.ID,
                    MOPriority = mo.MOPriority.Value,
                    MONumber = mo.MONumber,
                    FGCode = string.Join(", ", fggs.Select(f => f.ItemCode)),
                    FGName = string.Join(", ", fggs.Select(f => f.ItemName)),
                    MinStartDate = dates.MinStartDate,
                    MaxEndDate = dates.MaxEndDate,
                    EstRunTime = 0,
                    MinDeliveryDate = fggs.Min(f => f.DeliveryDate)
                }
            );
            return result.ToList();
        }

        public List<MOByResourceResult> GetMOByResource()
        {
            var result = (from h in db.MOHeaders
                          join fgg in db.MODetailFggs on h.ID equals fgg.MOHeaderID
                          join rsc in db.MODetailRscs on fgg.ID equals rsc.MODetailFGGID
                          //where res.ID == id
                          orderby h.MOPriority, rsc.FromDate
                          select new MOByResourceResult
                          {
                              ID = h.ID,
                              MOPriority = h.MOPriority.ToString(),
                              MONumber = h.MONumber.ToString(),
                              FGCode = fgg.ItemCode,
                              FGName = fgg.ItemName,
                              EndDateTime = rsc.ToDate,
                              StartDateTime = rsc.FromDate,
                              DeliveryDate = fgg.DeliveryDate,
                              EstRunTime = rsc.EstTime,
                              ResourceID = rsc.ResourceID ?? 0,
                              IsScheduled = h.IsScheduled ?? false,
                              Status = rsc.ClosedTag == true ? "Closed" : rsc.OnStreamTag == null ? "No Activity" : rsc.OnStreamTag == false ? "Stopped" : "Started",
                          });
            return result.ToList();
        }


        public void ProcessSchedule(int moHeaderID, string username)
        {
            //var dbSched = new MOResourceScheduleManager();
            //foreach (var fgg in GetFinishedGoods(moHeaderID))
            //{
            //    // create schedule entry
            //    dbSched.ProcessScheduleForFGG(fgg.ID, username);
            //}

            var dbRSC = new MODetailRSCManager();
            dbRSC.ProcessSchedule(new int[] { moHeaderID }, username);
        }

        public override void Remove(int id)
        {
            // remove all child tables
            var mo = GetByID(id);

            DeleteFGG(id);

            db.SaveChanges();

            db.MOHeaders.Remove(mo);

            db.SaveChanges();
        }

        private void DeleteFGG(int id)
        {
            if (db.MODetailFggs.Any(m => m.MOHeaderID == id))
            {
                
                foreach (var fgg in db.MODetailFggs.Where(m => m.MOHeaderID == id))
                {
                    DeleteRSC(fgg);
                    DeleteITM(fgg);
                }

                db.MODetailFggs.RemoveRange(db.MODetailFggs.Where(m => m.MOHeaderID == id));
            }
        }

        private void DeleteITM(MODetailFgg fgg)
        {
            db.MODetailItms.RemoveRange(db.MODetailItms.Where(m => m.MODetailFGGID == fgg.ID));
        }

        private void DeleteRSC(MODetailFgg fgg)
		{
			var fggID = fgg.ID;

			if (db.MODetailRscs.Any(m => m.MODetailFGGID == fggID))
			{
				foreach (var rsc in db.MODetailRscs.Where(m => m.MODetailFGGID == fggID))
				{
					// remove all resource schedule
					db.MOResourceSchedules.RemoveRange(db.MOResourceSchedules.Where(m => m.MODetailRSCID == rsc.ID));
				}

				db.MODetailRscs.RemoveRange(db.MODetailRscs.Where(m => m.MODetailFGGID == fggID));
			}
		}
	}
}
