﻿using ShopFloor.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Managers
{
    public class UserClassManager : ShopFloorEntityManager<UserClass>
    {
        public UserClassManager()
            : base(m => m.ID)
        {

        }

        public void ChangeUserClassFor(string userName, int userClassId, string createdByUsername)
        {
			var userId = GetUserIDByUsername(userName);
			var userUserClass = db.UserUserClasses.FirstOrDefault(m => m.UserID == userId);
			if(userUserClass?.ID == userClassId)
			{
				// no changes
				return;
			}
			
            // remove any old user class from user
            db.UserUserClasses.RemoveRange(db.UserUserClasses.Where(m => m.User.UserName == userName));

            // insert new user class
            var userClass = db.UserClasses.Single(u => u.ID == userClassId);
            var user = db.Users.Single(u => u.UserName == userName);
            db.UserUserClasses.Add(new UserUserClass
            {
                UserID = user.ID,
                UserClassID = userClass.ID,
                CreatedBy = GetUserIDByUsername(createdByUsername),
                CreatedOn = DateTime.Now
            });
            db.SaveChanges();
        }
    }
}
