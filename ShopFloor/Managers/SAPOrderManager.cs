﻿using ShopFloor.SAP.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using ShopFloor.DTO;

namespace ShopFloor.Managers
{
    public class SAPOrderManager : EntityManager<Order, SAPDb, int>, ISearchable2
    {
        public SAPOrderManager()
            : base(m => m.DocEntry)
        {
            db = new SAPDb();
            _search = (what) =>
            {
                var result = (
                    from o in db.Orders
                    join r in db.RDR1s on o.DocEntry equals r.DocEntry
                    where r.LineStatus == "O" && o.Canceled == "N"
                    select o)
                    .Include(m => m.OrderEx);


                return GetDistinct(
                    result
                    .AsEnumerable()
                    .Where(m => m.DocNum.ToString().Contains(what))
                    .AsQueryable()
                    );
            };
        }

        private IQueryable<Order> GetDistinct(IQueryable<Order> result)
        {
            return result.GroupBy(m => m.DocEntry)
                .Select(grp => grp.FirstOrDefault());
        }

        public void FilterWithItemCodeAsWell(string itemCode)
        {
            _search = (what) =>
            {
                var result = (
                    from o in db.Orders
                    join r in db.RDR1s on o.DocEntry equals r.DocEntry
                    where r.LineStatus == "O" && o.Canceled == "N" && r.ItemCode == itemCode
                    select o)
                    .Include(m => m.OrderEx);


                return GetDistinct(result
                    .AsEnumerable()
                    .Where(m => m.DocNum.ToString().Contains(what))
                    .AsQueryable()
                    );
            };
        }

        public Order GetByItemCode(string itemCode)
        {
            return (from i in db.Items
                    join o in db.Orders on i.DocEntry equals o.DocEntry
                    where i.ItemCode == itemCode
                    select o)
                    .FirstOrDefault();
        }

        public IList Search(string what)
        {
            return _search(what).ToList();
        }

        public IList Search(string what, int pageSize, int currentPage, out int pageCount, out int rowCount, string sortColumns, string direction)
        {
            return SearchEntity(what, pageSize, currentPage, out pageCount, out rowCount, sortColumns, direction);
        }


        public override Order GetByID(int id)
        {
            var result = (
               from o in db.Orders
               join r in db.RDR1s on o.DocEntry equals r.DocEntry
               where o.DocEntry == id
               select o)
               .Include(m => m.OrderEx);

            return result.FirstOrDefault();
        }

        public override List<Order> GetAll()
        {
            var result = (
               from o in db.Orders
               join r in db.RDR1s on o.DocEntry equals r.DocEntry
               select o)
               .Include(m => m.OrderEx);

            return result.ToList();
        }

        public Order GetByDocNum(int docNum)
        {
            var result = (
               from o in db.Orders
               join r in db.RDR1s on o.DocEntry equals r.DocEntry
               where o.DocNum == docNum
               select o)
               .Include(m => m.OrderEx);

            return result.FirstOrDefault();
        }
    }
}
