﻿using ShopFloor.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Managers
{
    public class OperationManager : ShopFloorEntityManager<Operation>, ISearchable2
    {
        public OperationManager() : base(m => m.ID) 
        {
            _search = (what) =>
            {
                return db.Operations.Where(m => m.OperCode.Contains(what) || m.OperDesc.Contains(what));
            };
        }

        public IList Search(string what)
        {
            return _search(what).ToList();
        }

        public IList Search(string what, int pageSize, int currentPage, out int pageCount, out int rowCount, string sortColumns, string direction)
        {
            return this.SearchEntity(what, pageSize, currentPage, out pageCount, out rowCount, sortColumns, direction);
        }
    }
}
