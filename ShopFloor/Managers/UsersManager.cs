﻿using LinqKit;
using ShopFloor.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Data.Entity;

namespace ShopFloor.Managers
{
    public class UsersManager: ShopFloorEntityManager<User>, ISearchable2
    {
        public UsersManager()
            : base(m => m.ID)
        {
            _search = (what) =>
            {
                return db.Users
                    .Include(m => m.UserUserClasses)
                    .Include(m => m.UserUserClasses.Select(s=>s.UserClass))
                    .Where(m => m.UserID.Contains(what) || m.UserName.Contains(what) || m.UserUserClasses.Any(uc=>uc.UserClass.UClassCode == what));
            };
        }

        public override User GetByID(int id)
        {
            return db.Users
                .Include(m => m.UserUserClasses)
                .Include(m => m.UserUserClasses.Select(s => s.UserClass))
                .Single(m => m.ID == id);
        }

        private int NewRegisteredUserClassID
        {
            get { return 7; } // change to get from config file or db.
        }

        public bool IsUserInRole(string name, string role)
        {
            var ret = db.UserUserClasses
                .Include(m => m.User)
                .Include(m => m.UserClass)
                .Any(m => m.UserClass.UClassCode == role || m.User.UserName == name);
            return ret;
        }

        public bool ValidateLogin(string username, string password)
        {
            return db.Users.AsEnumerable().Any(m => m.UserName.ToLower() == username.ToLower() && m.UserPW == HashText(password));
        }

        public List<string> GetRoles(string name)
        {
            return (from uc in db.UserClasses
                    join ucc in db.UserUserClasses on uc.ID equals ucc.UserClassID
                    join u in db.Users on ucc.UserID equals u.ID
                    where u.UserName == name
                    select uc.UClassCode)
                    .ToList();
        }

        public User GetByUsername(string username)
        {
            return db.Users
                .Include(m=>m.UserUserClasses)
                .Where(m => m.UserName == username).FirstOrDefault();
        }

        public bool HasDuplicate(string username)
        {
            return db.Users.Any(m => m.UserName == username);
        }

        public string RegisterNewUser(string username, string password, string userId, string empId)
        {
            // create new user
            string code = Guid.NewGuid().ToString();
            var user = new User
            {
                UserName = username,
                UserPW = HashText(password),
                EmpID = empId,
                UserID = userId,
                IsVerified = false
            };
            base.Create(user);

            return code;
        }

        public UserClass GetUserClass(int id)
        {
            var user = db.Users
                .Include(m => m.UserUserClasses)
                .Include(m => m.UserUserClasses.Select(s => s.UserClass))
                .Where(u => u.ID == id)
                .First();
            var userclass = user.UserUserClasses?.FirstOrDefault();
			return userclass?.UserClass;
        }

		public List<User> GetUserRegistrations()
        {
            return db.Users.Where(m => m.IsVerified.Value == false)
                .OrderByDescending(m => m.RegisterDate)
                .ToList();
        }

        public bool IsVerified(string username)
        {
            return db.Users.Any(m => m.UserName == username && m.IsVerified.Value == true);
        }

        public User GrantAccess(int id, string username)
        {
            User user = GetByID(id);
            user.IsVerified = true;
            user.VerifiedOn = DateTime.Now;
            user.VerifiedBy = GetByUsername(username).ID;
            Save(user);
            return user;
        }

        public IList Search(string what, int pageSize, int currentPage, out int pageCount, out int rowCount, string sortColumns, string direction)
        {
            return SearchEntity(what, pageSize, currentPage, out pageCount, out rowCount, sortColumns, direction);
        }

        public IList Search(string what)
        {
            return _search(what).ToList();
        }


        public string HashText(string text)
        {
            // Setup the password generator
            var provider = new SHA1CryptoServiceProvider();
            var encoding = new UnicodeEncoding();
            var s =  Convert.ToBase64String(provider.ComputeHash(encoding.GetBytes(text)));
            return s;
        }


        public void ChangePassword(string username, string password)
        {
            var user = db.Users.Single(m => m.UserName == username);
            user.UserPW = HashText(password);
            user.ChangePassword = false;
            db.SaveChanges();
        }

        public bool IsChangePassword(string username)
        {
            return db.Users.Any(m => m.UserName == username && m.ChangePassword == true);
        }
	}
}
