﻿using ShopFloor.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Managers
{
    public class AdditionalReasonManager : ShopFloorEntityManager<AdditionalReason>, ISearchable2
    {
        public AdditionalReasonManager() : base(m => m.ID) 
        {
            _search = (what) =>
            {
                return db.AdditionalReasons.Where(m => m.ReasonCode.Contains(what) || m.ReasonName.Contains(what));
            };
        }

        public IList Search(string what)
        {
            return _search(what).ToList();
        }

        public IList Search(string what, int pageSize, int currentPage, out int pageCount, out int rowCount, string sortColumns, string direction)
        {
            return SearchEntity(what, pageSize, currentPage, out pageCount, out rowCount, sortColumns, direction);
        }
    }
}
