﻿using ShopFloor.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ShopFloor.Managers
{
    public class ShopFloorQAParManager : ShopFloorEntityManager<ShopFloorQAPar>
    {
        public ShopFloorQAParManager() : base(m => m.ID) { }

        public List<ShopFloorQAPar> GetAllByShopFloorID(int shopFloorId, string resourceCode)
        {
            return db.ShopFloorQAPars
                .Include(m => m.QAParam)
                .Where(m => m.ShopFloorID == shopFloorId)
                .ToList();
        }

        public void SaveQAParamValues(int[] shopFloorIDs, ShopFloorQAPar model)
        {
            // update records with same QAParamID 
            var result = db.ShopFloorQAPars
                .Where(m => shopFloorIDs.Contains(m.ShopFloorID) && m.QAParamID == model.QAParamID)
                .ToList();
            foreach (var entity in result)
            {
                entity.QAParamActual = model.QAParamActual;
                entity.ModifiedBy = model.ModifiedBy;
                entity.ModifiedOn = model.ModifiedOn;
            }
            db.SaveChanges();
        }
    }
}
