﻿using ShopFloor.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Data.SqlClient;

namespace ShopFloor.Managers
{
	public class JEProcessingManager : ShopFloorEntityManager<JEProcessingHistory>
	{
		public JEProcessingManager() : base(m => m.ID)
		{
		}

		public JEProcessingHistory GetRecent()
		{
			return db.JEProcessingHistories.OrderByDescending(m => m.GeneratedOn).FirstOrDefault();
		}

		public void ProcessNow(DateTime from, DateTime to, int userId)
		{
			SqlParameter param1 = new SqlParameter("@From", from.ToShortDateString());
			SqlParameter param2 = new SqlParameter("@To", to.ToShortDateString());
			db.Database.ExecuteSqlCommand("usp_JEProcessing @From, @To", param1, param2);

			var history = new JEProcessingHistory();
			history.DateFrom = from;
			history.DateTo = to;
			history.GeneratedBy = userId;
			history.GeneratedOn = DateTime.Now;
			db.JEProcessingHistories.Add(history);
			db.SaveChanges();
		}

		public List<ZZTemp_ojdt> GetHeader()
		{
			return db.ZZTemp_ojdts.ToList();
		}

		public List<ZZTemp_JDT1> GetDetails()
		{
			return db.ZZTemp_JDT1.ToList();
		}
	}
}
