﻿using ShopFloor.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ShopFloor.Managers
{
    public class QACriteriaManager : ShopFloorEntityManager<QACriterion>, ISearchable2
    {
        public QACriteriaManager()
            : base(m => m.ID)
        {
            _search = (what) =>
            {
                return db.QACriteria
                    .Include(m => m.QAInspMethod)
                    .Where(m => m.QACritCode.Contains(what) && m.QACritDesc.Contains(what));
            };
        }

        public IList Search(string what)
        {
            return _search(what).ToList();
        }

        public IList Search(string what, int pageSize, int currentPage, out int filteredCount, out int totalCount, string sortColumns, string dir)
        {
            return SearchEntity(what, pageSize, currentPage, out filteredCount, out totalCount, sortColumns, dir);
        }
    }
}
