﻿using ShopFloor.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.SqlTypes;

namespace ShopFloor.Managers
{
    public class BOMManager : ShopFloorEntityManager<BillOfMaterial>, ISearchable2
    {
        public BOMManager()
            : base(m => m.ID)
        {
            _search = (what) =>
            {
                var result = db.BillOfMaterials
                    .Include(m => m.BOMDetails)
                    .Include(m => m.ItemBOMs)
                    .Where(m => m.BOMCode.Contains(what) 
                        || m.BOMName.Contains(what) 
                        || m.BOMDetails.Any(d => d.RMCode.Contains(what))
                        || m.ItemBOMs.Any(d => d.ItemCode.Contains(what))
                        || m.ItemBOMs.Any(d => d.ItemName.Contains(what)));

                return result;
            };
        }

        public IList Search(string what)
        {
            return _search(what).ToList();
        }

        public IList Search(string what, int pageSize, int currentPage, out int pageCount, out int rowCount, string sortColumns, string direction)
        {
            return SearchEntity(what, pageSize, currentPage, out pageCount, out rowCount, sortColumns, direction);
        }

        public void MarkAsFinal(int id, string username)
        {
            var bom = db.BillOfMaterials.Single(m => m.ID == id);
            bom.IsFinal = true;
            bom.ModifiedBy = new UsersManager().GetByUsername(username).ID;
            bom.ModifiedOn = DateTime.Now;
            db.SaveChanges();
        }

        public override void Remove(int id)
        {
			// remove child tables first
			var bomDetails = db.BillOfMaterialDetails.Where(m => m.BOMHeaderID == id);
			db.BOMDetailSubs.RemoveRange(db.BOMDetailSubs.Where(m => bomDetails.Select(d=>d.ID).Contains(m.BOMDetailID)));
			db.BillOfMaterialDetails.RemoveRange(bomDetails);
			db.ItemBoms.RemoveRange(db.ItemBoms.Where(b => b.BOMHeaderID == id));
            db.SaveChanges();

            base.Remove(id);
        }

        public void CreateItemBOM(int id, string itemCode, string username)
        {
            var dbItems = new SAP.Managers.SAPItemManager();
            var item = dbItems.GetByID(itemCode);

            var newItemBom = new ItemBom
            {
                BOMHeaderID = id,
                ItemCode = item.ItemCode,
                ItemName = item.ItemName,
                CreatedBy = GetUserIDByUsername(username),
                CreatedOn = DateTime.Now
            };
            db.ItemBoms.Add(newItemBom);
            db.SaveChanges();
        }

        public List<ItemBom> GetBOMItems(int id)
        {
            return db.ItemBoms.Where(m => m.BOMHeaderID == id).ToList();
        }

        public void DeleteItemBOM(int id)
        {
            var e = db.ItemBoms.Single(m => m.ID == id);
            db.ItemBoms.Remove(e);
            db.SaveChanges();
        }

        public BillOfMaterial GetBOMByItemCode(int bomHeaderId, string itemCode)
        {
            var e = (from b in db.BillOfMaterials
                     join ib in db.ItemBoms on b.ID equals ib.BOMHeaderID
                     where ib.ItemCode == itemCode && b.ID == bomHeaderId
                     select b)
                     .FirstOrDefault();
            return e;
        }

        public bool IsItemBOMAttached(string itemCode)
        {
            return db.ItemBoms.Any(m => m.ItemCode == itemCode);
        }

        public bool IsEffective(string itemcode)
		{
            var sqlMinValue = SqlDateTime.MinValue.Value;
            var sqlMaxValue = SqlDateTime.MaxValue.Value;
			var result = (from d in db.ItemBoms
						  join h in db.BillOfMaterials on d.BOMHeaderID equals h.ID
                          where d.ItemCode == itemcode
                          && (h.EffectFrom ?? sqlMinValue) <= DateTime.Now
                          && (h.EffectTo ?? sqlMaxValue) >= DateTime.Now
						  select h);

            return result.Count() > 0;
		}

		public void Copy(int sourceBOMID, int destBOMID)
		{
			if (sourceBOMID == 0 || destBOMID == 0) return;

			// copy bomdetails and subs
			var dbBOMDetailMgr = new BOMDetailManager();
			var dbSubsMgs = new ShopFloorEntityManager<BOMDetailSub>(m => m.ID);
			var bomdetails = dbBOMDetailMgr.QueryAll().Where(m => m.BOMHeaderID == sourceBOMID).ToList();
			foreach(var bomdetail in bomdetails)
			{
				var d = dbBOMDetailMgr.Map(bomdetail, new BillOfMaterialDetail());
				d.ID = 0;
				d.BOMHeaderID = destBOMID;
				dbBOMDetailMgr.Create(d);

				var subs = dbSubsMgs.QueryAll().Where(m => m.BOMDetailID == bomdetail.ID).ToList();
				foreach(var sub in subs)
				{
					var newSub = dbSubsMgs.Map(sub, new BOMDetailSub());
					newSub.BOMDetailID = d.ID;
					dbSubsMgs.Create(newSub);
				}
			}


			//// copy itembom
			//var dbItemBOM = new ShopFloorEntityManager<ItemBom>(m => m.ID);
			//var items = dbItemBOM.QueryAll().Where(m => m.BOMHeaderID == sourceBOMID).ToList();
			//foreach(var item in items)
			//{
			//	var newItm = dbItemBOM.Map(item, new ItemBom());
			//	newItm.BOMHeaderID = destBOMID;
			//	dbItemBOM.Create(newItm);
			//}
		}
	}
}
