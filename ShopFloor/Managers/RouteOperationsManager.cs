﻿using ShopFloor.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ShopFloor.Managers
{
    public class RouteOperationsManager : ShopFloorEntityManager<RouteOperation>, ISearchable2
    {
        public RouteOperationsManager() : base(m => m.ID) {
            _search = (what) =>
            {
                return db.RouteOperations
                    .Include(m => m.Route)
                    .Include(m => m.Operation)
                   .AsEnumerable().Where(m => m.Operation.OperCode.Contains(what) || m.Operation.OperDesc.Contains(what)
                       || m.Route.RouteCode.Contains(what) || m.Route.RouteDesc.Contains(what)).AsQueryable();
            };
        }

        public override RouteOperation GetByID(int id)
        {
            return db.RouteOperations
                .Include(m => m.Operation)
                .First(m => m.ID == id);
        }

        public override int Create(RouteOperation entity)
        {
            entity.Sequence = GetNextSequence(m => m.Sequence, m=>m.RouteID == entity.RouteID);
            return base.Create(entity);
        }

        public IList Search(string what)
        {
            return _search(what).ToList();
        }

        public IList Search(string what, int pageSize, int currentPage, out int pageCount, out int rowCount, string sortColumns, string direction)
        {
            return SearchEntity(what, pageSize, currentPage, out pageCount, out rowCount, sortColumns, direction);
        }

        public string GetOperDesc(int id)
        {
            return db.Operations.FirstOrDefault(m => m.ID == id).OperDesc;
        }

        public string GetRouteDesc(int id)
        {
            return db.Routes.FirstOrDefault(m => m.ID == id).RouteDesc;
        }

        public List<RouteOperation> GetAllByRouteID(int id)
        {
            return db.RouteOperations
                .Include(m => m.Operation)
                .Where(m => m.RouteID == id)
                .ToList();
        }
    }
}
