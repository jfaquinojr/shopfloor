﻿using ShopFloor.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Managers
{
    public class ActivityTypeManager : ShopFloorEntityManager<ActivityType>, ISearchable2
    {
        public ActivityTypeManager()
            : base(m => m.ID)
        {
            _search = (s) =>
            {
                return db.ActivityTypes.Where(m => m.ActivityCode.Contains(s) || m.ActivityName.Contains(s));
            };
        }

        public IList Search(string what)
        {
            var list = _search(what);
            return list.ToList();
        }

        public IList Search(string what, int pageSize, int currentPage, out int pageCount, out int rowCount, string sortColumns, string direction)
        {
            return SearchEntity(what, pageSize, currentPage, out pageCount, out rowCount, sortColumns, direction);
        }
    }
}

