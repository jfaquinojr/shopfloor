﻿using ShopFloor.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Collections;
using System.Data.Entity;

namespace ShopFloor.Managers
{
    public class ResourceManager : ShopFloorEntityManager<Resource>, ISearchable2
    {
        public ResourceManager() : base(m => m.ID)
        {
            _search = (what) =>
            {
                return db.Resources
                .Include(m => m.ResourceCategory)
                .Where(m => m.ResourceCode.Contains(what) || m.ResourceDesc.Contains(what));
            };
        }

        public string GetResourceCodeByID(int Id)
        {
            var resource = db.Resources.FirstOrDefault(m => m.ID == Id);
            return resource?.ResourceCode;
        }

        public override Resource GetByID(int id)
        {
            return db.Resources
                .Include(m => m.ResourceCategory)
                .Single(m => m.ID == id);
        }

        public IList Search(string what)
        {
            return _search(what).ToList();
        }

        public IList Search(string what, int pageSize, int currentPage, out int pageCount, out int rowCount, string sortColumns, string direction)
        {
            return SearchEntity(what, pageSize, currentPage, out pageCount, out rowCount, sortColumns, direction);
        }

        public List<SubsResource> GetAllSubresources(int id)
        {
            var result = db.SubsResources
                .Include(m => m.Parent)
                .Include(m => m.Subresource)
                .Where(m => m.ResourceID == id)
                .ToList();
            return result;
        }

        public List<SubsResource> GetAllSubresourcesByCode(string resourceCode)
        {
            var result = db.SubsResources
                .Include(m => m.Parent)
                .Include(m => m.Subresource)
                .Where(m => m.Parent.ResourceCode == resourceCode)
                .ToList();
            return result;
        }

        public SubsResource AddSubresource(SubsResource sub)
        {
            var newRec = db.SubsResources.Add(sub);
            db.SaveChanges();
            return GetSubResourceByID(newRec.ID);
        }

        public SubsResource GetSubResourceByID(int subResourceId)
        {
            return db.SubsResources
                .Include(m => m.Parent)
                .Include(m => m.Subresource)
                .FirstOrDefault(m => m.ID == subResourceId);
        }

        public void DeleteSubresource(int subResourceId)
        {
            db.SubsResources.Remove(db.SubsResources.FirstOrDefault(m => m.ID == subResourceId));
            db.SaveChanges();
        }

        public void DeleteQAParamResources(int qaParamId)
        {
            db.QAParamResources.Remove(db.QAParamResources.FirstOrDefault(m => m.ID == qaParamId));
            db.SaveChanges();
        }
    }
}
