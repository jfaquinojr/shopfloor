﻿using ShopFloor.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace ShopFloor.Managers
{
    public class QAParamManager : ShopFloorEntityManager<QAParam>, ISearchable2
    {
        public QAParamManager() : base(m => m.ID)
        {
            _search = (what) =>
            {
                return db.QAParams.Where(m => m.QAParaCode.Contains(what) || m.QAParaDesc.Contains(what));
            };
        }

        public IList Search(string what)
        {
            return _search(what).ToList();
        }

        public IList Search(string what, int pageSize, int currentPage, out int filteredCount, out int totalCount, string sortColumns, string dir)
        {
            return SearchEntity(what, pageSize, currentPage, out filteredCount, out totalCount, sortColumns, dir);
        }
    }
}
