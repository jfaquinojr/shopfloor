﻿using ShopFloor.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ShopFloor.Managers
{
    public class RouteOperationResourcesManager : ShopFloorEntityManager<RouteOperationResource>, ISearchable2
    {
        public RouteOperationResourcesManager()
            : base(m => m.ID)
        {
            _search = (what) =>
            {
                return db.RouteOperationResources
                .Include(m => m.RouteOperation)
                .Include(m => m.RouteOperation.Route)
                .Include(m => m.RouteOperation.Operation)
                .Include(m => m.Resource)
                .Where(m => m.RouteOperation.Route.RouteCode.Contains(what) || m.RouteOperation.Route.RouteDesc.Contains(what)
                    || m.RouteOperation.Operation.OperCode.Contains(what) || m.RouteOperation.Operation.OperDesc.Contains(what)
                    || m.Resource.ResourceCode.Contains(what) || m.Resource.ResourceDesc.Contains(what))
                .OrderBy(m => m.RouteOperation.Route.RouteCode)
                .ThenBy(m => m.RouteOperation.Operation.OperCode)
                .ThenBy(m => m.Sequence);
            };
        }

        public override RouteOperationResource GetByID(int id)
        {
            return db.RouteOperationResources
                .Include(m => m.RouteOperation)
                .Include(m => m.RouteOperation.Route)
                .Include(m => m.RouteOperation.Operation)
                .Include(m => m.Resource)
                .Single(m => m.ID == id);
        }

        public override int Create(RouteOperationResource entity)
        {
            entity.CreatedOn = DateTime.Now;
            entity.Sequence = GetNextSequence(m => m.Sequence, m => m.RouteOperationID == entity.RouteOperationID);
            entity.ID = base.Create(entity);

            SetLastTag(entity);

            return entity.ID;
        }

        private void SetLastTag(RouteOperationResource entity)
        {
            if (entity.LastTag == true)
            {
                // set the only LastTag in current group
                foreach (var e in db.RouteOperationResources.Where(m => m.RouteOperationID == entity.RouteOperationID && m.ID != entity.ID))
                {
                    e.LastTag = false;
                }
                db.SaveChanges();
            }
        }

        public override void Save(RouteOperationResource entity)
        {
            entity.ModifiedOn = DateTime.Now;
            base.Save(entity);

            SetLastTag(entity);
        }

        public override List<RouteOperationResource> GetAll()
        {
            return db.RouteOperationResources
                .Include(m => m.RouteOperation)
                .Include(m => m.Resource)
                .Include(m => m.RouteOperation.Route)
                .Include(m => m.RouteOperation.Operation)
                .OrderBy(m => m.RouteOperation.Route.RouteCode)
                .ThenBy(m => m.RouteOperation.Operation.OperCode)
                .ThenBy(m => m.Sequence)
                .ToList();
        }


        public bool IsDuplicate(int routeId, int operationId, RouteOperationResource entity)
        {
            return db.RouteOperationResources.Any(m => m.RouteOperation.RouteID == routeId 
                && m.RouteOperation.OperationID == operationId
                && m.ResourceID == entity.ResourceID
                && m.ID != entity.ID);
        }

        public List<RouteOperationResource> GetByRouteID(int routeId)
        {
            var result = (from ror in db.RouteOperationResources
                          join ro in db.RouteOperations on ror.RouteOperationID equals ro.ID
                          join r in db.Routes on ro.RouteID equals r.ID
                          where ro.RouteID == routeId
                          orderby ro.Sequence, ror.Sequence
                          select ror)
                          .Include(m => m.RouteOperation)
                          .Include(m => m.RouteOperation.Operation)
                          .Include(m => m.Resource)
                          .ToList();
            return result;
        }

        public List<RouteOperationResource> GetByItemCode(string itemCode)
        {
            var result = (from ror in db.RouteOperationResources
                          join ro in db.RouteOperations on ror.RouteOperationID equals ro.ID
                          join r in db.Routes on ro.RouteID equals r.ID
                          join ir in db.ItemRoutes on r.ID equals ir.RouteID
                          where ir.ItemCode == itemCode
                          orderby ro.Sequence, ror.Sequence
                          select ror)
                          .Include(m => m.RouteOperation)
                          .Include(m => m.RouteOperation.Operation)
                          .Include(m => m.Resource)
                          .ToList();
            return result;
        }

        public List<RouteOperationResource> GetByRouteID(int routeId, int operationId)
        {
            var result = (from r in db.RouteOperationResources
                          join ro in db.RouteOperations on r.RouteOperationID equals ro.ID
                          join o in db.Operations on ro.OperationID equals o.ID
                          where ro.RouteID == routeId && o.ID == operationId
                          orderby ro.Sequence, r.Sequence
                          select r)
                          .Include(m => m.RouteOperation)
                          .Include(m => m.RouteOperation.Operation)
                          .Include(m => m.Resource)
                          .ToList();
            return result;
        }

        public IList Search(string what)
        {
            return _search(what)
                .ToList();
        }

        public IList Search(string what, int pageSize, int currentPage, out int pageCount, out int rowCount, string sortColumns, string direction)
        {
            return SearchEntity(what, pageSize, currentPage, out pageCount, out rowCount, sortColumns, direction);
        }

        public string GetResourceDesc(int id)
        {
            return db.Resources.FirstOrDefault(m => m.ID == id).ResourceDesc;
        }

        public int GetOrCreateRouteOperation(int routeId, int operationId, int createdBy)
        {
            // find existing Route Operation
            if(db.RouteOperations.Any(m=>m.RouteID == routeId && m.OperationID == operationId))
            {
                return db.RouteOperations.Single(m=>m.RouteID==routeId && m.OperationID==operationId).ID;
            }

            // if not found, create new
            var routeOp = new RouteOperation
            {
                RouteID = routeId,
                OperationID = operationId,
                CreatedBy = createdBy,
                CreatedOn = DateTime.Now
            };

            return new RouteOperationsManager().Create(routeOp);
        }

        public void SetLastTag(int routeOperationResourceId)
        {
            var entity = GetByID(routeOperationResourceId);

            // get all route resources for route
            var routeResources = db.RouteOperationResources
                .Include(m => m.RouteOperation)
                .Where(m => m.RouteOperation.RouteID == entity.RouteOperation.RouteID);

            foreach(var routeRes in routeResources)
            {
                // clear LastTag
                routeRes.LastTag = false;
            }

            // except target
            entity.LastTag = true;

            db.SaveChanges();
        }
    }
}
