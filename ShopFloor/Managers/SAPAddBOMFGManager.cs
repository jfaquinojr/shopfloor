﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.SAP.Managers
{
	public class SAPAddBOMFGManager : SAPRawMaterialsManager
	{
		public SAPAddBOMFGManager()
		{
			/*
				Add the following ItmsGrpCod to the filter for the module Edit
				BOM Details / Item Assignment tab/ Add BOM FG screen.
				1. 103 -- already defined in SAPRawMaterialsManager
				2. 106 -- already defined in SAPRawMaterialsManager
				3. 129
				4. 130
			*/
			var _tmp = new List<short>();
			_tmp.AddRange(groupCodes);
			_tmp.Add(129);
			_tmp.Add(130);
			groupCodes = _tmp.ToArray();
		}
	}
}
