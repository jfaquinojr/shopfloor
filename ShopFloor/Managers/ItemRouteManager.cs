﻿using ShopFloor.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Collections;

namespace ShopFloor.Managers
{
    public class ItemRouteManager : ShopFloorEntityManager<ItemRoute>, ISearchable2
    {
        public ItemRouteManager() : base(m => m.ID)
        {
            this._search = (what) =>
            {
                return db.ItemRoutes
                    .Include(m => m.Route)
                    .Where(m => m.ItemCode.Contains(what) || m.ItemName.Contains(what));
            };
        }

        public override ItemRoute GetByID(int id)
        {
            return db.ItemRoutes
                .Include(m => m.Route)
                .FirstOrDefault(m => m.ID == id);
        }

        public List<ItemRoute> GetAllByRouteID(int routeId)
        {
            return db.ItemRoutes
                .Include(m => m.Route)
                .Where(m => m.RouteID == routeId)
                .ToList();
        }

        public List<ItemRoute> GetItemRoutesByItemCode(string itemcode)
        {
            var result = db.ItemRoutes
                .Include(m => m.Route)
                .Where(m => m.ItemCode == itemcode);
            return result.ToList();
        }

        public IList Search(string what)
        {
            return _search(what).ToList();
        }

        public IList Search(string what, int pageSize, int currentPage, out int filteredCount, out int totalCount, string sortColumns, string dir)
        {
            return SearchEntity(what, pageSize, currentPage, out filteredCount, out totalCount, sortColumns, dir);
        }
    }
}
