﻿using ShopFloor.Entities;
using ShopFloor.SAP.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Managers
{
    public static class ManagerFactory
    {
        public static ISearchable2 Create(string table, string filter) 
        {
            int i;
            switch (table)
            {
                case Constants.CONST_SEARCHTABLE_ITEM :
                    return new SAP.Managers.SAPItemManager();
				case Constants.CONST_SEARCHTABLE_ITEMASSIGNMENT:
					return new SAP.Managers.SAPItemAssignmentManager();
				//case "resourcemanager":
				//    return new ResourceManager();
				case Constants.CONST_SEARCHTABLE_ROUTE:
                    return new RoutesManager();
                case Constants.CONST_SEARCHTABLE_USER:
                    return new UsersManager();
                case Constants.CONST_SEARCHTABLE_BOM:
                    return new BOMManager();
                case Constants.CONST_SEARCHTABLE_MO:
                    return new MOHeaderManager();
                case Constants.CONST_SEARCHTABLE_BOMDETAIL:
                    int.TryParse(filter, out i);
                    return new BOMDetailManager(i);
                case Constants.CONST_SEARCHTABLE_OPERATION:
                    return new OperationManager();
                case Constants.CONST_SEARCHTABLE_ROUTEOPERATION:
                    return new RouteOperationsManager();
                case Constants.CONST_SEARCHTABLE_ROUTEOPERATIONRESOURCES:
                    return new RouteOperationResourcesManager();
                case Constants.CONST_SEARCHTABLE_ADDITIONALREASON:
                    return new AdditionalReasonManager();
                case Constants.CONST_SEARCHTABLE_ACTIVITYTYPES:
                    return new ActivityTypeManager();
                case Constants.CONST_SEARCHTABLE_RESOURCECATEGORY:
                    return new ResourceCategoryManager();
                case Constants.CONST_SEARCHTABLE_RESOURCES:
                    return new ResourceManager();
                case Constants.CONST_SEARCHTABLE_RUNRATE:
                    return new RunRateManager();
                case Constants.CONST_SEARCHTABLE_RUNRATEREASON:
                    return new RunRateReasonManager();
				case Constants.CONST_SEARCHTABLE_RAWMATERIALS:
					return new SAP.Managers.SAPRawMaterialsManager();
				case Constants.CONST_SEARCHTABLE_ADDBOMFG:
					return new SAP.Managers.SAPAddBOMFGManager();
				case Constants.CONST_SEARCHTABLE_QAINSPMETHOD:
                    return new QAInspMethodManager();
                case Constants.CONST_SEARCHTABLE_QACRITERIA:
                    return new QACriteriaManager();
                case Constants.CONST_SEARCHTABLE_ITEMROUTES:
                    return new ItemRouteManager();
                case Constants.CONST_SEARCHTABLE_QAPARAMS:
                    return new QAParamManager();
                case Constants.CONST_SEARCHTABLE_QAAPPS:
                    return new QAAppManager();
                case Constants.CONST_SEARCHTABLE_DATACAPTURE:
                    return new DataCaptureManager();

                default:
                    return new SAPItemManager();
            }
        }
    }
}
