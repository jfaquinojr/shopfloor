﻿using ShopFloor.Entities;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Entity;

namespace ShopFloor.Managers
{
    public class TransmittalManager : ShopFloorEntityManager<Transmittal>
    {
        public TransmittalManager() : base(m => m.ID)
        {

        }

        public IQueryable<Transmittal> GetByMOHeaderID(int moHeaderId)
        {
            var result = db.Transmittals
                .Include(m => m.MODetailFgg)
                .Where(m => m.MODetailFgg.MOHeaderID == moHeaderId);

            return result;
        }
    }
}
