﻿using ShopFloor.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using ShopFloor.SAP.Entities;
using System.Data.Entity;

namespace ShopFloor.SAP.Managers
{
    public class SAPRawMaterialsManager : SAPItemManager
    {
		protected short[] priceList = new short[] { };

		public SAPRawMaterialsManager() 
        {
            // items that belong to these groupcodes are identified as Raw Materials
            groupCodes = new short[] { 102, 103, 104, 105, 106, 107, 108, 109, 119, 126, 120, 121, 122, 123, 125, 127 };

			_search = (what) =>
			{
				var list = (from p in db.ItemPrices
							join i in db.Items on p.ItemCode equals i.ItemCode
							where groupCodes.Contains(i.ItmsGrpCod) && p.PriceList == 3 
							&& (i.ItemCode.Contains(what) || i.ItemName.Contains(what))
							select i);

				return list;
			};
		}

		public override List<Item> GetAll()
		{
			var list = (from p in db.ItemPrices
						join i in db.Items on p.ItemCode equals i.ItemCode
						where groupCodes.Contains(i.ItmsGrpCod) && p.PriceList == 3
						select i);
			return list.ToList();
		}

		public override ItemPrice GetPrice(string itemCode)
		{
			return db.ItemPrices.FirstOrDefault(m => m.ItemCode == itemCode && m.PriceList == 3);
		}
	}
}



