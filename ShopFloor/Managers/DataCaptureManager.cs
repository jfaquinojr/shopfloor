﻿using ShopFloor.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;

namespace ShopFloor.Managers
{
    public class DataCaptureManager : ShopFloorEntityManager<ShopFloorDataCapture>, ISearchable2
    {
        public DataCaptureManager()
            : base(m => m.ID)
        {
            _search = (what) =>
            {
				var result = db.ShopFloorDataCaptures
				//.Include(m => m.MODetailRsc)
				//.Include(m => m.MODetailRsc.MODetailFgg)
				//.Include(m => m.MODetailRsc.MODetailFgg.MOHeader)
				.Where(m => m.MONumber.ToString().Contains(what)
					|| m.ResourceCode.Contains(what));

                var returnset = (from t in result.ToList()
                                 join u in db.Users on t.UserID equals u.ID
                                 join a in db.ActivityTypes on t.ActivityTypeID equals a.ID
                                 let username = t.Username = u.UserName
                                 let status = t.Status = GetShopFloorStatus(t.MODetailRSCID)
                                 let activity = t.ActivityType = a
                                 select t);

                return returnset.AsQueryable();
            };
        }

        private string GetShopFloorStatus(int? moDetailRscId)
        {
            var rsc = db.MODetailRscs.FirstOrDefault(m => m.ID == (moDetailRscId ?? 0));
            if(rsc == null)
            {
                return "Completed";
            }

            bool? onStream = rsc.OnStreamTag;
            bool? closed = rsc.ClosedTag;


            string retval = "Unknown";

            if (onStream.HasValue)
            {
                if (onStream == true)
                {
                    retval = "Started";
                }
                else
                {
                    retval = "Stopped";
                }
            }

            if (closed.GetValueOrDefault(false))
            {
                retval = "Completed";
            }

            return retval;
        }

        public MODetailRsc GetMODetailRSC(int? id)
        {
            return db.MODetailRscs.FirstOrDefault(m => m.ID == (id ?? 0));
        }

        public override ShopFloorDataCapture GetByID(int id)
        {
			var result = db.ShopFloorDataCaptures
				//.Include(m => m.MODetailRsc)
				//.Include(m => m.MODetailRsc.MODetailFgg)
				//.Include(m => m.MODetailRsc.MODetailFgg.MOHeader)
				//.Include(m => m.MODetailRsc.MODetailFgg.ItemRoute)
				//.Include(m => m.MODetailRsc.MODetailFgg.MOHeader.MOType)
				.Where(m => m.ID == id);

			var returnset = (from t in result.ToList()
							 join u in db.Users on t.UserID equals u.ID
							 join a in db.ActivityTypes on t.ActivityTypeID equals a.ID
							 let username = t.Username = u.UserName
							 let status = t.Status = GetShopFloorStatus(t.MODetailRSCID)
							 let activity = t.ActivityType = a
							 select t);


			return returnset.Single();
        }

        public void CreateFromFGG(int fggId, string username)
        {
            int userId = GetUserIDByUsername(username);
            var dbRsc = new MODetailRSCManager();

            var resources = dbRsc.GetAllByFGGID(fggId);
            foreach (var res in resources)
            {
                var capture = new ShopFloorDataCapture
                {
                    MODetailRSCID = res.ID,
                    CreatedBy = userId,
                    CreatedOn = DateTime.Now,
                };
                Create(capture);
            }
        }

        public List<MODetailItm> GetRMAddlItems(int MOHeaderID)
        {
            return (from fgg in db.MODetailFggs
                    join itm in db.MODetailItms on fgg.ID equals itm.MODetailFGGID
                    join mo in db.MOHeaders on fgg.MOHeaderID equals mo.ID
                    join t in db.MOTypes on mo.MOTypeID equals t.ID
                    where mo.ID == MOHeaderID && t.MOTypeCode == Constants.CONST_MOTYPES_MO
                    select itm
                    ).ToList();
        }


        public List<ShopFloorDataCapture> GetAllByMOID(int mOHeaderID)
        {
            return (from fgg in db.MODetailFggs
                    join rsc in db.MODetailRscs on fgg.ID equals rsc.MODetailFGGID
                    join cap in db.ShopFloorDataCaptures on rsc.ID equals cap.MODetailRSCID
                    //join mo in db.MOHeaders on fgg.MOHeaderID equals mo.ID
                    where fgg.MOHeaderID == mOHeaderID
                    select cap
                    )
                    .ToList();
        }

        public List<ShopFloorDataCapture> GetAllByFGGID(int id)
        {
            return (from fgg in db.MODetailFggs
                    join rsc in db.MODetailRscs on fgg.ID equals rsc.MODetailFGGID
                    join cap in db.ShopFloorDataCaptures on rsc.ID equals cap.MODetailRSCID
                    //join mo in db.MOHeaders on fgg.MOHeaderID equals mo.ID
                    where fgg.ID == id
                    select cap
                    )
                    .Include(m => m.ShopFloorAddls)
                    .ToList();
        }



        public ShopFloorRej GetRejectByID(int rejectId)
        {
            return db.ShopFloorRejs.Single(m => m.ID == rejectId);
        }

        public void DeleteReject(int rejectId, int[] shopFloorIDs)
        {
            var e = GetRejectByID(rejectId);
            var result = db.ShopFloorRejs
                .Where(m => shopFloorIDs.Contains(m.ShopFloorID) && m.QACriteriaID == e.QACriteriaID);

            db.ShopFloorRejs.RemoveRange(result);
            db.SaveChanges();
        }

        public List<ShopFloorRej> GetRejects(int shopFloorID)
        {
            return db.ShopFloorRejs
                .Include(m => m.QACriterion)
                .Where(m => m.ShopFloorID == shopFloorID)
                .ToList();
        }

        public List<ShopFloorAddl> GetAddlRawMaterials(int shopFloorID)
        {
            return db.ShopFloorAddls
                .Where(m => m.ShopFloorID == shopFloorID)
                .ToList();
        }

        public ShopFloorRej GetRejectByShopFloorID(int shopFloorID, int qaCriteriaID)
        {
            return db.ShopFloorRejs
                .Include(m => m.QACriterion)
                .FirstOrDefault(m => m.ShopFloorID == shopFloorID && m.QACriteriaID == qaCriteriaID);
        }

        private ShopFloorEntityManager<ShopFloorQAPar> dbQAP = new ShopFloorEntityManager<ShopFloorQAPar>(m => m.ID);
        public void CreateShopFloorQAParam(ShopFloorQAPar sfQAPar)
        {
            dbQAP.Create(sfQAPar);
        }

        public void SaveShopFloorQAParam(ShopFloorQAPar sfQAPar)
        {
            dbQAP.Save(sfQAPar);
        }

        public void CreateReject(ShopFloorRej model, int[] shopFloorIDs)
        {
            foreach (int id in shopFloorIDs)
            {
                var newRec = new ShopFloorRej();
                newRec.ShopFloorID = id;
                newRec.QACriteriaID = model.QACriteriaID;
                newRec.Reject = model.Reject;
                newRec.CreatedBy = model.CreatedBy;
                newRec.CreatedOn = model.CreatedOn;
                db.ShopFloorRejs.Add(newRec);
            }

            db.SaveChanges();
        }

        public void SaveReject(ShopFloorRej model, int[] shopFloorIDs)
        {
            // save Reject value for all records with same QACriteria in shopFloorIDs
            var result = db.ShopFloorRejs
                .Where(m => shopFloorIDs.Contains(m.ShopFloorID) && m.QACriteriaID == model.QACriteriaID)
                .ToList();
            foreach (var entity in result)
            {
                entity.ModifiedBy = model.ModifiedBy;
                entity.ModifiedOn = model.ModifiedOn;
                entity.Reject = model.Reject;
            }
            db.SaveChanges();
        }

        public void AssignSubResource(SubsResource model, int[] shopFloorIDs)
        {
            var d = DateTime.Now;
            var result = db.ShopFloorDataCaptures
                .Where(m => shopFloorIDs.Contains(m.ID));
            foreach (var entity in result)
            {
                if (model.SubResourceID > 0)
                {
                    entity.SubResourceID = model.SubResourceID;
                }
                else
                {
                    entity.SubResourceID = null;
                }

                entity.ModifiedBy = model.ModifiedBy;
                entity.ModifiedOn = d;
            }
            db.SaveChanges();
        }

        public IList Search(string what)
        {
            return _search(what)
                .ToList();
        }

        public IList Search(string what, int pageSize, int currentPage, out int filteredCount, out int totalCount, string sortColumns, string dir)
        {
            return SearchEntity(what, pageSize, currentPage, out filteredCount, out totalCount, sortColumns, dir);
        }

        public override void Remove(int id)
        {
            try
            {
                RemoveAllChild(id);
                db.ShopFloorDataCaptures.Remove(GetByID(id));
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                throw;
            }
        }

        public void RemoveByRSCID(int rscId)
        {
            try
            {
                if (db.ShopFloorDataCaptures.Any(m => m.MODetailRSCID == rscId))
                {
                    foreach (var entity in db.ShopFloorDataCaptures.Where(m => m.MODetailRSCID == rscId).ToList())
                    {
                        RemoveAllChild(entity.ID);
                        db.SaveChanges();
                        db.ShopFloorDataCaptures.Remove(GetByID(entity.ID));
                        db.SaveChanges();
                    }
                    
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                throw;
            }

        }

        private void RemoveAllChild(int id)
        {
            db.ShopFloorAddls.RemoveRange(db.ShopFloorAddls.Where(m => m.ShopFloorID == id));
            db.ShopFloorQAPars.RemoveRange(db.ShopFloorQAPars.Where(m => m.ShopFloorID == id));
            db.ShopFloorRejs.RemoveRange(db.ShopFloorRejs.Where(m => m.ShopFloorID == id));
            
        }

    }
}
