﻿using ShopFloor.DTO;
using ShopFloor.Entities;
using ShopFloor.Schedule;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace ShopFloor.Managers
{
    public class MOResourceScheduleManager : ShopFloorEntityManager<MOResourceSchedule>
    {
        public MOResourceScheduleManager() : base(m => m.ID) { }

        public List<MOReschedule> GetActiveMOSchedule()
        {

            var minmaxDates =
            from sched in db.MOResourceSchedules
            group sched by sched.MOHeaderID into g
            select new
            {
                MOHeaderID = g.Key,
                MinStartDate = g.Min(m => m.StartDateTime),
                MaxEndDate = g.Max(m => m.EndDateTime)
            };


            var result = (
                from mo in db.MOHeaders
                    .Include(m => m.MOType)
                    .AsEnumerable() // <-- because of string.Join below
                join dates in minmaxDates on mo.ID equals dates.MOHeaderID
                where mo.IsScheduled == true 
                orderby mo.MOPriority
                let fggs = mo.FinishedGoods.Where(f => f.MOHeaderID == mo.ID)
                select new MOReschedule
                {
                    MOHeaderID = mo.ID,
                    MOPriority = mo.MOPriority.Value,
                    MONumber = mo.MONumber,
                    FGCode = string.Join(", ", fggs.Select(f => f.ItemCode)), 
                    FGName = string.Join(", ", fggs.Select(f => f.ItemName)),
                    MinStartDate = dates.MinStartDate,
                    MaxEndDate = dates.MaxEndDate,
                    EstRunTime = (double) fggs.Sum(f=>f.Resources.Sum(r=>r.EstTime + r.SetupTime + r.StockTime)),
                    MinDeliveryDate = fggs.Min(f => f.DeliveryDate)
                }
            );

            return result.ToList();
        }

        public void CreateSchedule(IEnumerable<MODetailRsc> schedules)
        {
            foreach(MODetailRsc sched in schedules)
            {
                this.Create(new MOResourceSchedule
                {
                    StartDateTime = sched.StartDate,
                    EndDateTime = sched.EndDate,
                    CreatedBy = sched.CreatedBy,
                    CreatedOn = DateTime.Now,
                    MODetailRSCID = sched.ID,
                    MOHeaderID = sched.MODetailFgg?.MOHeaderID ?? 0,
                    ResourceID = sched.ResourceID ?? 0
                });
            }
        }

        public void DeleteSchedByFGGID(int? id)
        {
            if (id == null)
            {
                return;
            }

            var scheds = (from sched in db.MOResourceSchedules
                          join rsc in db.MODetailRscs on sched.MODetailRSCID equals rsc.ID
                          where rsc.MODetailFGGID == id
                          select sched);
            db.MOResourceSchedules.RemoveRange(scheds);
        }

        public void DeleteSchedByRSCID(int rscId)
        {
            var scheds = db.MOResourceSchedules.Where(m => m.MODetailRSCID == rscId);
            db.MOResourceSchedules.RemoveRange(scheds);
        }

        /// <summary>
        /// Get the maximum (active) EndDate values for a Resource.
        /// </summary>
        /// <returns>Returns a dictionary collection containing Key=ResourceID and Value=MaxEndDateTime</returns>
        public Dictionary<int, DateTime> GetMaxEndDates()
        {
            var maxdates = (from sched in db.MOResourceSchedules
                            group sched by sched.ResourceID into g
                            select new { ResourceID = g.Key, EndDateTime = g.Max(e => e.EndDateTime) });

            var ret = new Dictionary<int, DateTime>();
            foreach(var d in maxdates)
            {
                if (d.EndDateTime.HasValue)
                    ret.Add(d.ResourceID, d.EndDateTime.Value);
            }

            return ret;
        }


    }
}
