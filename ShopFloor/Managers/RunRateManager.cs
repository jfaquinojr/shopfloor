﻿using ShopFloor.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ShopFloor.Managers
{
    public class RunRateManager : ShopFloorEntityManager<RunRate>, ISearchable2
    {
        public RunRateManager()
            : base(m => m.ID)
        {
            _search = (what) =>
            {
                return db.RunRates
                    .Include(m => m.Resource)
                    .Include(m => m.ItemRoute)
                    .Include(m => m.ItemRoute.Route)
                    .Where(m => m.Resource.ResourceCode.Contains(what));
            };
        }

        public IList Search(string what)
        {
            return _search(what).ToList();
        }

        public IList Search(string what, int pageSize, int currentPage, out int filteredCount, out int totalCount, string sortColumns, string dir)
        {
            return SearchEntity(what, pageSize, currentPage, out filteredCount, out totalCount, sortColumns, dir);
        }
    }
}
