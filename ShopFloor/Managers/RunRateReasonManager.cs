﻿using ShopFloor.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Managers
{
    public class RunRateReasonManager : ShopFloorEntityManager<RunRateReason>, ISearchable2
    {
        public RunRateReasonManager()
            : base(m => m.ID)
        {
            _search = (what) =>
            {
                return db.RunRateReasons.Where(m => m.ReasonCode.Contains(what) || m.ReasonName.Contains(what));
            };
        }

        public IList Search(string what)
        {
            return _search(what).ToList();
        }

        public IList Search(string what, int pageSize, int currentPage, out int filteredCount, out int totalCount, string sortColumns, string dir)
        {
            return SearchEntity(what, pageSize, currentPage, out filteredCount, out totalCount, sortColumns, dir);
        }
    }
}
