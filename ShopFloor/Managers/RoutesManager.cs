﻿using ShopFloor.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ShopFloor.Managers
{
    public class RoutesManager : ShopFloorEntityManager<Route>, ISearchable2
    {
        public RoutesManager()
            : base(m => m.ID)
        {
            _search = (what) =>
            {
                return db.Routes.Where(m => m.RouteCode.Contains(what) || m.RouteDesc.Contains(what));
            };
        }

        public List<Route> GetAllRoutesWithItems()
        {
            return db.Routes
                //.Where(m => m.ItemCode != null)
                .ToList();
        }

        public List<ItemRoute> GetItemRoutes()
        {
            return db.ItemRoutes.ToList();
        }

        public List<ItemRoute> GetItemRoutesByID(int routeId)
        {
            return db.ItemRoutes.ToList();
        }

        public List<Route> GetActiveRoutes()
        {
            var result = db.Routes
                .Where(m => m.Active == true)
                .ToList();
            return result;
        }

        public List<Operation> GetOperationsByRouteID(int id)
        {
            var list = (from r in db.RouteOperations
                        join o in db.Operations on r.OperationID equals o.ID
                        where r.RouteID == id
                        select o);
            return list.ToList();
        }

        public IList Search(string what)
        {
            return _search(what).ToList();
        }

        public IList Search(string what, int pageSize, int currentPage, out int pageCount, out int rowCount, string sortColumns, string direction)
        {
            return SearchEntity(what, pageSize, currentPage, out pageCount, out rowCount, sortColumns, direction);
        }

        public Route GetByItemCode(string itemCode)
        {
            var ir = db.ItemRoutes
                .Include(m => m.Route)
                .FirstOrDefault(m => m.ItemCode == itemCode);
            return ir?.Route;
        }

        public ItemRoute GetItemRouteByItemCode(string itemCode)
        {
            var ir = db.ItemRoutes
                .Include(m => m.Route)
                .FirstOrDefault(m => m.ItemCode == itemCode);
            return ir;
        }

        public Route GetByItemRouteID(int itemRouteId)
        {
            return db.ItemRoutes
                .Include(m => m.Route)
                .Where(m => m.ID == itemRouteId)
                .Select(m => m.Route)
                .First();
        }

		public List<Route> GetDownTimeRoutes()
		{
			return QueryAll()
				.Where(m => m.DownTimeTag == true && m.Active == true)
				.ToList();
		}


	}
}
