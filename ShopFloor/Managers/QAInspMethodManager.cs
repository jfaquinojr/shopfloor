﻿using ShopFloor.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ShopFloor.Managers
{
    public class QAInspMethodManager : ShopFloorEntityManager<QAInspMethod>, ISearchable2
    {
        public QAInspMethodManager()
            : base(m => m.ID)
        {
            _search = (what) =>
            {
                return db.QAInspMethods
                    .Where(m => m.QAInspMCode.Contains(what) && m.QAInspMDesc.Contains(what));
            };
        }

        public IList Search(string what)
        {
            return _search(what).ToList();
        }

        public IList Search(string what, int pageSize, int currentPage, out int filteredCount, out int totalCount, string sortColumns, string dir)
        {
            return SearchEntity(what, pageSize, currentPage, out filteredCount, out totalCount, sortColumns, dir);
        }
    }
}
