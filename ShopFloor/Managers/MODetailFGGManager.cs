﻿using ShopFloor.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ShopFloor.Managers
{
    public class MODetailFGGManager : ShopFloorEntityManager<MODetailFgg>
    {
        public MODetailFGGManager() : base(m => m.ID) { }

        public bool HasDuplicateItemCode(string itemcode, int MOHeaderId)
        {
            var ret = db.MODetailFggs.Any(m => m.MOHeaderID == MOHeaderId && m.ItemCode == itemcode);
            return ret;
        }

        public void DeleteRSCByFGGID(int id)
        {
            var rscs = db.MODetailRscs.Where(m => m.MODetailFGGID == id);
            var rscIDs = rscs.Select(m => m.ID).ToArray();

            db.ShopFloorDataCaptures.RemoveRange(db.ShopFloorDataCaptures.Where(f => rscIDs.Contains(f.MODetailRSCID.Value))); // delete child
            db.MODetailRscs.RemoveRange(rscs);
        }

        public void DeleteITMByFGGID(int id)
        {
            db.MODetailItms.RemoveRange(db.MODetailItms.Where(m => m.MODetailFGGID == id));
        }

        public List<MODetailFgg> GetByMOHeaderID(int moHeaderID)
        {
            return db.MODetailFggs
                .Where(m => m.MOHeaderID == moHeaderID)
                .ToList();
        }
    }
}
