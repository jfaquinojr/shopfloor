﻿using LinqKit;
using ShopFloor.Entities;
using ShopFloor.SAP.Managers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Managers
{
    public class BOMDetailManager : ShopFloorEntityManager<BillOfMaterialDetail>, ISearchable2
    {
        int BOMHeaderID = 0;
        public BOMDetailManager(int bomHeaderId)
            : base(m => m.ID)
        {
            BOMHeaderID = bomHeaderId;
            _search = (what) => {
                return db.BillOfMaterialDetails.Where(m => m.BOMHeaderID == this.BOMHeaderID);
            };
        }

        public BOMDetailManager() : this(0)
        {
            
        }

        public override List<BillOfMaterialDetail> GetAll()
        {
            if(BOMHeaderID > 0)
                return db.BillOfMaterialDetails.Where(m => m.BOMHeaderID == BOMHeaderID).ToList();

            return db.BillOfMaterialDetails.ToList();
        }


        public IList Search(string what)
        {
            return _search(what).ToList();
        }

        public IList Search(string what, int pageSize, int currentPage, out int pageCount, out int rowCount, string sortColumns, string direction)
        {
            return SearchEntity(what, pageSize, currentPage, out pageCount, out rowCount, sortColumns, direction);
        }

        public List<BillOfMaterialDetail> GetByItemCode(string itemCode)
        {
            var result = (
            from bom in db.BillOfMaterials
            join itm in db.ItemBoms on bom.ID equals itm.BOMHeaderID
            join dtl in db.BillOfMaterialDetails on bom.ID equals dtl.BOMHeaderID
            where itm.ItemCode == itemCode
            select dtl);

            return result.ToList();
        }

        public static decimal ComputeUnitCost(decimal rmFactor, decimal uomFactor, decimal rmQty, decimal price)
        {
            return rmFactor * uomFactor * rmQty * price;
        }
    }
}
