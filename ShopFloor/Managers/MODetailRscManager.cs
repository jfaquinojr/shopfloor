﻿using ShopFloor.Entities;
using ShopFloor.Schedule;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace ShopFloor.Managers
{
    public class MODetailRSCManager : ShopFloorEntityManager<MODetailRsc>
    {
        public MODetailRSCManager() : base(m => m.ID) { }

        public override MODetailRsc GetByID(int id)
        {
            return db.MODetailRscs
                .Include(m=>m.MODetailFgg)
                .SingleOrDefault(m => m.ID == id);
        }

        public override int Create(MODetailRsc entity)
        {
            return base.Create(entity);
        }

        public void ProcessReschedule(int[] MOHeaderIDs, string username)
        {
            // empty active schedule to re-prioritize MOs
            db.Database.ExecuteSqlCommand("TRUNCATE TABLE dbo.MOResourceSchedule");

            DateTime dte = DateTime.Now;

            var dbSchedMgr = new MOResourceScheduleManager();
            var maxDates = new Dictionary<int, DateTime>();

            foreach (int id in MOHeaderIDs)
            {
                
                var resources = this.GetRSCForScheduling(id)
                    .Include(m => m.MODetailFgg)
                    .ToList();

                IScheduler scheduler = SchedulerFactory.CreateScheduler(id);
                scheduler.Reschedule(dte, resources, maxDates);

                SaveRSCList(resources);

                dbSchedMgr.CreateSchedule(resources);
            }
        }


        public void ProcessSchedule(int[] MOHeaderIDs, string username)
        {
            DateTime dte = DateTime.Now;

            var dbSchedMgr = new MOResourceScheduleManager();

            foreach (int id in MOHeaderIDs)
            {
                db.Database.ExecuteSqlCommand($"delete from dbo.MOResourceSchedule where MOHeaderID = {id}");

                var resources = this.GetRSCForScheduling(id)
                    .Include(m => m.MODetailFgg);

                dbSchedMgr.CreateSchedule(resources);
            }
        }

        public override void Save(MODetailRsc entity)
        {
            base.Save(entity);
        }

        /// <summary>
        /// Gets MODetailRscs ordered by Route Operation Sequence then by Route Operation Resources Sequence and finally by just Sequence
        /// </summary>
        /// <param name="id">The MOHeaderID</param>
        /// <returns>A list of MODetailRSC</returns>
        public List<MODetailRsc> GetAllByMOHeaderID(int id)
        {

            var result = (from fgg in db.MODetailFggs
                          join rsc in db.MODetailRscs on fgg.ID equals rsc.MODetailFGGID
                          orderby fgg.ID, rsc.Sequence
                          select rsc)
                          .Include(m => m.MODetailFgg);
            return result.ToList();

        }

        public List<MODetailRsc> GetAllByFGGID(int fggId)
        {
            var result = db.MODetailRscs
                .Include(m => m.MODetailFgg)
                .Where(m => m.MODetailFGGID == fggId)
                .OrderBy(m => m.Sequence);
            return result.ToList();
        }

        public List<MODetailRsc> GetAllByFGGIDs(int[] fggIDs)
        {
            var result = db.MODetailRscs
                .Include(m => m.MODetailFgg)
                .Include(m => m.MODetailFgg.MOHeader)
                .Where(m => fggIDs.Contains(m.MODetailFGGID))
                .OrderBy(m => m.Sequence);
            return result.ToList();
        }

        public void CreateFromFGG(MODetailFgg fgg)
        {
            // get RouteOperationResources 
            var dbRouteResources = new RouteOperationResourcesManager();
            var resources = dbRouteResources.GetByItemCode(fgg.ItemCode);

            // build the list of new RSC
            var list = new List<MODetailRsc>();
            foreach (var r in resources)
            {
                var rsc = new MODetailRsc
                {
                    MODetailFGGID = fgg.ID,
                    CreatedBy = fgg.CreatedBy,
                    CreatedOn = DateTime.Now,
                    SetupTime = r.Resource.SetupTime,
                    StockTime = r.Resource.StockTime,
                    ResourceID = r.ResourceID,
                    ResourceCode = r.Resource.ResourceCode,
                    ResourceDesc = r.Resource.ResourceDesc,
                    OperationCode = r.RouteOperation.Operation.OperCode,
                    Sequence = r.Sequence
                };
                list.Add(rsc);
            }

            // save to database
            foreach (var rsc in list)
            {
                Create(rsc);
            }

            RescheduleMO(fgg.MOHeaderID);
        }

		public void CreateFromFGGGangRun(MODetailFgg fgg)
		{
			// get RouteOperationResources 
			var dbRouteResources = new RouteOperationResourcesManager();
			var resources = dbRouteResources.GetByItemCode(fgg.ItemCode);

			// build the list of new RSC
			var list = new List<MODetailRsc>();
			foreach (var r in resources)
			{
				if(r.Resource.UnitTag.GetValueOrDefault(false) || IsNotAlreadyInMO(r.ResourceID, fgg.MOHeaderID))
				{
                    var rsc = new MODetailRsc
                    {
                        MODetailFGGID = fgg.ID,
                        CreatedBy = fgg.CreatedBy,
                        CreatedOn = DateTime.Now,
                        SetupTime = r.Resource.SetupTime,
                        StockTime = r.Resource.StockTime,
                        ResourceID = r.ResourceID,
                        ResourceCode = r.Resource.ResourceCode,
                        ResourceDesc = r.Resource.ResourceDesc,
                        OperationCode = r.RouteOperation.Operation.OperCode,
                        Sequence = r.Sequence
                    };
					list.Add(rsc);
				} 
			}

			// save to database
			foreach (var rsc in list)
			{
				Create(rsc);
			}

			RescheduleMO(fgg.MOHeaderID);
		}

		private bool IsNotAlreadyInMO(int resourceID, int moHeaderID)
		{
            var resourceCode = new ResourceManager().GetResourceCodeByID(resourceID);

			var found = db.MODetailRscs
				.Include(m => m.MODetailFgg)
				.Include(m => m.MODetailFgg.MOHeader)
				.Any(m => m.MODetailFgg.MOHeaderID == moHeaderID && m.ResourceCode == resourceCode);

			return !found;
		}

		public void RescheduleMO(int moHeaderId)
        {
            List<MODetailRsc> list = GetRSCForScheduling(moHeaderId)
                .ToList();

            // process schedule
            var maxDates = new MOResourceScheduleManager().GetMaxEndDates();
            IScheduler scheduler = SchedulerFactory.CreateScheduler(moHeaderId);
            scheduler.AfterSchedule = AfterSchedule;
            scheduler.Reschedule(DateTime.Now, list, maxDates);

            SaveRSCList(list);
        }

        private void AfterSchedule(ISchedule sched)
        {
            // assign OrigXXXX fields 
            var rsc = sched as MODetailRsc;
            if(rsc != null)
            {
                if(rsc.OrigFromDate == null) rsc.OrigFromDate = sched.StartDate;
                if (rsc.OrigToDate == null) rsc.OrigToDate = sched.EndDate;
                if (rsc.OrigEstTime == null) rsc.OrigEstTime = (decimal) sched.AddHours;
            }
        }

        public void UpdateMOSchedule(int moHeaderId)
        {
            List<MODetailRsc> list = GetRSCForScheduling(moHeaderId)
                .ToList();

            // process schedule
            var maxDates = new MOResourceScheduleManager().GetMaxEndDates();
            IScheduler scheduler = SchedulerFactory.CreateScheduler(moHeaderId);
            scheduler.UpdateSchedule(list, maxDates);

            SaveRSCList(list);
        }

        private void SaveRSCList(List<MODetailRsc> list)
        {
            foreach (var rsc in list)
            {
                Save(rsc);
            }
        }


        private IQueryable<MODetailRsc> GetRSCForScheduling(int moHeaderID)
        {
            return (from rsc in db.MODetailRscs
                    join fgg in db.MODetailFggs on rsc.MODetailFGGID equals fgg.ID
                    join mo in db.MOHeaders on fgg.MOHeaderID equals mo.ID
                    where mo.ID == moHeaderID && rsc.ClosedTag != true
                    orderby fgg.ID, rsc.Sequence
                    select rsc
                    );
        }


		public override void Remove(int id)
		{

			// remove all resource schedule
			var dbSched = new MOResourceScheduleManager();
			dbSched.DeleteSchedByRSCID(id);
			dbSched.Flush();

			base.Remove(id);
		}
	}
}

