﻿using ShopFloor.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ShopFloor.Managers
{
    public class QAParamResourceManager : ShopFloorEntityManager<QAParamResource>
    {
        public QAParamResourceManager() : base(m => m.ID) { }

        public override QAParamResource GetByID(int id)
        {
            return db.QAParamResources
                .Include(m => m.Resource)
                .Include(m => m.QAParam)
                .FirstOrDefault(m => m.ID == id);
        }

        public override List<QAParamResource> GetAll()
        {
            return db.QAParamResources
                .Include(m => m.Resource)
                .Include(m => m.QAParam)
                .ToList();
        }

        public List<QAParamResource> GetAllByResourceID(int id)
        {
            return db.QAParamResources
                .Include(m => m.Resource)
                .Include(m => m.QAParam)
                .Where(m => m.ResourceID == id)
                .ToList();
        }

        public List<QAParamResource> GetAllByResourceCode(string resourceCode)
        {
            return db.QAParamResources
                .Include(m => m.Resource)
                .Include(m => m.QAParam)
                .Where(m => m.Resource.ResourceCode == resourceCode)
                .ToList();
        }
    }
}
