﻿using ShopFloor.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Managers
{
    public class ShopFloorEntityManager<TEntity> : EntityManager<TEntity, ShopFloorDb, int>
        where TEntity : class
    {
        public ShopFloorEntityManager(Expression<Func<TEntity, int>> propertyID)
            : base(propertyID)
        {
            db = new ShopFloorDb();
        }

        public int GetUserIDByUsername(string username)
        {
            return db.Users.Single(m => m.UserName == username).ID;
        }
    }
}
