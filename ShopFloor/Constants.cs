﻿namespace ShopFloor
{
    public static class Constants
    {
        public const string CONST_SEARCHTABLE_URL = "/search/search?table=";

        public const string CONST_SEARCHTABLE_ROUTE = "route";
        public const string CONST_SEARCHTABLE_ITEM = "item";
		public const string CONST_SEARCHTABLE_ITEMASSIGNMENT = "CONST_SEARCHTABLE_ITEMASSIGNMENT";
		public const string CONST_SEARCHTABLE_USER = "user";
        public const string CONST_SEARCHTABLE_BOM = "bom";
        public const string CONST_SEARCHTABLE_BOMDETAIL = "bomdetail";
        public const string CONST_SEARCHTABLE_MO = "mo";
        public const string CONST_SEARCHTABLE_ROUTEOPERATION = "routeoperation";
        public const string CONST_SEARCHTABLE_ROUTEOPERATIONRESOURCES = "routeoperationresources";
        public const string CONST_SEARCHTABLE_OPERATION = "operation";
        public const string CONST_SEARCHTABLE_ADDITIONALREASON = "additionalreason";
        public const string CONST_SEARCHTABLE_ACTIVITYTYPES = "activitytypes";
        public const string CONST_SEARCHTABLE_RESOURCECATEGORY = "CONST_SEARCHTABLE_RESOURCECATEGORY";
        public const string CONST_SEARCHTABLE_RESOURCES = "CONST_SEARCHTABLE_RESOURCES";
        public const string CONST_SEARCHTABLE_RUNRATE = "CONST_SEARCHTABLE_RUNRATE";
        public const string CONST_SEARCHTABLE_RUNRATEREASON = "CONST_SEARCHTABLE_RUNRATEREASON";
        public const string CONST_SEARCHTABLE_RAWMATERIALS = "CONST_SEARCHTABLE_RAWMATERIALS";
		public const string CONST_SEARCHTABLE_ADDBOMFG = "CONST_SEARCHTABLE_ADDBOMFG";
		public const string CONST_SEARCHTABLE_QAINSPMETHOD = "CONST_SEARCHTABLE_QAINSPMETHOD";
        public const string CONST_SEARCHTABLE_QACRITERIA = "CONST_SEARCHTABLE_QACRITERIA";
        public const string CONST_SEARCHTABLE_ITEMROUTES = "CONST_SEARCHTABLE_ITEMROUTES";
        public const string CONST_SEARCHTABLE_QAPARAMS = "CONST_SEARCHTABLE_QAPARAMS";
        public const string CONST_SEARCHTABLE_QAAPPS = "CONST_SEARCHTABLE_QAAPPS";
        public const string CONST_SEARCHTABLE_DATACAPTURE = "CONST_SEARCHTABLE_DATACAPTURE";

		public const string CONST_MOTYPES_MO = "M";
		public const string CONST_MOTYPES_FILLER = "F";

		public const string CONST_FORMAT_DATETIME = "MM/dd/yyyy hh:mm tt";
		public const string CONST_FORMAT_TIME = "hh:mm:ss";
	}
}
