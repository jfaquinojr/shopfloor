namespace ShopFloor.DAL
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using ShopFloor.Entities;

    public class ShopFloorModel : DbContext
    {
        // Your context has been configured to use a 'ShopFloorModel' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'ShopFloor.DAL.ShopFloorModel' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'ShopFloorModel' 
        // connection string in the application configuration file.
        public ShopFloorModel()
            : base("name=ShopFloorDb")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserClass> UserClasses { get; set; }
        public virtual DbSet<Route> Routes { get; set; }
        public virtual DbSet<Operation> Operations { get; set; }
        public virtual DbSet<Resource> Resources { get; set; }

    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}