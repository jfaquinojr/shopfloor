﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.DTO
{
    public class MOReschedule
    {
        public int MOHeaderID { get; set; }
        public int MOPriority { get; set; }
        public int MONumber { get; set; }
        public string FGCode { get; set; }
        public string FGName { get; set; }
        public DateTime? MinStartDate { get; set; }
        public DateTime? MaxEndDate { get; set; }
        public double EstRunTime { get; set; }
        public DateTime? MinDeliveryDate { get; set; }
    }
}
