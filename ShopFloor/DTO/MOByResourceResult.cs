﻿using System;

namespace ShopFloor.DTO
{
    public class MOByResourceResult
    {
        public int ID { get; set; }
        public int ResourceID { get; set; }
        public string MOPriority { get; set; }
        public string MONumber { get; set; }
        public string FGCode { get; set; }
        public string FGName { get; set; }
        public DateTime? StartDateTime { get; set; }
        public DateTime? EndDateTime { get; set; }
        public decimal? EstRunTime { get; set; }
        public DateTime? DeliveryDate { get; set; }


        public string DisplayEstRunTime
        {
            get
            {
                return EstRunTime.HasValue ? EstRunTime.Value.ToString("######0.####") : "";
            }
        }
        public string DisplayEndDate
        {
            get
            {
                return EndDateTime.HasValue ? EndDateTime.Value.ToShortDateString() : "";
            }
        }
        public string DisplayEndTime
        {
            get
            {
                return EndDateTime.HasValue ? EndDateTime.Value.ToShortTimeString() : "";
            }
        }
        public string DisplayDeliveryDate
        {
            get
            {
                return DeliveryDate.HasValue ? DeliveryDate.Value.ToShortDateString() : "";
            }
        }
        public string DisplayStartDate
        {
            get
            {
                return StartDateTime.HasValue ? StartDateTime.Value.ToShortDateString() : "";
            }
        }
        public string DisplayStartTime
        {
            get
            {
                return StartDateTime.HasValue ? StartDateTime.Value.ToShortTimeString() : "";
            }
        }

        public bool IsScheduled { get; set; }

        public string Status { get; set; }
    }
}