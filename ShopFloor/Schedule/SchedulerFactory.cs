﻿using ShopFloor.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Schedule
{
    public class SchedulerFactory
    {
        public static IScheduler CreateScheduler(int moHeaderId)
        {
            var dbMOHeader = new MOHeaderManager();
            var mo = dbMOHeader.GetByID(moHeaderId);
            if (mo.GangRunTag.GetValueOrDefault(false))
            {
                return new GangRunScheduler();
            }

            return new Scheduler();
        }

        public static IScheduler CreateGanRunScheduler()
        {
            return new GangRunScheduler();
        }

        public static IScheduler CreateRegularScheduler()
        {
            return new Scheduler();
        }
    }
}
