﻿using System;

namespace ShopFloor.Schedule
{
    public interface ISchedule
    {
        DateTime StartDate { get; set; }
        DateTime EndDate { get; set; }
        double AddHours { get; }
        int GroupID { get; }
        int ScheduleID { get; }
    }
}