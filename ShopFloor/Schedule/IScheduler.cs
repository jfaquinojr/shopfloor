﻿using System;
using System.Collections.Generic;

namespace ShopFloor.Schedule
{
    public interface IScheduler
    {
        Action<ISchedule> BeforeSchedule { get; set; }
        Action<ISchedule> AfterSchedule { get; set; }
        void Reschedule(DateTime startDateTime, IEnumerable<ISchedule> _target, Dictionary<int, DateTime> maxDates);
        void UpdateSchedule(IEnumerable<ISchedule> _target, DateTime? defaultDateTime = null);
        void UpdateSchedule(IEnumerable<ISchedule> _target, Dictionary<int, DateTime> maxDates, DateTime? defaultDateTime = null);
    }
}
