﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Schedule
{
    public class GangRunScheduler : IScheduler
    {
        internal GangRunScheduler()
        {

        }

        public Action<ISchedule> AfterSchedule { get; set; }

        public Action<ISchedule> BeforeSchedule { get; set; }

        public void Reschedule(DateTime startDateTime, IEnumerable<ISchedule> _target, Dictionary<int, DateTime> maxDates)
        {
            var firstGroupId = _target.FirstOrDefault()?.GroupID ?? 0;
            var prevGroupId = 0;
            foreach (var sched in _target)
            {
                if (BeforeSchedule != null) BeforeSchedule(sched);

                processSchedule(startDateTime, sched, maxDates, firstGroupId, prevGroupId != sched.GroupID);
                prevGroupId = sched.GroupID;

                if (AfterSchedule != null) AfterSchedule(sched);

                // remember end date
                maxDates.Remove(sched.ScheduleID);
                maxDates.Add(sched.ScheduleID, sched.EndDate);
                startDateTime = sched.EndDate;
            }
        }

        public void UpdateSchedule(IEnumerable<ISchedule> _target, DateTime? defaultDateTime = null)
        {
            UpdateScheduleByGroupID(_target, 0, new Dictionary<int, DateTime>(), defaultDateTime);
        }

        public void UpdateSchedule(IEnumerable<ISchedule> _target, Dictionary<int, DateTime> maxDates, DateTime? defaultDateTime = null)
        {
            UpdateScheduleByGroupID(_target, 0, maxDates, defaultDateTime);
        }

        private void processSchedule(DateTime startDateTime, ISchedule sched, Dictionary<int, DateTime> maxDates, int firstGroupId, bool isFirstRow)
        {
            // set startdate
            DateTime _outDte;
            if (maxDates.TryGetValue(sched.ScheduleID, out _outDte))
            {
                if (_outDte >= startDateTime           // if maxdate is later
                    || (firstGroupId != sched.GroupID  // or use it on first row of succeeding FGs
                        && isFirstRow))
                {
                    startDateTime = _outDte;
                }
            }
            sched.StartDate = startDateTime;

            // set end date
            sched.EndDate = startDateTime.AddHours(sched.AddHours);
        }

        private void UpdateScheduleByGroupID(IEnumerable<ISchedule> _target, int groupID, Dictionary<int, DateTime> maxDates, DateTime? defaultDateTime = null)
        {
            var startDateTime = _target.FirstOrDefault().StartDate;
            var prevGroupId = 0;
            // set default time just in case UpdateSchedule was called prior to Reschedule.
            if (startDateTime == DateTime.MinValue && defaultDateTime.HasValue)
            {
                startDateTime = defaultDateTime.Value;
            }

            foreach (var sched in _target)
            {
                if (groupID == 0 || groupID == sched.GroupID)
                {
                    if (BeforeSchedule != null) BeforeSchedule(sched);

                    processSchedule(startDateTime, sched, maxDates, 0, prevGroupId != sched.GroupID);
                    prevGroupId = sched.GroupID;

                    if (AfterSchedule != null) AfterSchedule(sched);
                }

                // remember end date
                maxDates.Remove(sched.ScheduleID);
                maxDates.Add(sched.ScheduleID, sched.EndDate);
                startDateTime = sched.EndDate;
            }
        }
    }
}
