﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Schedule
{
    class CurrentSchedule : ISchedule
    {
        public double AddHours { get; set; }

        public DateTime EndDate { get; set; }
        public int GroupID { get; set; }
        
        public int ScheduleID { get; set; }

        public DateTime StartDate { get; set; }
    }
}
