﻿using ShopFloor.Schedule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopFloor.Tests
{
    public class BaseScheduleTests
    {
        protected List<TestSchedule> CreateTestData1()
        {
            return new TestSchedule[] {
                new TestSchedule { GroupID = 130, ScheduleID = 42, AddHours=3 }, 
                new TestSchedule { GroupID = 130, ScheduleID = 43, AddHours=4 }, 
                new TestSchedule { GroupID = 130, ScheduleID = 44, AddHours=3 }, 
                new TestSchedule { GroupID = 130, ScheduleID = 45, AddHours=1 }, 
                new TestSchedule { GroupID = 131, ScheduleID = 42, AddHours=3 }, 
                new TestSchedule { GroupID = 131, ScheduleID = 43, AddHours=4 }, 
                new TestSchedule { GroupID = 131, ScheduleID = 44, AddHours=3 }, 
                new TestSchedule { GroupID = 131, ScheduleID = 45, AddHours=1 }  
            }.ToList();
        }

        protected internal class TestSchedule : ISchedule
        {
            public double AddHours { get; set; }

            public DateTime EndDate { get; set; }

            public int GroupID { get; set; }

            public int ScheduleID { get; set; }

            public DateTime StartDate { get; set; }
        }
    }
}
