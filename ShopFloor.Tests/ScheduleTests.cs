﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShopFloor.Schedule;
using System;
using System.Collections.Generic;
using System.Linq;


namespace ShopFloor.Tests
{
    [TestClass]
    public class ScheduleTests : BaseScheduleTests
    {
        [TestMethod]
        public void Reschedule_TestCleanSampleData_EndDatesShouldFollow()
        {

            // recreate sample data
            var dte = new DateTime(2015, 10, 21, 14, 50, 0); // 10/21/2015 2:50PM - just like in the sample data
            var target = CreateTestData1();

            IScheduler scheduler = SchedulerFactory.CreateRegularScheduler();
            scheduler.Reschedule(dte, target, new Dictionary<int, DateTime>());

            /*

            the following is the actual data from client. group 130 follows an obvious pattern.
            

            Group   ID  From            To
            130     42  10/21  2:50PM   10/21  5:50PM  0
            130     43  10/21  5:50PM   10/21  9:50PM  1
            130     44  10/21  9:50PM   10/22 12:50AM  2
            130     45  10/22 12:50AM   10/22  1:50AM  3

            131     42  10/21  1:50AM   10/21  4:50AM  4
            131     43  10/21  4:50AM   10/22  8:50AM  5
            131     44  10/22  8:50AM   10/22 11:50AM  6
            131     45  10/22 11:50AM   10/22 12:50PM  7

            */

            TestSchedule t;
            t = target[0]; Assert.IsTrue(t.StartDate.Hour == 14); //  2PM
            t = target[1]; Assert.IsTrue(t.StartDate.Hour == 17); //  5PM
            t = target[2]; Assert.IsTrue(t.StartDate.Hour == 21); //  9PM
            t = target[3]; Assert.IsTrue(t.StartDate.Hour == 0);  // 12AM
            t = target[4]; Assert.IsTrue(t.StartDate.Hour == 1);  //  1AM
            t = target[5]; Assert.IsTrue(t.StartDate.Hour == 4);  //  4AM
            t = target[6]; Assert.IsTrue(t.StartDate.Hour == 8);  //  8AM
            t = target[7]; Assert.IsTrue(t.StartDate.Hour == 11); // 11AM
        }

        [TestMethod]
        public void Reschedule_HasPastMaxDates_MustUseDateTimeNow()
        {
            var maxDates = new Dictionary<int, DateTime>();
            var threeDaysAgo = DateTime.Now.AddDays(-3);
            var id = 43;
            maxDates.Add(id, threeDaysAgo);

            // recreate sample data
            var target = CreateTestData1();

            // reschedule
            IScheduler scheduler = SchedulerFactory.CreateRegularScheduler();
            var now = DateTime.Now;
            scheduler.Reschedule(now, target, maxDates);

            var sched = target.First(m => m.ScheduleID == id);

            Assert.IsTrue(sched.StartDate > now);
        }

        [TestMethod]
        public void Reschedule_HasFutureMaxDates_MustUseFuture()
        {
            var maxDates = new Dictionary<int, DateTime>();
            var threeDaysForward = DateTime.Now.AddDays(3);
            var id = 43;
            maxDates.Add(id, threeDaysForward);

            // recreate sample data
            var target = CreateTestData1();

            // reschedule
            IScheduler scheduler = SchedulerFactory.CreateRegularScheduler();
            var now = DateTime.Now;
            scheduler.Reschedule(now, target, maxDates);

            var sched = target.First(m => m.ScheduleID == id);

            Assert.AreEqual(sched.StartDate, threeDaysForward);
        }

        [TestMethod]
        public void UpdateSchedule_ChangeAddHours_StartDateShouldNotChange()
        {
            var maxDates = new Dictionary<int, DateTime>();

            // recreate sample test data
            var target = CreateTestData1();
            var dte = new DateTime(2015, 10, 21, 14, 50, 0); // 10/21/2015 2:50PM 
            IScheduler scheduler = SchedulerFactory.CreateRegularScheduler();
            scheduler.Reschedule(dte, target, maxDates);

            target[2].AddHours = 4; // from 3 to 4. ID = 44 GROUP = 130

            scheduler.UpdateSchedule(target);


            var t = target[0];
            Assert.IsTrue(t.StartDate.Equals(dte)); // start date should not change
        }

        [TestMethod]
        public void UpdateSchedule_ChangeAddHours_DatesShouldCascade()
        {

            // recreate sample test data
            var target = CreateTestData1();
            var dte = new DateTime(2015, 10, 21, 14, 50, 0); // 10/21/2015 2:50PM 
            IScheduler scheduler = SchedulerFactory.CreateRegularScheduler();
            scheduler.Reschedule(dte, target, new Dictionary<int, DateTime>());


            /*

            ORIG: 

            Group ID  From To
            130     42  10 / 21  2:50PM   10 / 21  5:50PM  0
            130     43  10 / 21  5:50PM   10 / 21  9:50PM  1
            130     44  10 / 21  9:50PM   10 / 22 12:50AM  2
            130     45  10 / 22 12:50AM   10 / 22  1:50AM  3

            131     42  10 / 21  1:50AM   10 / 21  4:50AM  4
            131     43  10 / 21  4:50AM   10 / 22  8:50AM  5
            131     44  10 / 22  8:50AM   10 / 22 11:50AM  6
            131     45  10 / 22 11:50AM   10 / 22 12:50PM  7
            */

            var id44s = target.Where(m => m.ScheduleID == 44);
            foreach (TestSchedule id44 in id44s)
            {
                id44.AddHours = 5; // from 3 to 5. ID = 44
            }

            /*
            AFTER:

            Group ID  From To
            130     42  10 / 21  2:50PM   10 / 21  5:50PM  0
            130     43  10 / 21  5:50PM   10 / 21  9:50PM  1
            130     44  10 / 21  9:50PM   10 / 22  2:50AM  2 from 3 to 5
            130     45  10 / 22  2:50AM   10 / 22  3:50AM  3 cascaded

            131     42  10 / 21  3:50AM   10 / 21  6:50AM  4 cascaded
            131     43  10 / 21  6:50AM   10 / 22 10:50AM  5 cascaded
            131     44  10 / 22 10:50AM   10 / 22  3:50PM  6 from 3 to 5
            131     45  10 / 22  3:50PM   10 / 22  2:50PM  7 cascaded

            */

            scheduler.UpdateSchedule(target, new Dictionary<int, DateTime>());


            var t = target[0];
            Assert.IsTrue(t.StartDate.Equals(dte)); // start date should not change

            Assert.IsTrue(t.StartDate.Hour == 14); // 2PM
            t = target[1];
            Assert.IsTrue(t.StartDate.Hour == 17); // 5PM
            t = target[2];
            Assert.IsTrue(t.StartDate.Hour == 21); // 9PM (ID=44 AddHours was changed)
            t = target[3];
            Assert.IsTrue(t.StartDate.Hour == 2); // 1AM (ID=45 Group=130 Should cascade from 12AM to 2AM Startdate)
            t = target[4];
            Assert.IsTrue(t.StartDate.Hour == 3); // 
            t = target[5];
            Assert.IsTrue(t.StartDate.Hour == 6); //  
            t = target[6];
            Assert.IsTrue(t.StartDate.Hour == 10); // 
            t = target[7];
            Assert.IsTrue(t.StartDate.Hour == 15); // 
        }


        [TestMethod]
        public void UpdateSchedule_HasEmptyStartDate_ShouldStartWithCurrentDateTime()
        {
            // recreate sample test data
            var target = CreateTestData1();

            var first = target.First();

            Assert.AreEqual(first.StartDate, DateTime.MinValue);

            IScheduler scheduler = SchedulerFactory.CreateRegularScheduler();
            DateTime dte = DateTime.Now;
            var maxDates = new Dictionary<int, DateTime>();
            scheduler.UpdateSchedule(target, maxDates, dte);

            Assert.AreEqual(first.StartDate, dte);
        }

    }
}
