﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShopFloor.Schedule;

namespace ShopFloor.Tests
{
    /// <summary>
    /// Summary description for ScheduleGangRunTests
    /// </summary>
    [TestClass]
    public class ScheduleGangRunTests : BaseScheduleTests
    {
        public ScheduleGangRunTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void Reschedule_TestCleanSampleData_EndDatesShouldFollow()
        {

            // recreate sample data
            var dte = new DateTime(2015, 10, 21, 14, 50, 0); // starts at: 10/21/2015 2:50PM
            var target = CreateTestData1();

            /*
            Grp  ID                    
            130  42   2:50PM    5:50PM 
            130  43   5:50PM    9:50PM 
            130  44   9:50PM   12:50AM 
            130  45  12:50AM    1:50AM 
            ---------------------------
            131  42   5:50PM    8:50PM 
            131  43   9:50PM    1:50AM    <-- BECAUSE 130.43 > 131.42
            131  44   1:50AM    4:50AM    
            131  45   4:50AM    5:50AM 
            */

            IScheduler scheduler = SchedulerFactory.CreateGanRunScheduler();
            scheduler.Reschedule(dte, target, new Dictionary<int, DateTime>());

            TestSchedule t;
            t = target[0]; Assert.IsTrue(t.StartDate.Hour == 14); //  2PM -  5PM <-- 130.42
            t = target[1]; Assert.IsTrue(t.StartDate.Hour == 17); //  5PM -  9PM <-- 130.43
            t = target[2]; Assert.IsTrue(t.StartDate.Hour == 21); //  9PM - 12AM <-- 130.44
            t = target[3]; Assert.IsTrue(t.StartDate.Hour == 0);  // 12AM -  1AM <-- 130.45
            // beyond this point gangrun should behave differently than regular scheduler
            t = target[4]; Assert.IsTrue(t.StartDate.Hour == 17); //  5PM -  8PM <-- 131.42 should follow 130.42
            t = target[5]; Assert.IsTrue(t.StartDate.Hour == 21); //  9PM -  2AM <-- 131.43 should follow 130.43 BECAUSE 130.43 > 131.42
            t = target[6]; Assert.IsTrue(t.StartDate.Hour == 1);  //  1AM -  4AM <-- 131.44 should follow 131.43
            t = target[7]; Assert.IsTrue(t.StartDate.Hour == 4);  //  4AM -  5AM <-- 131.45 should follow 131.44
        }

    }
}
