﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShopFloor.Entities;
using ShopFloor.Business;

namespace ShopFloor.Tests
{
	[TestClass]
	public class MOComputationTests
	{
		private const int CONST_fggBatchQty = 2500;
		private const int CONST_fggOuts = 10;
		private const int CONST_fgg2BatchQty = 3500;
		private const int CONST_fgg2Outs = 9;

		private const decimal CONST_RMQTY = 1.500000000000m;
		private const decimal CONST_UOMFACTOR = 1.250000000000m;
		private const decimal CONST_ALLOWANCE = 2000.000000000000m;
		private const decimal CONST_RMFACTOR = 0.250000000000m;

		private MODetailItm createTestITM_Regular()
		{
			// create MO
			MOHeader mo = createMO(false);

			// create FGG
			var fgg = createTestFGG(mo, "FGG0001", "Test FGG 001", CONST_fggBatchQty, 100, CONST_fggOuts);
			mo.FinishedGoods.Add(fgg);
			var fgg2 = createTestFGG(mo, "FGG0002", "Test FGG 002", CONST_fgg2BatchQty, 100, CONST_fgg2Outs);
			mo.FinishedGoods.Add(fgg2);

			// create ITM
			MODetailItm itm = createTestITM(fgg, 0);
			fgg.RawMaterials.Add(itm);

			return itm;
		}

		private MODetailItm createTestITM_103NonGR()
		{
			// create MO
			MOHeader mo = createMO(false);

			// create FGG
			var fgg = createTestFGG(mo, "FGG0001", "Test FGG 001", CONST_fggBatchQty, 100, CONST_fggOuts);
			mo.FinishedGoods.Add(fgg);
			var fgg2 = createTestFGG(mo, "FGG0002", "Test FGG 002", CONST_fgg2BatchQty, 100, CONST_fgg2Outs);
			mo.FinishedGoods.Add(fgg2);

			// create ITM
			MODetailItm itm = createTestITM(fgg, 103);
			fgg.RawMaterials.Add(itm);

			return itm;
		}

		private MODetailItm createTestITM_103GR()
		{
			// create MO
			MOHeader mo = createMO(true);

			// create FGG
			var fgg = createTestFGG(mo, "FGG0001", "Test FGG 001", CONST_fggBatchQty, 100, CONST_fggOuts);
			mo.FinishedGoods.Add(fgg);
			var fgg2 = createTestFGG(mo, "FGG0002", "Test FGG 002", CONST_fgg2BatchQty, 100, CONST_fgg2Outs);
			mo.FinishedGoods.Add(fgg2);

			// create ITM
			MODetailItm itm = createTestITM(fgg, 103);
			fgg.RawMaterials.Add(itm);

			return itm;
		}

		private MODetailItm createTestITM(MODetailFgg fgg, short itmsGrpCod)
		{
			var itm = new MODetailItm
			{
				ItmsGrpCod = itmsGrpCod,
				MODetailFgg = fgg
			};
			return itm;
		}

		private MOHeader createMO(bool gangRun)
		{
			var moType = new MOType { MOTypeCode = MOType.CONST_CODES_MO, MOTypeName = "MO" };

			var mo = new MOHeader
			{
				ID = int.MaxValue,
				MONumber = int.MaxValue,
				MOTypeID = 1,
				MOType = moType,
				GangRunTag = gangRun
			};
			mo.FinishedGoods = new List<MODetailFgg>();

			return mo;
		}

		private MODetailFgg createTestFGG(MOHeader mo, string code, string name, int batchQty, int batchPrcnt, int outs)
		{
			return new MODetailFgg
			{
				MOHeaderID = mo.ID,
				MOHeader = mo,
				ItemCode = code,
				ItemName = name,
				BatchQty = batchQty,
				BatchPrcnt = batchPrcnt,
				OutsTag = outs
			};
		}

		private BillOfMaterialDetail createBOMDetail(string code, decimal rmQty, decimal uomFactor, decimal allowance, decimal rmFactor)
		{
			return new BillOfMaterialDetail
			{
				RMCode = code,
				RMQuantity = rmQty,
				UOMFactor = uomFactor,
				Allowance = allowance,
				RMFactor = rmFactor
			};
		}
		private BillOfMaterialDetail createBOMDetailSample()
		{
			return createBOMDetail("R0SUSC2S00900002", CONST_RMQTY, CONST_UOMFACTOR, CONST_ALLOWANCE, CONST_RMFACTOR);
		}

		[TestMethod]
		public void TestIfRegularCalculatorIsCreated()
		{
			var itm = createTestITM_Regular();
			var calc = CalculatorFactory.Create(itm);

			Assert.IsTrue(calc.GetType() == typeof(RegularMOCalculator));
		}

		[TestMethod]
		public void TestIfGroup103NonGangRunCalculatorIsCreated()
		{
			var itm = createTestITM_103NonGR();
			var calc = CalculatorFactory.Create(itm);

			Assert.IsTrue(calc.GetType() == typeof(Group103NonGangRunCalculator));
		}

		[TestMethod]
		public void TestIfGroup103GangRunCalculatorIsCreated()
		{
			var itm = createTestITM_103GR();
			var calc = CalculatorFactory.Create(itm);

			Assert.IsTrue(calc.GetType() == typeof(Group103GangRunCalculator));
		}


		[TestMethod]
		public void TestComputationForGrpCD103NonGR()
		{
			var itm = createTestITM_103NonGR();
			var calc = CalculatorFactory.Create(itm);

			var d = createBOMDetailSample();
			var moQty = calc.ComputeMOQty(d.RMQuantity, d.UOMFactor.GetValueOrDefault(), d.Allowance.GetValueOrDefault(), d.RMFactor.GetValueOrDefault());

			var answer = (CONST_fggBatchQty * Decimal.Divide(1, CONST_fggOuts) * d.RMQuantity * d.UOMFactor.GetValueOrDefault()) + d.Allowance.GetValueOrDefault();
			Assert.AreEqual(moQty, answer);
		}

		[TestMethod]
		public void TestComputationForRegularITM()
		{
			var itm = createTestITM_Regular();
			var calc = CalculatorFactory.Create(itm);

			//var batchQty = calc.ComputeBatchQty();
			//var fgg = itm.MODetailFgg;
			//Assert.IsTrue(batchQty == (decimal) fgg.BatchQty);

			var d = createBOMDetailSample();
			var moQty = calc.ComputeMOQty(d.RMQuantity, d.UOMFactor.GetValueOrDefault(), d.Allowance.GetValueOrDefault(), d.RMFactor.GetValueOrDefault());

			var answer = (CONST_fggBatchQty * d.RMQuantity * d.RMFactor.GetValueOrDefault() * d.UOMFactor.GetValueOrDefault()) + d.Allowance.GetValueOrDefault();
			Assert.AreEqual(moQty, answer);
		}

		[TestMethod]
		public void TestComputationForGrpCD103GR()
		{
			var itm = createTestITM_103GR();
			var calc = CalculatorFactory.Create(itm);

			var d = createBOMDetailSample();
			var moQty = calc.ComputeMOQty(d.RMQuantity, d.UOMFactor.GetValueOrDefault(), d.Allowance.GetValueOrDefault(), d.RMFactor.GetValueOrDefault());

			decimal answer = (CONST_fgg2BatchQty * Decimal.Divide(1, CONST_fgg2Outs) * d.RMQuantity * d.UOMFactor.GetValueOrDefault()) + d.Allowance.GetValueOrDefault();
			Assert.AreEqual(moQty, answer);
		}
	}
}
